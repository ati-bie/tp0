package ar.edu.itba.ati.back.utils;

import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import ar.edu.itba.ati.back.transformations.ContrastIncreaseOperator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;

public final class ContrastIncreaseFunction implements Function<Image, Image> {
    private Map<Color, ContrastIncreaseOperator> functions;

    /**
     *
     * @param functions the contrast increase functions that will be applied to each of the pixels in the provided
     *                  image
     */
    public ContrastIncreaseFunction(final Map<Color, ContrastIncreaseOperator> functions) {
        this.functions = functions;
    }

    /**
     * Apply, for every pixel of every band of the image, a function that corresponds to the color that represents
     * that pixel. If a function is not specified for an existing band in the image, then that band is cloned to the
     * new image (without any changes)
     * @param image to be applied contrast increase
     * @return a new image with the contrast increase applied
     */
    @Override
    public Image apply(final Image image) {
        final List<Band> bands = new LinkedList<>();

        for (final Band band : image.getBandsList()) {
            final byte[][] m = new byte[image.getHeight()][image.getWidth()];
            final ContrastIncreaseOperator contrastIncrease = functions.get(band.getColor());
            final DoubleUnaryOperator operator;

            if(contrastIncrease == null) {
                operator = value -> value;
            } else {
                operator = contrastIncrease::apply;
            }

            for(int x = 0; x < band.getHeight(); x++) {
                for (int y = 0; y < band.getWidth(); y++) {
                    final double currPixel = band.getFromPixel(x, y) & 0xFF;
                    final int newPixel = toIntExact(round(operator.applyAsDouble(currPixel)));

                    if(newPixel < 0 || newPixel > 255) {
                        throw new IllegalStateException("value must be between 0 and 255");
                    }
                    m[x][y] = (byte) newPixel;
                }
            }
            bands.add(new Band(m, band.getColor()));
        }


        return new Image(bands);
    }
}
