package ar.edu.itba.ati.back.filters;

import ar.edu.itba.ati.back.interfaces.ImageFilter;
import ar.edu.itba.ati.back.models.Band;

public class GaussFilter implements ImageFilter{

    private double[][] window;
    private double windowSum;

    public GaussFilter(double sigma) {
        int side = (int)Math.round((2 * sigma) + 1);
        side += (side+1)%2;

        int midPoint = Math.max(3,((side-1)/2) + 1 );

        this.window = new double[side][side];
        for(int i = 0; i < side; i++){
            for(int j = 0; j < side; j++) {
                int midCenterX = Math.abs(i-midPoint)-1;
                int midCenterY = Math.abs(j-midPoint)-1;

                window[i][j] =
                        (1/Math.sqrt(2*Math.PI*sigma*sigma))*
                                Math.exp(-(midCenterX*midCenterX + midCenterY*midCenterY)/(2*sigma*sigma));
                windowSum += window[i][j];
            }
        }
    }

    @Override
    public byte applyFilter(int x, int y, Band band) {
        double sum = 0;

        int minOffset = (window.length-1) / 2;

        for(int i = -1 * minOffset; i <= minOffset; i++){
            for(int j = -1 * minOffset; j <= minOffset; j++) {
                // Avoids getting elements outside window by repetition method
                int useI = Math.max(Math.min(x+i, band.getHeight()-1), 0);
                int useJ = Math.max(Math.min(y+j, band.getWidth()-1), 0);

                sum += (double)(band.getFromPixel(useI,useJ) & 0xFF) * window[i+minOffset][j+minOffset] / windowSum;
            }
        }
        return (byte)(sum);
    }
}
