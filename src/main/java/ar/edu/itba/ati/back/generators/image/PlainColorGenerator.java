package ar.edu.itba.ati.back.generators.image;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import java.util.LinkedList;
import java.util.List;

public class PlainColorGenerator {

  public Image generatePlainImage(int width, int height, byte valueR, byte valueG, byte valueB) {
    byte[][] matrixR = new byte[height][width];
    byte[][] matrixG = new byte[height][width];
    byte[][] matrixB = new byte[height][width];

    for (int x = 0; x < height; x++) {
      for (int y = 0; y < width; y++) {
        matrixR[x][y] = valueR;
        matrixG[x][y] = valueG;
        matrixB[x][y] = valueB;
      }
    }

    List<Band> bandList = new LinkedList<>();
    bandList.add(new Band(matrixR, Color.RED));
    bandList.add(new Band(matrixG, Color.GREEN));
    bandList.add(new Band(matrixB, Color.BLUE));

    return new Image(bandList);
  }

}
