package ar.edu.itba.ati.back.models;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class Image {

  private final List<Band> bands;

  public Image(final List<Band> bands) {
    this.bands = bands;
  }

  public Optional<Band> getBand(final Color color) {
    return bands.stream().filter(band -> band.getColor() == color).findFirst();
  }

  /**
   * Returns the bands in the image
   *
   * @return the list of bands, not a deep or shallow copy
   */
  public Collection<Band> getBandsList() {
    return bands;
  }

  public int getHeight() {
    return bands.get(0).getHeight();
  }

  public int getWidth() {
    return bands.get(0).getWidth();
  }

  public byte[] getByteArray(final Color color) {
    final Optional<Band> bandOptional = getBand(color);

    if (!bandOptional.isPresent()) {
      return null;
    }

    return bandOptional.get().getByteArray();
 }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Image image = (Image) o;

    if (image.getBandsList().size() != this.getBandsList().size()) return false;

    for (Band b : this.getBandsList()) {
      if (!b.getByteArray().equals(this.getBand(b.getColor()).get())) {
        return false;
      }
    }
    return true;
  }

  @Override
  public int hashCode() {
    return bands != null ? bands.hashCode() : 0;
  }
}
