package ar.edu.itba.ati.back.interfaces;

import ar.edu.itba.ati.back.models.Band;

public interface ImageFilter {

    byte applyFilter(int x, int y, Band band);
}
