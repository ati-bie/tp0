package ar.edu.itba.ati.back.handlers.loaders;

import ar.edu.itba.ati.back.interfaces.ImageLoader;
import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import ij.ImagePlus;
import ij.io.Opener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PpmReader implements ImageLoader {

  @Override
  public Image loadImage(final String path) {
    final ImagePlus imagePlus = new Opener().openImage(path);
    final List<Band> bands = mapTo2DArray(imagePlus);

    return new Image(bands);
  }

  private List<Band> mapTo2DArray(final ImagePlus image) {
    final byte[][] matrixR = new byte[image.getHeight()][image.getWidth()];
    final byte[][] matrixG = new byte[image.getHeight()][image.getWidth()];
    final byte[][] matrixB = new byte[image.getHeight()][image.getWidth()];

    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        int[] pixel = image.getPixel(y, x);
        matrixR[x][y] = (byte) pixel[0];
        matrixG[x][y] = (byte) pixel[1];
        matrixB[x][y] = (byte) pixel[2];
      }
    }

    return new ArrayList<>(Arrays.asList(
        new Band(matrixR, Color.RED),
        new Band(matrixG, Color.GREEN),
        new Band(matrixB, Color.BLUE)
    ));
  }
}
