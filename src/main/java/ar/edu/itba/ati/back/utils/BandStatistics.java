package ar.edu.itba.ati.back.utils;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

import ar.edu.itba.ati.back.models.Band;

public class BandStatistics {

    private BandStatistics() { }

    public static double findMean(final Band band) {
        double cumSum = 0;

        for(int x = 0; x < band.getHeight(); x++) {
            for(int y = 0; y < band.getWidth(); y++) {
                cumSum += (band.getFromPixel(x, y) & 0xFF);
            }
        }

        return cumSum / (band.getHeight() * band.getWidth());
    }

    public static double findStandardDeviation(final Band band) {
        final double mean = findMean(band);
        double cumSum = 0;

        for(int x = 0; x < band.getHeight(); x++) {
            for(int y = 0; y < band.getWidth(); y++) {
                cumSum += pow((band.getFromPixel(x, y) & 0xFF) - mean, 2);
            }
        }

        return sqrt(cumSum / (band.getHeight() * band.getWidth()));
    }
}
