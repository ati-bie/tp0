package ar.edu.itba.ati.back.utils;

import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Image;
import ar.edu.itba.ati.back.models.Pair;
import ar.edu.itba.ati.back.transformations.LinearTransformation;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

public class LinearTransformationPunctualFunction implements Function<Image, Image> {

    @Override
    public Image apply(final Image image) {
        final List<Band> bands = new LinkedList<>();

        for (final Band band : image.getBandsList()) {
            final Pair bounds = getBounds(band);
            final LinearTransformation lt = new LinearTransformation(bounds.getValue1(), bounds.getValue2());
            final byte[][] newMatrix = new byte[band.getHeight()][band.getWidth()];

            for (int x = 0; x < image.getHeight(); x++) {
                for (int y = 0; y < image.getWidth(); y++) {
                    final int pixelMod = toIntExact(round(lt.apply(band.getFromPixel(x, y) & 0xFF)));

                    if (pixelMod < 0 || pixelMod > 255) {
                        throw new IllegalStateException("Compressed value must be in range [0, 255]: " + pixelMod);
                    }
                    newMatrix[x][y] = (byte) pixelMod;
                }
            }
            bands.add(new Band(newMatrix, band.getColor()));
        }

        return new Image(bands);
    }

    private Pair getBounds(final Band band) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int x = 0; x < band.getHeight(); x++) {
            for (int y = 0; y < band.getWidth(); y++) {
                final int currPixel = band.getFromPixel(x, y) & 0xFF;

                if (currPixel < min) {
                    min = currPixel;
                }
                if (currPixel > max) {
                    max = currPixel;
                }
            }
        }

        return new Pair(min, max);
    }
}
