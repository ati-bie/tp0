package ar.edu.itba.ati.back.handlers.writers;

import ar.edu.itba.ati.back.interfaces.ImageWriter;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import java.io.FileOutputStream;

public class PgmWriter implements ImageWriter {
    @Override
    public void writeImage(Image image, String path) {

        try {
            byte[] data = image.getBand(Color.GRAY).get().getByteArray();

            FileOutputStream out = new FileOutputStream(path);
            byte[] headerBytes = ("P5\n" + image.getWidth() + " " + image.getHeight() + "\n255\n")
                .getBytes();
            out.write(headerBytes);
            out.write(data);
            out.close();

        }
        catch (Exception e){
            System.err.println("Error : " + e.getMessage());
        }
    }
}
