package ar.edu.itba.ati.back.handlers.writers;

import ar.edu.itba.ati.back.interfaces.ImageWriter;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class RawWriter implements ImageWriter {

    private static final String HEADER_FORMAT = "#[Width],[Height],[isGrayscale]";
    private static final String DATA_EXTENSION = ".RAW";
    private static final String HEADER_EXTENSION = ".RHDR";

    @Override
    public void writeImage(final Image image, final String name) {

        if (!name.endsWith(DATA_EXTENSION)) {
            throw new IllegalArgumentException("The source file does not have the required extension");
        }

        writeImageHeader(image, name);

        writeImageData(image, name);

    }

    private void writeImageData(Image image, String name) {
        boolean isGrayscale = image.getBand(Color.GRAY).isPresent();
        int pixelSize = isGrayscale ? 3 : 1;
        byte data[] = new byte[image.getWidth() * image.getHeight() * pixelSize];

        if (isGrayscale) {
            data = image.getByteArray(Color.GRAY);
        } else {
            byte[][] bandsData = new byte[][]{image.getByteArray(Color.RED),
                    image.getByteArray(Color.GREEN), image.getByteArray(Color.BLUE)};
            for (int i = 0; i < image.getWidth() * image.getHeight() * 3; i++) {
                data[i] = bandsData[i % 3][i / 3];
            }
        }

        try {
            FileOutputStream out = new FileOutputStream(name);
            out.write(data);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void writeImageHeader(Image image, String name) {
        String fileWithoutExtension = name.substring(0, name.length() - DATA_EXTENSION.length());
        String headerFile = String.format("%s%s", fileWithoutExtension, HEADER_EXTENSION);

        int width = image.getWidth();
        int height = image.getHeight();
        boolean isGrayscale = image.getBand(Color.GRAY).isPresent();

        try {
            PrintWriter writer = new PrintWriter(headerFile, "UTF-8");
            writer.println(HEADER_FORMAT);
            writer.println(String.format("%d,%d,%s", width, height, isGrayscale));
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
