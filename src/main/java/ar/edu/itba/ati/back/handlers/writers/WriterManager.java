package ar.edu.itba.ati.back.handlers.writers;

import ar.edu.itba.ati.back.interfaces.ImageWriter;
import ar.edu.itba.ati.back.models.Image;

public class WriterManager {

    private final String writePath =
            System.getProperty("user.dir") + "/src/sift/resources/written/";

    public void writeImage(final Image image, final String imagePath) {
        final ImageWriter imageWriter;

        int i = imagePath.lastIndexOf('.');
        String extension = null;
        if (i > 0) {
            extension = imagePath.substring(i + 1);
        }

        switch (extension.toUpperCase()) {
            case "RAW":
                imageWriter = new RawWriter();
                break;
            case "PGM":
                imageWriter = new PgmWriter();
                break;
            case "PPM":
                imageWriter = new PpmWriter();
                break;
            default:
                throw new IllegalArgumentException("Cannot resolve writer for extension" + extension);

        }
        imageWriter.writeImage(image, imagePath);
    }
}
