package ar.edu.itba.ati.back.interfaces;

import ar.edu.itba.ati.back.models.Image;

public interface ImageLoader {

  Image loadImage(final String path);
}
