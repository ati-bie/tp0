package ar.edu.itba.ati.back.generators.random;

import ar.edu.itba.ati.back.interfaces.RandomGenerator;
import java.util.Random;

public class ExponentialRandomGenerator implements RandomGenerator {

  private final Random random;
  private final double lambda;

  public ExponentialRandomGenerator(final double lambda) {
    if (lambda <= 0) {
      throw new IllegalArgumentException("lambda must be greater than zero");
    }

    this.random = new Random();
    this.lambda = lambda;
  }

  @Override
  public double getNext() {
    return -Math.log(this.random.nextDouble()) / this.lambda;
  }
}
