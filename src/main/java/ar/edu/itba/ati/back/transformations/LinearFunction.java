package ar.edu.itba.ati.back.transformations;

import java.util.function.DoubleUnaryOperator;

public class LinearFunction implements DoubleUnaryOperator {
    private final double m;
    private final double b;

    public LinearFunction(final double x1, final double y1, final double x2, final double y2) {
        if(Double.compare(x2, x1) == 0) {
            throw new IllegalArgumentException("x1 cannot be equal to x2");
        }

        m = (y2 - y1) / (x2 - x1);
        b = y1 - m * x1;
    }

    public double applyAsDouble(final double value) {
        return m * value + b;
    }
}
