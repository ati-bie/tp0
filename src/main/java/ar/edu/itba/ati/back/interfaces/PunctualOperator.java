package ar.edu.itba.ati.back.interfaces;

/**
 * This interface is intended to be used with a value in the band
 */
public interface PunctualOperator {

  int apply(int value);
}
