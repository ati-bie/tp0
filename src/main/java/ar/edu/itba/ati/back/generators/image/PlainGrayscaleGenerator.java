package ar.edu.itba.ati.back.generators.image;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import java.util.LinkedList;
import java.util.List;

public class PlainGrayscaleGenerator {

    public Image generatePlainImage(int width, int height, byte value){
        byte[][] matrix = new byte[height][width];
        for(int x = 0; x < height; x++){
            for(int y = 0; y < width; y++) {
                matrix[x][y] = value;
            }
        }
        List<Band> bandList = new LinkedList<>();
        bandList.add(new Band(matrix, Color.GRAY));

        return new Image(bandList);
    }

}
