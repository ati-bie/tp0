package ar.edu.itba.ati.back.operators;

import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import ar.edu.itba.ati.back.interfaces.PunctualOperator;

public class ScalarPunctualOperator implements PunctualOperator {

  private final double scalar;

  public ScalarPunctualOperator(final double scalar) {
    this.scalar = scalar;
  }


  @Override
  public int apply(final int value) {
    return toIntExact(round(scalar * value));
  }
}
