package ar.edu.itba.ati.back.operators;

import ar.edu.itba.ati.back.interfaces.PunctualOperator;

public class NegativePunctualOperator implements PunctualOperator {

    private static final int MAX_VALUE = 255;

  @Override
  public int apply(final int value) {
    return (MAX_VALUE - value);
  }
}
