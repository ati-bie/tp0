package ar.edu.itba.ati.back.transformations;

import java.util.function.DoubleUnaryOperator;

/**
 * A linear function which contains the points (r1, 0) and (r2, 255). If r1 and r2 are equal then the linear
 * transformation acts as an identity function
 */
public class LinearTransformation {
  private static final int MAX_VALUE = 255;

  private final DoubleUnaryOperator f;

  public LinearTransformation(final double r1, final double r2) {
    if (Double.compare(r1, r2) == 0) {
      f = value -> value;
    } else {
      f = new LinearFunction(r1, 0, r2, MAX_VALUE);
    }
  }

  public double apply(final double value) {
    return f.applyAsDouble(value);
  }
}
