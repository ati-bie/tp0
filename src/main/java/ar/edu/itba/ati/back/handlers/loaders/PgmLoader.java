package ar.edu.itba.ati.back.handlers.loaders;

import ar.edu.itba.ati.back.interfaces.ImageLoader;
import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import ij.ImagePlus;
import ij.io.Opener;
import java.util.Collections;

public class PgmLoader implements ImageLoader {

  @Override
  public Image loadImage(final String path) {
    final ImagePlus imagePlus = new Opener().openImage(path);
    final byte[][] matrix = mapTo2DArray(imagePlus);
    final Band grayBand = new Band(matrix, Color.GRAY);

    return new Image(Collections.singletonList(grayBand));
  }

  private byte[][] mapTo2DArray(final ImagePlus image) {
    final byte[][] matrix = new byte[image.getHeight()][image.getWidth()];

    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        // ImageJ has the coordinate system inverted
        // Obtain only the first byte of the pixel because it is in Gray color
        matrix[x][y] = (byte) image.getPixel(y, x)[0];
      }
    }

    return matrix;
  }
}
