package ar.edu.itba.ati.back.interfaces;

/**
 * An operator for getting the modifying a pixel according to the method
 */
public interface NoiseOperator {
    double getNewPixel(double pixelValue, double random);
}
