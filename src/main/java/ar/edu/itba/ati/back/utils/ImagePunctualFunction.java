package ar.edu.itba.ati.back.utils;

import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import ar.edu.itba.ati.back.interfaces.PunctualOperator;
import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Image;
import ar.edu.itba.ati.back.transformations.DRC;
import java.util.LinkedList;
import java.util.List;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;

public class ImagePunctualFunction implements Function<Image, Image> {

  private final PunctualOperator operator;

  public ImagePunctualFunction(final PunctualOperator operator) {
    this.operator = operator;
  }

  /**
   * Applies the PunctualOperator to every pixel value in the image, in every band
   *
   * @param image the image to have applied the punctual operator
   * @return a new image with the new values
   */
  @Override
  public Image apply(final Image image) {
    final List<Band> newBandsList = new LinkedList<>();

    for (final Band band : image.getBandsList()) {
      final int[][] rawMatrix = new int[image.getHeight()][image.getWidth()];
        int max = Integer.MIN_VALUE;

      for (int x = 0; x < image.getHeight(); x++) {
        for (int y = 0; y < image.getWidth(); y++) {
          final int result = operator.apply(band.getFromPixel(x, y) & 0xFF);

          rawMatrix[x][y] = result;
            if (max < result) {
                max = result;
          }
        }
      }

        final DoubleUnaryOperator operator;

        if (max <= 255) {
            operator = value -> value;
        } else {
          operator = new DRC(max)::apply;
        }

      final byte[][] bandMatrix = new byte[image.getHeight()][image.getWidth()];

      for (int x = 0; x < image.getHeight(); x++) {
        for (int y = 0; y < image.getWidth(); y++) {
            final int compressedValue = toIntExact(round(operator.applyAsDouble(rawMatrix[x][y])));

          if (compressedValue < 0 || compressedValue > 255) {
            throw new IllegalStateException("Compressed value must be in range [0, 255]");
          }
            bandMatrix[x][y] = (byte) compressedValue;
        }
      }
      newBandsList.add(new Band(bandMatrix, band.getColor()));
    }

    return new Image(newBandsList);
  }
}
