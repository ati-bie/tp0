package ar.edu.itba.ati.back.generators.random;

import static java.lang.Math.log;
import static java.lang.Math.sqrt;

import ar.edu.itba.ati.back.interfaces.RandomGenerator;
import java.util.Random;

public class RayleighRandomGenerator implements RandomGenerator {

  private final Random random;
  private final double psi;

  public RayleighRandomGenerator(final double psi) {
    this.random = new Random();
    this.psi = psi;
  }

  @Override
  public double getNext() {
    return this.psi * sqrt(-2 * log(1 - this.random.nextDouble()));
  }
}
