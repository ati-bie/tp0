package ar.edu.itba.ati.back.generators.image;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;

public class CircleGenerator {

    public Image drawCircleBorder(Image img, byte color, int xCenter, int yCenter, int radius) {

        applyMidpointCenterAlgorithmBorder(img, xCenter, yCenter, radius, color);

        return img;
    }

    public Image drawCircle(Image img, byte color, int xCenter, int yCenter, int radius) {

        applyMidpointCenterAlgorithm(img, xCenter, yCenter, radius, color);

        return img;
    }

    void applyMidpointCenterAlgorithmBorder(Image image, int x0, int y0, int radius, byte value) {
        Band band = image.getBand(Color.GRAY).get();

        int x = radius - 1;
        int y = 0;
        int dx = 1;
        int dy = 1;
        int err = dx - (radius << 1);

        while (x >= y) {

            band.putInPixel(value, x0 + y, y0 + x);
            band.putInPixel(value, x0 - y, y0 + x);

            band.putInPixel(value, x0 + x, y0 + y);
            band.putInPixel(value, x0 - x, y0 + y);

            band.putInPixel(value, x0 - x, y0 - y);
            band.putInPixel(value, x0 + x, y0 - y);

            band.putInPixel(value, x0 - y, y0 - x);
            band.putInPixel(value, x0 + y, y0 - x);

            if (err <= 0) {
                y++;
                err += dy;
                dy += 2;
            }
            if (err > 0) {
                x--;
                dx += 2;
                err += (-radius << 1) + dx;
            }
        }
    }


    void applyMidpointCenterAlgorithm(Image image, int x0, int y0, int radius, byte value) {
        Band band = image.getBand(Color.GRAY).get();

        int x = radius - 1;
        int y = 0;
        int dx = 1;
        int dy = 1;
        int err = dx - (radius << 1);

        while (x >= y) {
            drawHorizontalLine(band, x0 - y, x0 + y, y0 + x, value);
            drawHorizontalLine(band, x0 - x, x0 + x, y0 + y, value);
            drawHorizontalLine(band, x0 - x, x0 + x, y0 - y, value);
            drawHorizontalLine(band, x0 - y, x0 + y, y0 - x, value);

            if (err <= 0) {
                y++;
                err += dy;
                dy += 2;
            }
            if (err > 0) {
                x--;
                dx += 2;
                err += (-radius << 1) + dx;
            }
        }
    }

    private void drawHorizontalLine(Band band, int x0, int x1, int y, byte value) {

        for (int i = x0; i <= x1; i++) {
            band.putInPixel(value, i, y);
        }

    }

}
