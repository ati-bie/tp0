package ar.edu.itba.ati.back.interfaces;

/**
 * A pseudorandom double generator
 */
public interface RandomGenerator {

  /**
   * Obtain a pseudorandom double
   *
   * @return the pseudorandom double
   */
  double getNext();
}
