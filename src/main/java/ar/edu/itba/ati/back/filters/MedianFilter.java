package ar.edu.itba.ati.back.filters;

import ar.edu.itba.ati.back.interfaces.ImageFilter;
import ar.edu.itba.ati.back.models.Band;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MedianFilter implements ImageFilter {

    private int side;

    public MedianFilter(int side) {
        if(side % 2 == 0){
            throw new IllegalArgumentException("Window Side should be odd");
        }
        this.side = side;
    }

    @Override
    public byte applyFilter(int x, int y, Band band) {

        List<Integer> pixels = new ArrayList<>(side*side);
        int minOffset = (side-1) / 2;

        for(int i = -1 * minOffset; i <= minOffset; i++){
            for(int j = -1 * minOffset; j <= minOffset; j++) {
                // Avoids getting elements outside window by repetition method
                int useI = Math.max(Math.min(x+i, band.getHeight()-1), 0);
                int useJ = Math.max(Math.min(y+j, band.getWidth()-1), 0);

                pixels.add(band.getFromPixel(useI,useJ)&0xFF);
            }
        }
        Collections.sort(pixels);

        return (byte)((int)pixels.get((((side*side)-1)/2)+1));
    }
}
