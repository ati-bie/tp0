package ar.edu.itba.ati.back.transformations;

import static java.lang.Math.pow;
import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import ar.edu.itba.ati.back.interfaces.PunctualOperator;

public class GammaPower implements PunctualOperator {
    private static final int L = 256;

  private final double c;
  private final double gamma;

  public GammaPower(final double gamma) {
    if (Double.compare(gamma, 0) < 0 || Double.compare(gamma, 2.0) > 0) {
      throw new IllegalArgumentException("Gamma must be between 0 and 2.0");
    }
      System.out.println(gamma);
    this.c = pow(L - 1, 1 - gamma);
    this.gamma = gamma;
  }

    @Override
    public int apply(int value) {
        return toIntExact(round(c * pow(value, gamma)));
  }
}
