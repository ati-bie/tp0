package ar.edu.itba.ati.back.interfaces;

import ar.edu.itba.ati.back.models.Image;

public interface ImageWriter {

  void writeImage(Image image, String path);
}
