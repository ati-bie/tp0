package ar.edu.itba.ati.back.transformations;

import static java.lang.Math.log;

/**
 * Apply dynamic range compression to a value
 */
public class DRC {

  private static final int L = 256;

  private final double c;

  /**
   * @param R is the highest value in the band
   */
  public DRC(final int R) {
    if (R < 0) {
      throw new IllegalArgumentException("R must be equal or greater than zero");
    }

    this.c = (L - 1) / log(1 + R);
  }

  /**
   * Map a value in the range [0, L-1]
   *
   * @param r the value to be compressed
   * @return the value compressed
   */
  public double apply(final double r) {
    return c * log(1 + r);
  }
}
