package ar.edu.itba.ati.back.utils;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Pair;

public class ContrastIncreaseConstantsFinder {

    private ContrastIncreaseConstantsFinder() {
    }

    /**
     * Finds r1 and r2 constants for increase of contrast in a band. r1 is stored in value1 and r2 in value2 in the
     * Pair class
     * @param band
     * @return a pair with r1 and r2
     */
    public static Pair findConstants(final Band band) {
        final double mean = BandStatistics.findMean(band);
        final double stdDeviation = BandStatistics.findStandardDeviation(band);
        final double r1, r2;

        if(stdDeviation >= mean) {
            r1 = mean / 2;
        } else {
            r1 = mean - stdDeviation;
        }

        if(mean + stdDeviation > 255) {
            r2 = mean + (255 - mean) / 2;
        } else {
            r2 = mean + stdDeviation;
        }

        return new Pair(r1, r2);
    }
}
