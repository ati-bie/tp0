package ar.edu.itba.ati.back.handlers.writers;

import ar.edu.itba.ati.back.interfaces.ImageWriter;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import java.io.FileOutputStream;
import java.io.IOException;

public class PpmWriter implements ImageWriter {

  @Override
  public void writeImage(final Image image, final String path) {
    try (final FileOutputStream out = new FileOutputStream(path)) {
      final byte[] headerBytes = ("P6\n" + image.getWidth() + " " + image.getHeight() + "\n255\n")
          .getBytes();
      final byte dataToWrite[] = new byte[image.getWidth() * image.getHeight() * 3];
      final byte[][] bandsData = new byte[][]{image.getByteArray(Color.RED),
          image.getByteArray(Color.GREEN), image.getByteArray(Color.BLUE)};

      for (int i = 0; i < image.getWidth() * image.getHeight() * 3; i++) {
        dataToWrite[i] = bandsData[i % 3][i / 3];
      }
      out.write(headerBytes);
      out.write(dataToWrite);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
