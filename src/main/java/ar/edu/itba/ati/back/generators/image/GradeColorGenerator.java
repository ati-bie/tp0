package ar.edu.itba.ati.back.generators.image;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import java.util.LinkedList;
import java.util.List;

public class GradeColorGenerator {

    public Image generateGradeImageHorizontal(int width, int height, byte minValueR, byte maxValueR
            , byte minValueG, byte maxValueG, byte minValueB, byte maxValueB) {
        return generateGradeImage(width, height, minValueR, maxValueR,
                minValueG, maxValueG,minValueB, maxValueB, true);
    }

    public Image generateGradeImageVertical(int width, int height, byte minValueR, byte maxValueR
            , byte minValueG, byte maxValueG, byte minValueB, byte maxValueB) {
        return generateGradeImage(width, height, minValueR, maxValueR,
                minValueG, maxValueG,minValueB, maxValueB,  false);
    }

    private Image generateGradeImage(int width, int height, byte minValueR, byte maxValueR,
                                     byte minValueG, byte maxValueG, byte minValueB, byte maxValueB,boolean isHorizontal) {
        byte[][] matrixR = new byte[height][width];
        byte[][] matrixG = new byte[height][width];
        byte[][] matrixB = new byte[height][width];

        byte gradiantR = (byte) ((maxValueR & 0xFF - minValueR & 0xFF) & 0xFF);
        byte gradiantG = (byte) ((maxValueG & 0xFF - minValueG & 0xFF) & 0xFF);
        byte gradiantB = (byte) ((maxValueB & 0xFF - minValueB & 0xFF) & 0xFF);

        for (int x = 0; x < height; x++) {
            for (int y = 0; y < width; y++) {
                if (isHorizontal) {
                    matrixR[x][y] = (byte) ((gradiantR & 0xFF) * ((double) y / width) + minValueR);
                    matrixG[x][y] = (byte) ((gradiantG & 0xFF) * ((double) y / width) + minValueG);
                    matrixB[x][y] = (byte) ((gradiantB & 0xFF) * ((double) y / width) + minValueB);
                } else {
                    matrixR[x][y] = (byte) ((gradiantR & 0xFF) * ((double) x / width) + minValueR);
                    matrixG[x][y] = (byte) ((gradiantG & 0xFF) * ((double) x / width) + minValueG);
                    matrixB[x][y] = (byte) ((gradiantB & 0xFF) * ((double) x / width) + minValueB);
                }
            }
        }

        List<Band> bandList = new LinkedList<>();
        bandList.add(new Band(matrixR, Color.RED));
        bandList.add(new Band(matrixG, Color.GREEN));
        bandList.add(new Band(matrixB, Color.BLUE));

        return new Image(bandList);
    }
}
