package ar.edu.itba.ati.back.transformations;

/**
 * A 3 piecewise function which lowers the value of the pixel if it is lower than r1 because the slope is between
 * 0 and 1 and increases the value if it is greater than r2 because the slope is greater than 1 (making them lighter)
 */
public class ContrastIncreaseOperator {
    private static final int MAX_VALUE = 255;

    final double r1;
    final double r2;

    /**
     * Set of functions that conform the piecewise function
     */
    final LinearFunction f1;
    final LinearFunction f2;
    final LinearFunction f3;

    /**
     *
     * @param r1 bound from which the lower values than r1 have their color value decreased
     * @param y1 f(r1)
     * @param r2 bound from which the upper values than r2 have their color value increased
     * @param y2 f(r2)
     */
    public ContrastIncreaseOperator(final double r1, final double y1, final double r2, final double y2) {
        if(Double.compare(r1, r2) > 0) {
            throw new IllegalArgumentException("r2 must be equal or greater than r1");
        }

        f1 = new LinearFunction(0, 0, r1, y1);
        f2 = new LinearFunction(r1, y1, r2, y2);
        f3 = new LinearFunction(r2, y2, MAX_VALUE, MAX_VALUE);

        this.r1 = r1;
        this.r2 = r2;
    }

    public double apply(final double value) {
        if(Double.compare(0, value) > 0 || Double.compare(value, MAX_VALUE) > 0) {
            throw new IllegalArgumentException("value must be between 0 and 255");
        }

        if(Double.compare(value, r1) <= 0) {
            return f1.applyAsDouble(value);
        } else if(Double.compare(value, r2) <= 0) {
            return f2.applyAsDouble(value);
        } else {
            return f3.applyAsDouble(value);
        }
    }
}
