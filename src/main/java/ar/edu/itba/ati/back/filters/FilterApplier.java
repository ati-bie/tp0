package ar.edu.itba.ati.back.filters;

import ar.edu.itba.ati.back.interfaces.ImageFilter;
import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Image;
import java.util.LinkedList;
import java.util.List;

public class FilterApplier {

    public Image applyFilter(ImageFilter filter, Image image) {

        List<Band> newBands = new LinkedList<>();
        for (final Band band : image.getBandsList()) {
            final byte[][] newMatrix = new byte[image.getHeight()][image.getWidth()];

            for (int x = 0; x < image.getHeight(); x++) {
                for (int y = 0; y < image.getWidth(); y++) {
                    final byte result = filter.applyFilter(x, y, band);
                    newMatrix[x][y] = result;
                }
            }

            newBands.add(new Band(newMatrix, band.getColor()));
        }
        return new Image(newBands);
    }
}