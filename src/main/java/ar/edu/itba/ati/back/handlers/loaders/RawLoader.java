package ar.edu.itba.ati.back.handlers.loaders;

import ar.edu.itba.ati.back.interfaces.ImageLoader;
import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class RawLoader implements ImageLoader {

    private static final String DATA_EXTENSION = ".RAW";
    private static final String HEADER_EXTENSION = ".RHDR";

    @Override
    public Image loadImage(final String name) {

        if (!name.endsWith(DATA_EXTENSION)) {
            throw new IllegalArgumentException("The source file does not have the required extension");
        }

        RawHeader header = loadImageHeader(name);

        Image loadedImage = loadRawImage(name, header);

        return loadedImage;
    }

    private RawHeader loadImageHeader(String name) {
        String fileWithoutExtension = name.substring(0, name.length() - DATA_EXTENSION.length());
        String headerFile = String.format("%s%s", fileWithoutExtension, HEADER_EXTENSION);
        String line = null;

        try (BufferedReader br = new BufferedReader(new FileReader(headerFile))) {
            //Discard header
            br.readLine();
            line = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] headerAttrs = line.split(",");
        int width = Integer.valueOf(headerAttrs[0]);
        int height = Integer.valueOf(headerAttrs[1]);
        boolean isGrayscale = Boolean.valueOf(headerAttrs[2]);

        return new RawHeader(width, height, isGrayscale);
    }

    private Image loadRawImage(String name, RawHeader header) {
        StringBuilder builder = new StringBuilder();

        byte[] imageData = new byte[0];
        try {
            imageData = Files.readAllBytes(Paths.get(name));
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Band> bands = new LinkedList<>();

        if (header.isGrayscale) {
            byte[][] matrix = new byte[header.getHeight()][header.getWidth()];
            for (int x = 0; x < header.getHeight(); x++) {
                for (int y = 0; y < header.getWidth(); y++) {
                    matrix[x][y] = imageData[x * header.getWidth() + y];
                }
            }
            bands.add(new Band(matrix, Color.GRAY));
        } else {
            Color[] bandColors = new Color[]{Color.RED, Color.GREEN, Color.BLUE};

            for (int i = 0; i < bandColors.length; i++) {
                Color bandColor = bandColors[i];

                byte[][] matrix = new byte[header.getHeight()][header.getWidth()];
                for (int x = 0; x < header.getHeight(); x++) {
                    for (int y = 0; y < header.getWidth(); y++) {
                        matrix[x][y] = imageData[(x * header.getWidth() + y) * 3 + i];
                    }
                }
                bands.add(new Band(matrix, bandColor));
            }
        }
        return new Image(bands);
    }

    private class RawHeader {
        private int width;
        private int height;
        private boolean isGrayscale;

        public RawHeader(int width, int height, boolean isGrayscale) {
            this.width = width;
            this.height = height;
            this.isGrayscale = isGrayscale;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public boolean isGrayscale() {
            return isGrayscale;
        }
    }

}
