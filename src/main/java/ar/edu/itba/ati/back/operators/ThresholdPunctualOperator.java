package ar.edu.itba.ati.back.operators;

import ar.edu.itba.ati.back.interfaces.PunctualOperator;

public class ThresholdPunctualOperator implements PunctualOperator {

  private static final int L = 255;
  private final int threshold;

  public ThresholdPunctualOperator(final int threshold) {
    if (threshold < 0 || threshold > L) {
      throw new IllegalArgumentException("Threshold must be in the range [0, " + L + "]");
    }
    this.threshold = threshold;
  }

  @Override
  public int apply(final int value) {
    if (value < 0 || value > L) {
      throw new IllegalArgumentException("Value must be in the range [0, " + L + "]");
    }
    return value > threshold ? L : 0;
  }
}
