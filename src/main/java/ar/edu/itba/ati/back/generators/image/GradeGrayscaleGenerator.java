package ar.edu.itba.ati.back.generators.image;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import java.util.LinkedList;
import java.util.List;

public class GradeGrayscaleGenerator {

    public Image generateGradeImageHorizontal(int width, int height, byte minValue, byte maxValue) {
        return generateGradeImage(width, height, minValue, maxValue, true);
    }

    public Image generateGradeImageVertical(int width, int height, byte minValue, byte maxValue) {
        return generateGradeImage(width, height, minValue, maxValue, false);
    }

    private Image generateGradeImage(int width, int height, byte minValue, byte maxValue, boolean isHorizontal) {
        byte[][] matrix = new byte[height][width];

        byte gradiant = (byte) ((maxValue & 0xFF - minValue & 0xFF) & 0xFF);

        for (int x = 0; x < height; x++) {
            for (int y = 0; y < width; y++) {
                if (isHorizontal) {
                    matrix[x][y] = (byte) ((gradiant & 0xFF) * ((double) y / width) + minValue);
                } else {
                    matrix[x][y] = (byte) ((gradiant & 0xFF) * ((double) x / width) + minValue);
                }
            }
        }

        List<Band> bandList = new LinkedList<>();
        bandList.add(new Band(matrix, Color.GRAY));

        return new Image(bandList);
    }
}
