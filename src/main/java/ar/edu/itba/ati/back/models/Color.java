package ar.edu.itba.ati.back.models;

public enum Color {
  GRAY, RED, GREEN, BLUE
}
