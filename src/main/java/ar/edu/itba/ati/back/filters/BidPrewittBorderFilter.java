package ar.edu.itba.ati.back.filters;

import ar.edu.itba.ati.back.interfaces.ImageFilter;
import ar.edu.itba.ati.back.models.Band;

public class BidPrewittBorderFilter implements ImageFilter {
    private static final int[][] window1 = {{-1,0,1},{-1,0,1},{-1,0,1}};
    private static final int[][] window2 = {{1,1,1},{0,0,0},{-1,-1,-1}};
    private static final int[][] window3 = {{0,1,1},{-1,0,1},{-1,-1,0}};
    private static final int[][] window4 = {{-1,-1,0},{-1,0,1},{0,1,1}};


    @Override
    public byte applyFilter(int x, int y, Band band) {
        double sum = 0;

        int minOffset = (window1.length-1) / 2;

        for(int i = -1 * minOffset; i <= minOffset; i++){
            for(int j = -1 * minOffset; j <= minOffset; j++) {
                int useI = Math.max(Math.min(x+i, band.getHeight()-1), 0);
                int useJ = Math.max(Math.min(y+j, band.getWidth()-1), 0);

                sum += getMaxFilterValue((double)(band.getFromPixel(useI,useJ) & 0xFF), i, j, minOffset);
            }
        }
        return (byte)(sum);
    }

    private double getMaxFilterValue(double pixel, int i, int j, int minOffset) {
        double r1 = pixel * window1[i+minOffset][j+minOffset];
        double r2 = pixel * window2[i+minOffset][j+minOffset];
        double r3 = pixel * window3[i+minOffset][j+minOffset];
        double r4 = pixel * window4[i+minOffset][j+minOffset];

        return Math.max(r1, Math.max(r2, Math.max(r3, r4)));
    }
}
