package ar.edu.itba.ati.back.generators.random;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.log;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

import ar.edu.itba.ati.back.interfaces.RandomGenerator;
import java.util.Random;

public class GaussianRandomGenerator implements RandomGenerator {

  private final Random random;

  private final double mu;
  private final double sigma;

  private double y2;

  /**
   * True if y1 and y2 have been used an two new random numbers should be generated
   */
  private boolean shouldGenerate;

  /**
   * @param mu is the mean
   * @param sigma is the standard deviation
   */
  public GaussianRandomGenerator(final double mu, final double sigma) {
    this.random = new Random();
    this.mu = mu;
    this.sigma = sigma;
    this.shouldGenerate = false;
  }

  @Override
  public double getNext() {
    this.shouldGenerate = !this.shouldGenerate;

    if (!shouldGenerate) {
      return applyMuAndSigma(this.y2);
    }

    final double x1 = this.random.nextDouble();
    final double x2 = this.random.nextDouble();
    double y1 = sqrt(-2 * log(x1)) * cos(2 * PI * x2);
    this.y2 = sqrt(-2 * log(x1)) * sin(2 * PI * x2);

    return applyMuAndSigma(y1);
  }

  private double applyMuAndSigma(final double y) {
    return this.mu + y * this.sigma;
  }
}
