package ar.edu.itba.ati.back.handlers.loaders;

import ar.edu.itba.ati.back.interfaces.ImageLoader;
import ar.edu.itba.ati.back.models.Image;

public class LoaderManager {

    private final String loadPath = System.getProperty("user.dir") + "/src/sift/resources/";

  public Image loadImage(final String imagePath) {
        final ImageLoader imageLoader;

    int i = imagePath.lastIndexOf('.');
        String extension = null;
        if (i > 0) {
          extension = imagePath.substring(i + 1);
        }

        switch (extension.toUpperCase()) {
            case "RAW":
                imageLoader = new RawLoader();
                break;
            case "PGM":
                imageLoader = new PgmLoader();
                break;
            case "PPM":
                imageLoader = new PpmReader();
                break;
            default:
                throw new IllegalArgumentException("Cannot resolve loader for extension " + extension);
        }

        return imageLoader.loadImage(imagePath);
    }
}
