package ar.edu.itba.ati.back.utils;

import static java.lang.Math.max;
import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Image;
import ar.edu.itba.ati.back.transformations.LinearTransformation;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.BinaryOperator;
import java.util.function.DoubleUnaryOperator;
import java.util.function.IntBinaryOperator;

public class ImagePunctualBinaryOperator implements BinaryOperator<Image> {

  private final IntBinaryOperator operator;

  public ImagePunctualBinaryOperator(final IntBinaryOperator operator) {
    this.operator = operator;
  }

  @Override
  public Image apply(final Image image, final Image image2) {
    Objects.requireNonNull(image);
    Objects.requireNonNull(image2);

    final int newImageHeight = max(image.getHeight(), image2.getHeight());
    final int newImageWidth = max(image.getWidth(), image2.getWidth());
    final List<Band> newBands = new LinkedList<>();

    for (final Band bandImage1 : image.getBandsList()) {
      final Band bandImage2 = image2.getBand(bandImage1.getColor()).get();
      int r1 = Integer.MAX_VALUE; // Lower bound of Linear transformation
      int r2 = Integer.MIN_VALUE; // Upper bound of Linear transformation
      final int[][] rawMatrix = new int[newImageHeight][newImageWidth];

      for (int x = 0; x < newImageHeight; x++) {
        for (int y = 0; y < newImageWidth; y++) {
          final int value1 = getRealOrVirtualValueFromMatrix(x, y, bandImage1);
          final int value2 = getRealOrVirtualValueFromMatrix(x, y, bandImage2);
          final int result = operator.applyAsInt(value1, value2);

          rawMatrix[x][y] = result;
          if (result < r1) {
            r1 = result;
          }
          if (result > r2) {
            r2 = result;
          }
        }
      }

        final DoubleUnaryOperator operator;
        if (r1 >= 0 && r1 <= 255 && r2 >= 0 && r2 <= 255) {
            operator = value -> value;
        } else {
            operator = new LinearTransformation(r1, r2)::apply;
        }


      final byte[][] bandMatrix = new byte[newImageHeight][newImageWidth];
      for (int x = 0; x < newImageHeight; x++) {
        for (int y = 0; y < newImageWidth; y++) {
            final int compressedValue = toIntExact(round(operator.applyAsDouble(rawMatrix[x][y])));

          if (compressedValue < 0 || compressedValue > 255) {
              throw new IllegalStateException("Compressed value must be in range [0, 255]: " + compressedValue);
          }

            bandMatrix[x][y] = (byte) compressedValue;
        }
      }

      newBands.add(new Band(bandMatrix, bandImage1.getColor()));
    }

    return new Image(newBands);
  }

  /**
   * Obtain the value of the current offset in the band. If it is off the dimensions of the band,
   * then zero is returned
   *
   * @param x coordinate
   * @param y coordinate
   * @return the
   */
  private int getRealOrVirtualValueFromMatrix(final int x, final int y, final Band band) {
    if (x < band.getHeight() && y < band.getWidth()) {
      return band.getFromPixel(x, y) & 0xFF;
    }
    return 0;
  }
}