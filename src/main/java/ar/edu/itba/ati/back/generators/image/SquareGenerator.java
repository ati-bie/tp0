package ar.edu.itba.ati.back.generators.image;

import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;

public class SquareGenerator {

    public Image drawSquare(Image img, byte color, int xCenter, int yCenter, int side) {

        int centerCorrection = side % 2;
        int centerOffset = side / 2;
        for (int x = xCenter - centerOffset - centerCorrection; x < xCenter + centerOffset; x++) {
            for (int y = yCenter - centerOffset - centerCorrection; y < yCenter + centerOffset; y++) {
                img.getBand(Color.GRAY).get().putInPixel(color, x, y);
            }
        }

        return img;
    }

}
