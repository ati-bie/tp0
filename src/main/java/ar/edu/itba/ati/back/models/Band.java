package ar.edu.itba.ati.back.models;

public class Band {

  private final byte[][] matrix;
  private final Color color;

  public Band(final byte[][] matrix, final Color color) {
    this.matrix = matrix;
    this.color = color;
  }

  public void putInPixel(final byte value, final int x, final int y) {
    matrix[x][y] = value;
  }

  public byte getFromPixel(final int x, final int y) {
    return matrix[x][y];
  }

  public Color getColor() {
    return color;
  }

  public int getHeight() {
    return matrix.length;
  }

  public int getWidth() {
    return matrix[0].length;
  }

  /**
   * Creates a copy of the pixels values in a byte array manner
   */
  public byte[] getByteArray() {
    final byte[] byteArray = new byte[getHeight() * getWidth()];

    for (int x = 0; x < getHeight(); x++) {
      for (int y = 0; y < getWidth(); y++) {
        byteArray[x * getWidth() + y] = matrix[x][y];
      }
    }

    return byteArray;
  }
}
