package ar.edu.itba.ati.front.PlayGround;

import ar.edu.itba.ati.back_2.adapter.*;
import ar.edu.itba.ati.back_2.models.IntPoint;
import ar.edu.itba.ati.front.ImageByteDrawer;
import ar.edu.itba.ati.front.ImageSelection;
import ar.edu.itba.ati.front.MainWindow;
import com.github.sarxos.webcam.Webcam;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Marco on 10/12/2017.
 */
public class CameraLevelSetDrawer {

  private static final PlaygroundOperatorsAdapter adapter = new PlaygroundOperatorsAdapter();
  private static long lastPrint;
  private static Pane originalImageGrid;
  private static ImageByte image;
  private static BufferedImage cameraImg;
  private static Webcam webcam;
  private static Timeline timeline;

  private static ImagesFiltersOperatorsAdapter filtersOperators = new ImagesFiltersOperatorsAdapter();
  private static ImagesUtilsAdapter utilsAdapters = new ImagesUtilsAdapter();

  private static int startingX, startingY;

  private static Rectangle newRectangle = null;
  private static boolean isBeingDrawn = false;
  private static boolean isStarted = false;

  private static java.util.List<IntPoint> listIn;
  private static java.util.List<IntPoint> listOut;

  private static int[][] phi;

  public static void openCamera() {

    webcam = Webcam.getDefault();

    isStarted = false;
    if(webcam.isOpen()){
      webcam.close();
    }

     //webcam.setViewSize(new Dimension(640, 480));
    webcam.setViewSize(new Dimension(320, 240));

    webcam.open();

    cameraImg = webcam.getImage();
    image = new ImagesFileHandlersAdapter().getEmptyColorImage(cameraImg.getHeight(), cameraImg.getWidth());

    adapter.byteStreamToImage(image, cameraImg);
    initPanel(image);

    lastPrint = System.currentTimeMillis();

    System.out.println("Camera started");

  }

  private static void startPolling(ImageSelection selection){

    listIn = MainWindow.getSquarePoints(selection.getX0(), selection.getY0(),
            selection.getX1(), selection.getY1());
    listOut = MainWindow.getSquarePoints(selection.getX0() - 1, selection.getY0() - 1,
            selection.getX1() + 1, selection.getY1() + 1);

    phi = filtersOperators.levelSet(image, 100000, listIn, listOut);

    ImageByte imageClone = utilsAdapters.drawPixels(image, listIn, listOut);
    ImageByteDrawer.renderImageByte(imageClone, originalImageGrid);

    if(timeline != null){
      timeline.stop();
    }

    timeline = new Timeline(new KeyFrame(Duration.millis(200), ev -> {
      drawFrame();
      timeline.setRate(System.currentTimeMillis() - lastPrint);
      double frameRate = (1.0/((double)(System.currentTimeMillis() - lastPrint)/1000));
      System.out.println("FPS: "+frameRate);
      lastPrint = System.currentTimeMillis();
    }));

    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.play();
  }

  private static void drawFrame() {
    if(!isBeingDrawn) {
      cameraImg = webcam.getImage();
      adapter.byteStreamToImage(image, cameraImg);
      phi = filtersOperators.levelSet(image, (int)MainWindow.doubleParam1, listIn, listOut, phi);
      ImageByte imageClone = utilsAdapters.drawPixels(image, listIn, listOut);
      ImageByteDrawer.renderImageByte(imageClone, originalImageGrid);
    }
  }


  private static void initPanel(ImageByte imageByte) {

    Pane originalImgWindow = new Pane();
    originalImgWindow.setVisible(true);

    originalImageGrid = new StackPane();
    originalImgWindow.getChildren().add(originalImageGrid);
    originalImgWindow.setPrefSize(imageByte.getWidth(), imageByte.getHeight());
    ImageByteDrawer.renderImageByte(imageByte, originalImageGrid);

    Stage newStage = new Stage();
    newStage.setWidth(imageByte.getWidth());
    newStage.setHeight(imageByte.getHeight());
    Scene imageScene = new Scene(originalImgWindow);

    newStage.setScene(imageScene);
    newStage.setTitle("Say cheese!");
    newStage.show();
    newStage.sizeToScene();
    newStage.setResizable(false);

    originalImgWindow.setLayoutX(0);
    originalImgWindow.setLayoutY(0);

    setRectangleDrawer(originalImgWindow);
  }

  private static void setRectangleDrawer(Pane pane) {
    pane.setOnMousePressed((MouseEvent event) ->
    {
     if (!isBeingDrawn) {
        startingX = (int) event.getSceneX();
        startingY = (int) event.getSceneY();
        newRectangle = new javafx.scene.shape.Rectangle();
        newRectangle.setStroke(javafx.scene.paint.Color.RED);
        newRectangle.setFill(javafx.scene.paint.Color.TRANSPARENT);
        newRectangle.setVisible(true);

        originalImageGrid.getChildren().add(newRectangle);
       System.out.println(startingX+" , "+startingY);
        isBeingDrawn = true;
      }
    });

    pane.setOnMouseDragged((MouseEvent event) ->
    {
      if (isBeingDrawn) {
        adjustProperties(startingX,
                startingY,
                (int) event.getX(),
                (int) event.getY(),
                newRectangle);
      }
    });

    pane.setOnMouseReleased((MouseEvent event) ->
    {
      if (isBeingDrawn) {
        newRectangle.setVisible(false);
        originalImageGrid.getChildren().remove(newRectangle);
        newRectangle = null;
        isBeingDrawn = false;

        ImageSelection selection = new ImageSelection(startingY, startingX,
                (int) event.getY(), (int) event.getX(), image.getWidth(), image.getHeight());
          startPolling(selection);
          isStarted = true;
      }
    });
  }

  private static void adjustProperties(int starting_point_x, int starting_point_y,
                                int ending_point_x, int ending_point_y,
                                Rectangle given_rectangle) {
   /*
    given_rectangle.setX(starting_point_x);
    given_rectangle.setY(starting_point_y);
    given_rectangle.setWidth(ending_point_x - starting_point_x);
    given_rectangle.setHeight(ending_point_y - starting_point_y);


    if (given_rectangle.getWidth() < 0) {
      given_rectangle.setWidth(-given_rectangle.getWidth());
      given_rectangle.setX(given_rectangle.getX() - given_rectangle.getWidth());
    }

    if (given_rectangle.getHeight() < 0) {
      given_rectangle.setHeight(-given_rectangle.getHeight());
      given_rectangle.setY(given_rectangle.getY() - given_rectangle.getHeight());
    }
    */
  }
}
