package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.front.InfoBar;
import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import java.util.List;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * Created by Marco on 10/12/2017.
 */
public class ThresholdMenuPopulator implements MenuPopulator {

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {

        final Menu thresholdsMenu = new Menu("Thresholds");

        final MenuItem globalThreshold = new MenuItem("Global");
        globalThreshold.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.GLOBAL_THRESHOLD);
            InfoBar.clearDisplay();
        });

        final MenuItem otsuThreshold = new MenuItem("Otsu");
        otsuThreshold.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.OTSU_THRESHOLD);
            InfoBar.clearDisplay();
        });
        thresholdsMenu.getItems().addAll(globalThreshold, otsuThreshold);

        menus.add(thresholdsMenu);
        return menus;
    }
}
