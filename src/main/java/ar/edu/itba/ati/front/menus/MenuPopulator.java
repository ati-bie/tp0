package ar.edu.itba.ati.front.menus;

import java.util.List;
import javafx.scene.control.Menu;
import javafx.stage.Stage;

public interface MenuPopulator {

    List<Menu> addMenus(final Stage stage, final List<Menu> menus);
}
