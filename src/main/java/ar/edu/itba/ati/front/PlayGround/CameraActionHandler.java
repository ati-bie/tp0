package ar.edu.itba.ati.front.PlayGround;

import ar.edu.itba.ati.back_2.adapter.*;
import ar.edu.itba.ati.front.InfoBar;
import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;

import static ar.edu.itba.ati.front.MainWindow.operatorInput;

/**
 * Created by Marco on 10/12/2017.
 */
public class CameraActionHandler {

    private static final ImagesUnaryOperatorsAdapter unaryOperators = new ImagesUnaryOperatorsAdapter();
    private static final ImagesNoiseOperatorsAdapter noiseOperators = new ImagesNoiseOperatorsAdapter();
    private static final ImagesFiltersOperatorsAdapter filtersOperators = new ImagesFiltersOperatorsAdapter();
    private static final ImagesThresholdsOperatorsAdapter thresholdsOperators = new ImagesThresholdsOperatorsAdapter();

    public static ImageByte transformActual(final ImageByte imageByte, Mode mode) {
        ImageByte result = null;

        if(mode == null){
            return imageByte;
        }

        switch (mode){
            case THRESHOLD:
                result = unaryOperators.threshold(imageByte, operatorInput);
                break;
            case NEGATIVE:
                result = unaryOperators.negative(imageByte);
                break;
           case HISTOGRAM:
               InfoBar.showHistogram(imageByte);
                result = imageByte;
                break;
            case FILTER_AVERAGE:
                result = filtersOperators.average(imageByte, (int) MainWindow.doubleParam1);
                break;
            case FILTER_MEDIAN:
                result = filtersOperators.median(imageByte, (int) MainWindow.doubleParam1);
                break;
            case FILTER_WEIGHTED_MEDIAN:
                result = filtersOperators.weightedMedian(imageByte);
                break;
            case FILTER_GAUSS:
                result = filtersOperators.gauss(imageByte, (int) MainWindow.doubleParam1);
                break;
            case FILTER_BORDER_DETECTION:
                result = filtersOperators.borderDetection(imageByte, (int) MainWindow.doubleParam1);
                break;
            case FILTER_PREWITT_VERTICAL:
                result = filtersOperators.prewittVertical(imageByte, (int) MainWindow.doubleParam1);
                break;
            case FILTER_PREWITT_HORIZONTAL:
                filtersOperators.prewittHorizontal(imageByte, (int) MainWindow.doubleParam1);
                break;
            case FILTER_PREWITT_SYNTHESIZED:
                result = filtersOperators.prewittSynthesized(imageByte, (int) MainWindow.doubleParam1);
                break;
            case FILTER_SOBEL:
                result = filtersOperators.sobelFilter(imageByte);
                break;
            case FILTER_LAPLACIAN_NO_SLOPE:
                result = filtersOperators.laplacianNoSlope(imageByte);
                break;
            case FILTER_LAPLACIAN_WITH_SLOPE:
                result = filtersOperators.laplacianWithSlope(imageByte, MainWindow.doubleParam1);
                break;
            case FILTER_ANISOTRPIC_DIFFUSION:
                result = filtersOperators.anisotropicFilter(imageByte, MainWindow.anisotropicMethod,
                        MainWindow.anisotropicSigma, MainWindow.anisotropicIterations);
                break;
            case FILTER_LAPLACIAN_GAUSSIAN_SLOPE:
                result = filtersOperators
                        .laplacianGaussianSlope(imageByte, MainWindow.doubleParam2, MainWindow.doubleParam1);
                break;
            case FILTER_BID_PREWITT_BORDER_DETECTION:
                result = filtersOperators.bidPrewittBorder(imageByte);
                break;
            case FILTER_BID_SOBEL_BORDER_DETECTION:
                result = filtersOperators.bidSobelBorder(imageByte);
                break;
            case GLOBAL_THRESHOLD:
                result = thresholdsOperators.global(imageByte);
                break;
            case OTSU_THRESHOLD:
                result = thresholdsOperators.otsu(imageByte);
                break;
            case SUSAN:
                result = filtersOperators.susan(imageByte);
                break;
            case CANNY:
                result = filtersOperators.canny(imageByte, MainWindow.params.get(0), MainWindow.params.get(1));
                break;
            default:
                System.out.println("Camera cant handle "+mode+" yet u_u");
                result = imageByte;
        }

        return result;
    }
}
