package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.front.InfoBar;
import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import java.util.List;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * Created by Marco on 10/12/2017.
 */
public class UnaryMenuPopulator implements MenuPopulator {

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {

        final MenuItem scalarProductItem = new MenuItem("Scalar Product");
        scalarProductItem.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.SCALAR_PRODUCT);
            InfoBar.showOperatorInput();
        });

        final MenuItem thresholdItem = new MenuItem("Threshold");
        thresholdItem.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.THRESHOLD);
            InfoBar.showOperatorInput();
        });

        final MenuItem negativeItem = new MenuItem("Negative");
        negativeItem.setOnAction(e -> MainWindow.setCurrentState(Mode.NEGATIVE));

        final MenuItem gammaPowerItem = new MenuItem("Gamma Power");
        gammaPowerItem.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.GAMMA_POWER);
            InfoBar.showOperatorInput();
        });

        final MenuItem contrastConstantsFinder = new MenuItem("Contrast Increase");
        contrastConstantsFinder.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.CONTRAST);
            InfoBar.showContrastInput();
        });

        final Menu unaryOperationsMenu = new Menu("Unary");
        unaryOperationsMenu.getItems().addAll(
//        scalarProductItem,
//        gammaPowerItem,
          negativeItem,
//        contrastConstantsFinder,
          thresholdItem
        );

        menus.add(unaryOperationsMenu);
        return menus;
    }
}
