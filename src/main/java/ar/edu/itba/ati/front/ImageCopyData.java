package ar.edu.itba.ati.front;

import ar.edu.itba.ati.back.models.Image;

public class ImageCopyData {

    private final Image image;
    private final ImageSelection selection;

    public ImageCopyData(Image image, ImageSelection selection) {
        this.image = image;
        this.selection = selection;
    }

    public Image getImage() {
        return image;
    }

    public ImageSelection getSelection() {
        return selection;
    }
}
