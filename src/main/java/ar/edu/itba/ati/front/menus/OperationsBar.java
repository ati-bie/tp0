package ar.edu.itba.ati.front.menus;

import ar.edu.itba.ati.front.menus.menuPopulators.AdvancedBorderMenuPopulator;
import ar.edu.itba.ati.front.menus.menuPopulators.FileMenuPopulator;
import ar.edu.itba.ati.front.menus.menuPopulators.FilterMenuPopulator;
import ar.edu.itba.ati.front.menus.menuPopulators.HistogramMenuPopulator;
import ar.edu.itba.ati.front.menus.menuPopulators.NoiseMenuPopulator;
import ar.edu.itba.ati.front.menus.menuPopulators.PlaygroundMenuPopulator;
import ar.edu.itba.ati.front.menus.menuPopulators.ThresholdMenuPopulator;
import ar.edu.itba.ati.front.menus.menuPopulators.UnaryMenuPopulator;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

 public class OperationsBar {

  private final static List<MenuPopulator> menuPopulators = Arrays.asList(
          new FileMenuPopulator(),
//          new SelectorMenuPopulator(),
          new UnaryMenuPopulator(),
//          new BinaryMenuPopulator(),
          new HistogramMenuPopulator(),
          new NoiseMenuPopulator(),
          new FilterMenuPopulator(),
      new ThresholdMenuPopulator(),
          new AdvancedBorderMenuPopulator(),
          new PlaygroundMenuPopulator()
          //   new ShapesMenuPopulator()
  );

  public static void createAndSetToPane(final BorderPane borderPane, final Stage stage) {

    final List<Menu> menus = new LinkedList<>();

    menuPopulators.forEach(menuPopulator -> menuPopulator.addMenus(stage, menus));

    MenuBar menuBar = new MenuBar();
    menuBar.getMenus().addAll(menus);
    borderPane.setTop(menuBar);
  }
}
