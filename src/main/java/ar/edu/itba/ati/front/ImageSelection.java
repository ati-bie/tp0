package ar.edu.itba.ati.front;

public class ImageSelection {

    private final int x0,y0,x1,y1;

    public ImageSelection(int x0, int y0, int x1, int y1, int width, int height) {

        int initX, initY, lastX, lastY;
        if (x0 < x1) {
            initX = x0;
            lastX = x1;
        } else {
            initX = x1;
            lastX = x0;
        }
        if (y0 < y1) {
            initY = y0;
            lastY = y1;
        } else {
            initY = y1;
            lastY = y0;
        }

        initX = Math.max(initX , 0);
        lastX = Math.min(lastX , height);
        initY = Math.max(initY , 0);
        lastY = Math.min(lastY , width);

        this.x0 = initX;
        this.y0 = initY;
        this.x1 = lastX;
        this.y1 = lastY;
    }


    public int getX0() {
        return x0;
    }

    public int getY0() {
        return y0;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }
}
