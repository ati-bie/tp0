package ar.edu.itba.ati.front;


import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.adapter.ImagesFileHandlersAdapter;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.front.utils.ImageByteToByteArrayConverter;
import ar.edu.itba.ati.front.utils.ImageByteType;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import mpicbg.imagefeatures.Feature;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.Objects;

/**
 * Util to render images of type {@link ImageByte}
 */
public abstract class ImageByteDrawer {


    private static final ImagesFileHandlersAdapter fileHandlers = new ImagesFileHandlersAdapter();

    public static ImageByte printFileImage(final File file) {
        ImageByte originalImage = fileHandlers.loadImage(file.getPath());
        printImage(originalImage, file.getName(), file.getPath());
        return originalImage;
    }


    public static Pane printImage(final ImageByte imageByte, final String name){
        return printImage(imageByte, name, null);
    }

    /**
     * Method for printing an image in its own window
     *
     * @param imageByte image to print on screen
     * @param name      the name of the image which will be showed
     */
    public static Pane printImage(final ImageByte imageByte, final String name, final String fileName) {
        Pane originalImgWindow = new Pane();
        originalImgWindow.setVisible(true);

        Pane originalImageGrid = new StackPane();
        originalImgWindow.getChildren().add(originalImageGrid);
        originalImgWindow.setPrefSize(imageByte.getWidth(), imageByte.getHeight());

        renderImageByte(imageByte, originalImageGrid);

        Stage newStage = new Stage();
        newStage.setWidth(imageByte.getWidth());
        newStage.setHeight(imageByte.getHeight());
        Scene imageScene = new Scene(originalImgWindow);

        newStage.setScene(imageScene);
        newStage.setTitle(name);
        newStage.show();
        newStage.sizeToScene();
        newStage.setResizable(false);

        originalImgWindow.setLayoutX(0);
        originalImgWindow.setLayoutY(0);

        new ImgMouseEventHandler(imageByte, originalImageGrid, fileName).setImgMouseHandlers(originalImgWindow);

        return originalImageGrid;
    }

    public static void renderImageByte(final ImageByte imageByte, final Pane imageGrid) {
        Objects.requireNonNull(imageByte, "imageByte cannot be null");
        Objects.requireNonNull(imageGrid, "imageGrid cannot be null");

        final javafx.scene.image.Image fxImage = getFXImage(imageByte);
        final Canvas canvas = createCanvas(fxImage);
        imageGrid.getChildren().add(canvas);
    }

    private static javafx.scene.image.Image getFXImage(final ImageByte imageByte) {
        final ImageByteToByteArrayConverter converter = new ImageByteToByteArrayConverter(imageByte);
        final ImageByteType imageByteType = ImageByteType.getImageByteType(imageByte);
        final BufferedImage bufferedImage;

        if (imageByteType == ImageByteType.GREYSCALE) {
            bufferedImage = new BufferedImage(imageByte.getWidth(), imageByte.getHeight(),
                    BufferedImage.TYPE_BYTE_GRAY);

            bufferedImage.getRaster().setDataElements(0, 0, imageByte.getWidth(),
                    imageByte.getHeight(), converter.getByteArrayforChannelType(ChannelType.GREY));
        } else if (imageByteType == ImageByteType.RGB) {
            final byte[] redStream = converter.getByteArrayforChannelType(ChannelType.RED);
            final byte[] greenStream = converter.getByteArrayforChannelType(ChannelType.GREEN);
            final byte[] blueStream = converter.getByteArrayforChannelType(ChannelType.BLUE);

            final int[] RGBStream = new int[imageByte.getDimensions() * 3];
            for (int i = 0; i < imageByte.getDimensions(); i++) {
                int fullPixel = redStream[i];
                fullPixel = (fullPixel << 8) + greenStream[i];
                fullPixel = (fullPixel << 8) + blueStream[i];

                RGBStream[i] = fullPixel;
            }
            bufferedImage = new BufferedImage(imageByte.getWidth(), imageByte.getHeight(),
                    BufferedImage.TYPE_INT_RGB);
            bufferedImage.getRaster().setDataElements(0, 0, imageByte.getWidth(),
                    imageByte.getHeight(), RGBStream);
        } else {
            throw new IllegalArgumentException("Image type not supported for converting to FX image");
        }

        return SwingFXUtils.toFXImage(bufferedImage, null);
    }

    private static Canvas createCanvas(final javafx.scene.image.Image fxImage) {
        final Canvas canvas = new Canvas(fxImage.getWidth(), fxImage.getHeight());
        final GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gc.drawImage(fxImage, 0, 0);

        return canvas;
    }

}
