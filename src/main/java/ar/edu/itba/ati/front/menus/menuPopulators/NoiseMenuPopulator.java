package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.front.InfoBar;
import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import java.util.List;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * Created by Marco on 10/12/2017.
 */
public class NoiseMenuPopulator implements MenuPopulator {

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {

        final Menu noisesMenu = new Menu("Noises");

        final MenuItem rayleighNoise = new MenuItem("Rayleigh");
        rayleighNoise.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.NOISE_RAYLEIGH);
            InfoBar.showRayleighInput();
        });

        final MenuItem gaussNoise = new MenuItem("Gauss");
        gaussNoise.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.NOISE_GAUSS);
            InfoBar.showGaussInput();
        });

        final MenuItem expNoise = new MenuItem("Exponential multiplicative");
        expNoise.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.NOISE_EXPMULT);
            InfoBar.showExpMultInput();
        });

        final MenuItem pepperSaltNoise = new MenuItem("Pepper & Salt");
        pepperSaltNoise.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.NOISE_PEPPERSALT);
            InfoBar.showPepperSaltInput();
        });

        noisesMenu.getItems().addAll(rayleighNoise, gaussNoise, expNoise, pepperSaltNoise);

        menus.add(noisesMenu);
        return menus;
    }
}
