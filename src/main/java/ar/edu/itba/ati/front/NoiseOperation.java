package ar.edu.itba.ati.front;

public interface NoiseOperation {
    int applyNoiseOperation(byte x1, byte x2);
}
