package ar.edu.itba.ati.front;

public class IntPair {

  private final int min;
  private final int max;

  public IntPair(final int min, final int max) {
    this.min = min;
    this.max = max;
  }

  public int getMin() {
    return min;
  }

  public int getMax() {
    return max;
  }

  @Override
  public String toString() {
    return "Pair{" +
        "min=" + min +
        ", max=" + max +
        '}';
  }
}
