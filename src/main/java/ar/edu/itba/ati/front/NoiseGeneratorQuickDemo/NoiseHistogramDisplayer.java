package ar.edu.itba.ati.front.NoiseGeneratorQuickDemo;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import ar.edu.itba.ati.back.models.Pair;
import ar.edu.itba.ati.back.utils.ContrastIncreaseConstantsFinder;
import ar.edu.itba.ati.front.ImageCopyData;
import ar.edu.itba.ati.front.ImageSelection;
import ar.edu.itba.ati.front.InfoBar;
import ar.edu.itba.ati.front.Mode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NoiseHistogramDisplayer extends Application {

    public static Mode currentMode;
    public static Image selectedImage;
    public static ImageCopyData copyData;
    public static int selectedColor;

    // Information for noise operations
    public static double pixelsPercentage, psi, mu, sigma, lambda, p0, p1;

    // Information for contrast operation
    public static double r1, y1, r2, y2;
    public static Image imageToApplyContrast = null;

    // Information for filter operations
    public static int selectedFilterSize;
    public static double selectedFilterSigma;

    // Pixel information nodes
    private static Scene scene;
    // Main Window State
    public static Image binaryOperatorImage;
    public static double operatorInput;
    private BorderPane root = new BorderPane();
    private FlowPane infoBar;

    public static void setCurrentState(Mode mode) {
        currentMode = mode;

        TextField modeInfo = new TextField("MODE - " + mode.toString());
        modeInfo.setDisable(true);
        displayBottomInfo(modeInfo);
    }

    public static void printPixelsAmountAndAvg(ImageSelection selection, Image image) {
        InfoBar.printPixelsAmountAndAvg(selection, image);
    }

    public static void printPixelInfo(int x, int y, String value) {
        InfoBar.printPixelInfo(x, y, value);
    }

    public static void displayBottomInfo(Node info) {
        ((BorderPane) NoiseHistogramDisplayer.scene.getRoot()).setBottom(info);
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static Pane drawHistogram(double[][] noiseMatrix) {
        Pane root = new Pane();
        root.setPrefSize(1200, 500);
        Map<Double, Double> histogramMap = calculateHistogram(noiseMatrix);

        Label labelInfo = new Label();
        labelInfo.setText("Noise histogram");

        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BarChart<String, Number> barChart =
                new BarChart<>(xAxis, yAxis);
        barChart.setCategoryGap(10);
        barChart.setBarGap(10);
        barChart.setHorizontalGridLinesVisible(false);
        barChart.setHorizontalZeroLineVisible(false);
        barChart.setPrefSize(400, 400);
        xAxis.setLabel("Range");
        yAxis.setLabel("Population");

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Color percentage");


        List<Double> list = new ArrayList();
        list.addAll(histogramMap.keySet());
        Collections.sort(list);

        for (Double binValue: list){
            if (histogramMap.containsKey(binValue)) {
                series1.getData().add(new XYChart.Data(binValue + "", histogramMap.get(binValue)));
            } else {
                series1.getData().add(new XYChart.Data(binValue + "", 0));
            }
        }

        barChart.getData().addAll(series1);

        VBox vBox = new VBox();
        vBox.getChildren().addAll(labelInfo, barChart);

        root.getChildren().add(vBox);

        return root;
    }


    private static double round(double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    //Returns the RELATIVE FREQUENCY by band for each pixel in the image
    private static Map<Double, Double> calculateHistogram(final double[][] image) {
        final Map<Color, Map<Integer, Double>> histograms = new HashMap<>();
        final int imageSize = image.length * image[0].length;

        final Map<Double, Double> pixelsBandMap = new HashMap<>();

        for (int x = 0; x < image.length; x++) {
            for (int y = 0; y < image[0].length; y++) {
                final double pixelValue = image[x][y];
                final double valueCount;
                final double assignedBin = round(pixelValue, 1);

                if (pixelsBandMap.containsKey(assignedBin)) {
                    valueCount = pixelsBandMap.get(assignedBin) + 1.0;
                } else {
                    valueCount = 1.0;
                }
                pixelsBandMap.put(assignedBin, valueCount);
            }
        }

        for (final Double pixelValue : pixelsBandMap.keySet()) {
            pixelsBandMap.put(pixelValue, pixelsBandMap.get(pixelValue) / imageSize);
        }


        return pixelsBandMap;
    }


    public static void displayBottomString(String s) {
        TextField modeInfo = new TextField(s);
        modeInfo.setDisable(true);
        displayBottomInfo(modeInfo);
    }

//    public static void showHistogram(Image image) {
//        InfoBar.showHistogram(image);
//    }

//    public static void equalizeHistogram(Image image) {
//        InfoBar.equalizeHistogram(image);
//    }
//
//    public static void applyRayleighNoise(Image image, double psi) {
//        Image noisedImage = NoiseHandler.applyRayleighNoise(image, psi);
//        ImageDrawer.printImage(noisedImage, "Rayleigh");
//    }
//
//    public static void applyGaussNoise(Image image, double mu, double sigma) {
//        Image noisedImage = NoiseHandler.applyGaussNoise(image, mu, sigma);
//        ImageDrawer.printImage(noisedImage, "Gauss");
//    }
//
//    public static void applyExpMultNoise(Image image, double lambda) {
//        Image noisedImage = NoiseHandler.applyExpMultNoise(image, lambda);
//        ImageDrawer.printImage(noisedImage, "Exponential Multiplicative");
//    }
//
//    public static void applyPeppermintAndSaltNoise(Image image, double p0, double p1) {
//        Image noisedImage = NoiseHandler.applyPeppermintAndSaltNoise(image, p0, p1);
//        ImageDrawer.printImage(noisedImage, "Peppermint and Salt");
//    }

    @Override
    public void start(Stage stage) throws Exception {
        scene = new Scene(root, 1200, 500);

        NoiseToolsBar.configureNavbar(this.root, stage);
        this.infoBar = InfoBar.configureInfoBar(this.root);
        currentMode = Mode.POINTER;

        stage.setTitle("ATI");
        stage.setScene(scene);
        stage.show();
    }

//    public static void mediaFilter(Image image) {
//        ImageFilter filter = new MediaFilter(NoiseHistogramDisplayer.selectedFilterSize);
//        Image mediaImage = new FilterApplier().applyFilter(filter, image);
//        ImageDrawer.printImage(mediaImage, "Media filter");
//    }
//
//    public static void medianFilter(Image image) {
//        ImageFilter filter = new MedianFilter(NoiseHistogramDisplayer.selectedFilterSize);
//        Image mediaImage = new FilterApplier().applyFilter(filter, image);
//        ImageDrawer.printImage(mediaImage, "Median filter");
//    }
//
//    public static void weightedMediaFilter(Image image) {
//        ImageFilter filter = new WeightedMedianFilter();
//        Image mediaImage = new FilterApplier().applyFilter(filter, image);
//        ImageDrawer.printImage(mediaImage, "Weighted Median filter");
//    }

//    public static void gaussFilter(Image image) {
//        ImageFilter filter = new GaussFilter(NoiseHistogramDisplayer.selectedFilterSigma);
//        Image mediaImage = new FilterApplier().applyFilter(filter, image);
//        ImageDrawer.printImage(mediaImage, "Gauss filter");
//    }
//
//    public static void borderDetectionFilter(Image image) {
//        ImageFilter filter = new BorderDetectionFilter(NoiseHistogramDisplayer.selectedFilterSize);
//        Image mediaImage = new FilterApplier().applyFilter(filter, image);
//        ImageDrawer.printImage(mediaImage, "Border detection filter");
//    }
//
//    public static void scalarProduct(Image image) {
//        PunctualOperator operator = new ScalarPunctualOperator(NoiseHistogramDisplayer.operatorInput);
//        ImagePunctualFunction punctualFunction = new ImagePunctualFunction(operator);
//        Image mediaImage = punctualFunction.apply(image);
//        ImageDrawer.printImage(mediaImage, "Scalar product");
//    }
//
//    public static void threshold(Image image) {
//        PunctualOperator operator = new ThresholdPunctualOperator((int) NoiseHistogramDisplayer.operatorInput);
//        ImagePunctualFunction punctualFunction = new ImagePunctualFunction(operator);
//        Image mediaImage = punctualFunction.apply(image);
//        ImageDrawer.printImage(mediaImage, "Threshold");
//    }

//    public static void negative(Image image) {
//        final PunctualOperator operator = new NegativePunctualOperator();
//        final ImagePunctualFunction punctualFunction = new ImagePunctualFunction(operator);
//        final Image mediaImage = punctualFunction.apply(image);
//        ImageDrawer.printImage(mediaImage, "Negative");
//    }

    public static void contrastConstantsFinder(Image image) {
        final Optional<Band> band = image.getBand(Color.GRAY);

        if (!band.isPresent()) {
            return;
        }

        final Pair constants = ContrastIncreaseConstantsFinder.findConstants(band.get());

        InfoBar.setContrastValueR1(constants.getValue1());
        InfoBar.setContrastValueR2(constants.getValue2());

        imageToApplyContrast = image;
    }

//    public static void contrastApply() {
//        if (imageToApplyContrast == null) {
//            return;
//        }
//        System.out.println(NoiseHistogramDisplayer.r1);
//        System.out.println(NoiseHistogramDisplayer.r2);
//        System.out.println(NoiseHistogramDisplayer.y1);
//        System.out.println(NoiseHistogramDisplayer.y2);
//        final ContrastIncreaseOperator operator = new ContrastIncreaseOperator(
//                NoiseHistogramDisplayer.r1, NoiseHistogramDisplayer.y1, NoiseHistogramDisplayer.r2, NoiseHistogramDisplayer.y2);
//        final Map<Color, ContrastIncreaseOperator> functions = new HashMap<>();
//        functions.put(Color.GRAY, operator);
//        final ContrastIncreaseFunction punctualFunction = new ContrastIncreaseFunction(functions);
//        final Image mediaImage = punctualFunction.apply(imageToApplyContrast);
//        ImageDrawer.printImage(mediaImage, "Contrast Increase");
//
//        imageToApplyContrast = null;
//    }

//    public static void subtraction(Image image) {
//        IntBinaryOperator operator = (left, right) -> left - right;
//        ImagePunctualBinaryOperator punctualFunction = new ImagePunctualBinaryOperator(operator);
//        Image mediaImage = punctualFunction.apply(NoiseHistogramDisplayer.binaryOperatorImage, image);
//        ImageDrawer.printImage(mediaImage, "Subtraction");
//    }

//    public static void addition(Image image) {
//        IntBinaryOperator operator = Integer::sum;
//        ImagePunctualBinaryOperator punctualFunction = new ImagePunctualBinaryOperator(operator);
//        Image mediaImage = punctualFunction.apply(NoiseHistogramDisplayer.binaryOperatorImage, image);
//        ImageDrawer.printImage(mediaImage, "Addition");
//    }

//    public static void product(Image image) {
//        IntBinaryOperator operator = (left, right) -> left * right;
//        ImagePunctualBinaryOperator punctualFunction = new ImagePunctualBinaryOperator(operator);
//        Image mediaImage = punctualFunction.apply(NoiseHistogramDisplayer.binaryOperatorImage, image);
//        ImageDrawer.printImage(mediaImage, "Product");
//    }
//
//    public static void gammaPower(final Image image) {
//        final double gamma = NoiseHistogramDisplayer.operatorInput;
//        final GammaPower gammaPower = new GammaPower(gamma);
//        final ImagePunctualFunction imagePunctualFunction = new ImagePunctualFunction(gammaPower);
//        final Image imageTransformed = imagePunctualFunction.apply(image);
//
//        ImageDrawer.printImage(imageTransformed, "Gamma Power");
//    }
//
//    public static void linearTransformation(final Image image) {
//        final Image imageTransformed = new LinearTransformationPunctualFunction().apply(image);
//
//        ImageDrawer.printImage(imageTransformed, "Linear Transformation");
//    }
//

    public static void applySelectedNoise() {
        double[][] noiseMatrix = null;
        switch (NoiseHistogramDisplayer.currentMode) {
            case DOUBLE_NOISE_EXPMULT:
                noiseMatrix = NoiseHandlerDoubles.applyExpMultNoise(NoiseHistogramDisplayer.lambda);
                break;
            case DOUBLE_NOISE_RAYLEIGH:
                noiseMatrix = NoiseHandlerDoubles.applyRayleighNoise(NoiseHistogramDisplayer.psi);
                break;
            case DOUBLE_NOISE_GAUSS:
                noiseMatrix = NoiseHandlerDoubles.applyGaussNoise(NoiseHistogramDisplayer.mu, NoiseHistogramDisplayer.sigma);
                break;
            case DOUBLE_NOISE_PEPPERSALT:
                noiseMatrix = NoiseHandlerDoubles.applyPeppermintAndSaltNoise(NoiseHistogramDisplayer.p0, NoiseHistogramDisplayer.p1);
                break;
        }

        Pane histWindow = NoiseHistogramDisplayer.drawHistogram(noiseMatrix);

        InfoBar.displayPane(histWindow);
    }
}
