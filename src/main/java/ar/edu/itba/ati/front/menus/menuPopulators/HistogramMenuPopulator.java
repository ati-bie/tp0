package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import java.util.List;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * Created by Marco on 10/12/2017.
 */
public class HistogramMenuPopulator implements MenuPopulator {

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {

        final Menu histogramMenu = new Menu("Histogram");

        MenuItem histogramItem = new MenuItem("Make histogram");
        histogramItem.setOnAction(e -> MainWindow.setCurrentState(Mode.HISTOGRAM));

//    MenuItem histogramEqualizationItem = new MenuItem("Equalize");
//    histogramEqualizationItem.setOnAction(e -> MainWindow.setCurrentState(Mode.EQUALIZE));

        histogramMenu.getItems().addAll(histogramItem/*, histogramEqualizationItem*/);

        menus.add(histogramMenu);
        return menus;
    }
}
