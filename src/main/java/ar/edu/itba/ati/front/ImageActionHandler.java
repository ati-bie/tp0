package ar.edu.itba.ati.front;

import ar.edu.itba.ati.back_2.adapter.ImageByte;


public class ImageActionHandler {

    public static void triggerAction(final ImageByte image) {
        switch (MainWindow.currentMode){
            case THRESHOLD:
                MainWindow.threshold(image);
                break;
            case NEGATIVE:
                MainWindow.negative(image);
                break;
            case HISTOGRAM:
                MainWindow.showHistogram(image);
                break;
            case FILTER_AVERAGE:
                MainWindow.averageFilter(image);
                break;
            case FILTER_MEDIAN:
                MainWindow.medianFilter(image);
                break;
            case FILTER_WEIGHTED_MEDIAN:
                MainWindow.weightedMedianFilter(image);
                break;
            case FILTER_GAUSS:
                MainWindow.gaussFilter(image);
                break;
            case FILTER_BORDER_DETECTION:
                MainWindow.borderDetectionFilter(image);
                break;
            case FILTER_PREWITT_VERTICAL:
                MainWindow.prewittVerticalFilter(image);
                break;
            case FILTER_PREWITT_HORIZONTAL:
                MainWindow.prewittHorizontalFilter(image);
                break;
            case FILTER_PREWITT_SYNTHESIZED:
                MainWindow.prewittSynthesizedFilter(image);
                break;
            case FILTER_SOBEL:
                MainWindow.sobelFilter(image);
                break;
            case FILTER_LAPLACIAN_NO_SLOPE:
                MainWindow.laplacianNoSlope(image);
                break;
            case FILTER_LAPLACIAN_WITH_SLOPE:
                MainWindow.laplacianWithSlope(image);
                break;
            case FILTER_ANISOTRPIC_DIFFUSION:
                MainWindow.anisotropicDiffusion(image);
                break;
            case FILTER_LAPLACIAN_GAUSSIAN_SLOPE:
                MainWindow.laplacianGaussianSlope(image);
                break;
            case FILTER_BID_PREWITT_BORDER_DETECTION:
                MainWindow.bidPrewittBorderDetectionFilter(image);
                break;
            case FILTER_BID_SOBEL_BORDER_DETECTION:
                MainWindow.bidSobelBorderDetectionFilter(image);
                break;
            case GLOBAL_THRESHOLD:
                MainWindow.globalThreshold(image);
                break;
            case OTSU_THRESHOLD:
                MainWindow.otsuThreshold(image);
                break;
            case CANNY:
                MainWindow.canny(image, MainWindow.params.get(0), MainWindow.params.get(1));
                break;
            case SUSAN:
                MainWindow.susan(image);
                break;
            case HOUGH:
                MainWindow.hough(image);
                break;
            case HARRIS:
                MainWindow.harris(image);
                break;
            case SIFT_1:
                MainWindow.binaryOperatorImage = image;
                MainWindow.currentMode = Mode.SIFT_2;
                break;
            case SIFT_2:
                MainWindow.sift(image);
                break;
            case NOISE_RAYLEIGH:
                MainWindow.rayleighNoise(image, MainWindow.params.get(0));
                break;
            case NOISE_GAUSS:
                MainWindow.gauss(image, MainWindow.params.get(0), MainWindow.params.get(1));
                break;
            case NOISE_EXPMULT:
                MainWindow.exponentialNoise(image, MainWindow.params.get(0));
                break;
            case NOISE_PEPPERSALT:
                MainWindow.peppermintAndSaltNoise(
                        image, MainWindow.params.get(0), MainWindow.params.get(1));
        }
//      if (MainWindow.currentMode == Mode.SUBTRACTION_1) {
//        MainWindow.setCurrentState(Mode.SUBTRACTION_2);
//        MainWindow.binaryOperatorImage = image;
//      } else if (MainWindow.currentMode == Mode.ADDITION_1) {
//        MainWindow.setCurrentState(Mode.ADDITION_2);
//        MainWindow.binaryOperatorImage = image;
//      } else if (MainWindow.currentMode == Mode.PRODUCT_1) {
//        MainWindow.setCurrentState(Mode.PRODUCT_2);
//        MainWindow.binaryOperatorImage = image;
//            } else if (MainWindow.currentMode == Mode.SUBTRACTION_2) {
//                MainWindow.subtraction(image);
//            } else if (MainWindow.currentMode == Mode.ADDITION_2) {
//                MainWindow.addition(image);
//            } else if (MainWindow.currentMode == Mode.PRODUCT_2) {
//                MainWindow.product(image);

//            } else if (MainWindow.currentMode == Mode.CONTRAST) {
//                MainWindow.contrastConstantsFinder(image);
//            } else if (MainWindow.currentMode == Mode.GAMMA_POWER) {
//                MainWindow.gammaPower(image);
//            } else if (MainWindow.currentMode == Mode.SCALAR_PRODUCT) {
//                MainWindow.scalarProduct(image);
//            } else if (MainWindow.currentMode == Mode.LINEAR_TRANSFORMATION) {
//                MainWindow.linearTransformation(image);

//            } else if (MainWindow.currentMode == Mode.EQUALIZE) {
//                MainWindow.equalizeHistogram(image);

//            } else if (MainWindow.currentMode == Mode.POINTER) {
//                this.displayPixelInfo(startingX, startingY);
//            } else if (MainWindow.currentMode == Mode.PASTE) {
//                this.pasteSelection(MainWindow.copyData, startingY, startingX);

    }

    public static void triggerAction(ImageByte image, ImageSelection selection) {
        switch (MainWindow.currentMode) {
            case LEVEL_SET_VIDEO:
            case LEVEL_SET:{
                MainWindow.levelSet(image, selection);
            }
        }
    }
}
