package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.front.InfoBar;
import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.util.List;

/**
 * Created by Marco on 10/12/2017.
 */
public class AdvancedBorderMenuPopulator implements MenuPopulator {

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {

        final Menu advancedOperationsMenu = new Menu("Advanced borders");

        MenuItem cannyItem = new MenuItem("Canny");
        cannyItem.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.CANNY);
            InfoBar.showCanny();
        });

        MenuItem susanItem = new MenuItem("S.U.S.A.N");
        susanItem.setOnAction(e -> MainWindow.setCurrentState(Mode.SUSAN));

        MenuItem houghItem = new MenuItem("Hough");
        houghItem.setOnAction(
            e -> {
                MainWindow.setCurrentState(Mode.HOUGH);
                InfoBar.showHoughInput();
            });


        MenuItem levelSetItem = new MenuItem("Level Set");
        levelSetItem.setOnAction(e -> {
            InfoBar.showFilterInput("Input max iterations:");
            MainWindow.setCurrentState(Mode.LEVEL_SET);
        });

        MenuItem levelSetVideoItem = new MenuItem("Level Set - Video");
        levelSetVideoItem.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.LEVEL_SET_VIDEO);
            InfoBar.showFilterInput("Input max iterations:");
        });

        final MenuItem harrisItem = new MenuItem("Harris");
        harrisItem.setOnAction(e -> {
            InfoBar.showFilterInput("Threshold");
            MainWindow.setCurrentState(Mode.HARRIS);
        });

        final MenuItem siftItem = new MenuItem("SIFT");
        siftItem.setOnAction(e -> {
            InfoBar.showFilterInput("Threshold");
            MainWindow.setCurrentState(Mode.SIFT_1);
        });

      advancedOperationsMenu.getItems()
          .addAll(cannyItem, susanItem, houghItem, levelSetItem, levelSetVideoItem, harrisItem, siftItem);

        menus.add(advancedOperationsMenu);
        return menus;
    }
}
