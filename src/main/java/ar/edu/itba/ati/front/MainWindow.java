package ar.edu.itba.ati.front;

import ar.edu.itba.ati.back.models.Image;
import ar.edu.itba.ati.back_2.adapter.*;
import ar.edu.itba.ati.back_2.models.IntPoint;
import ar.edu.itba.ati.front.menus.OperationsBar;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.util.*;

public class MainWindow extends Application {

    private static final ImagesUnaryOperatorsAdapter unaryOperators = new ImagesUnaryOperatorsAdapter();
    private static final ImagesNoiseOperatorsAdapter noiseOperators = new ImagesNoiseOperatorsAdapter();
    private static final ImagesFiltersOperatorsAdapter filtersOperators = new ImagesFiltersOperatorsAdapter();
    private static final ImagesThresholdsOperatorsAdapter thresholdsOperators = new ImagesThresholdsOperatorsAdapter();
    private static final ImagesUtilsAdapter utilsAdapters = new ImagesUtilsAdapter();
    private static final ImagesFileHandlersAdapter fileHandlers = new ImagesFileHandlersAdapter();
    private static ImagesTrackingOperatorsAdapter trackingOperators = new ImagesTrackingOperatorsAdapter();


    // Generic parameters
    public static final List<Double> params = new ArrayList<>(4);
    public static ImageByte selectedImage;
    public static Mode currentMode;

    public static int selectedColor;

  public static double doubleParam1, doubleParam2, doubleParam3, doubleParam4, doubleParam5, doubleParam6, doubleParam7,
          doubleParam8;

  // Information for noise operations
  public static double pixelsPercentage;

  // Information for contrast operation
  public static ImageByte imageToApplyContrast = null;

    //Anisotropic Diffusion
    public static int anisotropicMethod;
    public static double anisotropicSigma;
    public static int anisotropicIterations;

    //For video display
    private static Timeline timeline;
    private static int[][] phi;
    private static long lastPrint;

    public static String imageDirectory;

    // Main Window State
    public static ImageByte binaryOperatorImage;
    public static double operatorInput;
    // Pixel information nodes
    private static Scene scene;

  private BorderPane root = new BorderPane();
    private FlowPane infoBar;

    /* package-private */
    static void borderDetectionFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.borderDetection(imageByte, (int) doubleParam1);
        ImageByteDrawer.printImage(result, "Border Detection Filter");
    }

    /**
     * UNARY
     **/

  /* package-private */
    static void negative(final ImageByte imageByte) {
        final ImageByte result = unaryOperators.negative(imageByte);
        ImageByteDrawer.printImage(result, "Negative");
    }

    public static void setParam(int index, double value) {
        System.out.println(String.format("Insertando %f en %d", value, index));
        params.add(index, value);
        //params.set(index, value);
    }

    public static void setCurrentState(Mode mode) {
        currentMode = mode;
        TextField modeInfo = new TextField("MODE - " + mode.toString());
        modeInfo.setDisable(true);
        displayBottomInfo(modeInfo);
    }

    public static void printPixelsAmountAndAvg(ImageSelection selection, Image image) {
        InfoBar.printPixelsAmountAndAvg(selection, image);
    }

    public static void printPixelInfo(int x, int y, String value) {
        InfoBar.printPixelInfo(x, y, value);
    }

    public static void displayBottomInfo(Node info) {
        ((BorderPane) MainWindow.scene.getRoot()).setBottom(info);
    }

    public static void main(String[] args) {
            launch(args);
    }

    public static void displayBottomString(String s) {
        TextField modeInfo = new TextField(s);
        modeInfo.setDisable(true);
        displayBottomInfo(modeInfo);
    }

    public static void showHistogram(final ImageByte imageByte) {
        InfoBar.showHistogram(imageByte);
    }

    /* package-private */
    static void threshold(final ImageByte imageByte) {
        final ImageByte result = unaryOperators.threshold(imageByte, operatorInput);
        ImageByteDrawer.printImage(result, "Threshold");
    }

    /**
     * NOISE
     **/

  /* package-private */
    static void exponentialNoise(final ImageByte imageByte, final double lambda) {
        final ImageByte result = noiseOperators.exponential(imageByte, pixelsPercentage, lambda);
        ImageByteDrawer.printImage(result, "Exponential");
    }

    /* package-private */
    static void gauss(final ImageByte imageByte, final double mean, final double standardDeviation) {
        System.out.println(mean);
        System.out.println(standardDeviation);
        final ImageByte result = noiseOperators
                .gauss(imageByte, pixelsPercentage, mean, standardDeviation);
        ImageByteDrawer.printImage(result, "Gauss");
    }

    /* package-private */
    static void rayleighNoise(final ImageByte imageByte, final double psi) {
        final ImageByte result = noiseOperators.rayleigh(imageByte, pixelsPercentage, psi);
        ImageByteDrawer.printImage(result, "Rayleigh");
    }

    /* package-private */
    static void peppermintAndSaltNoise(final ImageByte imageByte, final double p0, final double p1) {
        final ImageByte result = noiseOperators.saltAndPepper(imageByte, pixelsPercentage, p0, p1);
        ImageByteDrawer.printImage(result, "Peppermint && Salt");
    }

//    public static void equalizeHistogram(Image image) {
//        InfoBar.equalizeHistogram(image);
//    }

//    public static void applyRayleighNoise(Image image, double psi) {
//        Image noisedImage = NoiseHandler.applyRayleighNoise(image, psi);
//        ImageByteDrawer.printImage(noisedImage, "Rayleigh");
//    }
//
//    public static void applyGaussNoise(Image image, double mu, double sigma) {
//        Image noisedImage = NoiseHandler.applyGaussNoise(image, mu, sigma);
//        ImageByteDrawer.printImage(noisedImage, "Gauss");
//    }
//
//    public static void applyExpMultNoise(Image image, double lambda) {
//        Image noisedImage = NoiseHandler.applyExpMultNoise(image, lambda);
//        ImageByteDrawer.printImage(noisedImage, "Exponential Multiplicative");
//    }
//
//    public static void applyPeppermintAndSaltNoise(Image image, double p0, double p1) {
//        Image noisedImage = NoiseHandler.applyPeppermintAndSaltNoise(image, p0, p1);
//        ImageByteDrawer.printImage(noisedImage, "Peppermint and Salt");
//    }

    /**
     * FILTERS
     **/

  /* package-private */
    static void gaussFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.gauss(imageByte, (int) doubleParam1);
        ImageByteDrawer.printImage(result, "Gauss Filter");
    }

    /* package-private */
    static void medianFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.median(imageByte, (int) doubleParam1);
        ImageByteDrawer.printImage(result, "Median Filter");
    }

    /* package-private */
    static void averageFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.average(imageByte, (int) doubleParam1);
        ImageByteDrawer.printImage(result, "Average Filter");
    }

    /* package-private */
    static void weightedMedianFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.weightedMedian(imageByte);
        ImageByteDrawer.printImage(result, "Weighted Median Filter");
    }

    /* package-private */
    static void prewittVerticalFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.prewittVertical(imageByte, (int) doubleParam1);
        ImageByteDrawer.printImage(result, "Prewitt Vertical Filter");
    }

    /* package-private */
    static void prewittHorizontalFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.prewittHorizontal(imageByte, (int) doubleParam1);
        ImageByteDrawer.printImage(result, "Prewitt Horizontal Filter");
    }

    /* package-private */
    static void prewittSynthesizedFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.prewittSynthesized(imageByte, (int) doubleParam1);
        ImageByteDrawer.printImage(result, "Prewitt Synthesized Filter");
    }

    /* package-private */
    static void sobelFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.sobelFilter(imageByte);
        ImageByteDrawer.printImage(result, "Sobel Filter");
    }

    /* package-private */
    static void laplacianNoSlope(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.laplacianNoSlope(imageByte);
        ImageByteDrawer.printImage(result, "Laplacian No Slope Filter");
    }

    /* package-private */
    static void laplacianWithSlope(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.laplacianWithSlope(imageByte, doubleParam1);
        ImageByteDrawer.printImage(result, "Laplacian With Slope Filter");
    }

    /* package-private */
    static void anisotropicDiffusion(final ImageByte imageByte) {
        final ImageByte result = filtersOperators.anisotropicFilter(imageByte, anisotropicMethod,
                anisotropicSigma, anisotropicIterations);
        ImageByteDrawer.printImage(result, "Anisotropic Diff. " + anisotropicIterations);
    }

    /* package-private */
    static void laplacianGaussianSlope(final ImageByte imageByte) {
        final ImageByte result = filtersOperators
                .laplacianGaussianSlope(imageByte, doubleParam2, doubleParam1);
        ImageByteDrawer.printImage(result, "Laplacian Gaussian Slope Filter");
    }


    @Override
    public void start(Stage stage) throws Exception {
      scene = new Scene(root, 650, 500);

        OperationsBar.createAndSetToPane(this.root, stage);
        this.infoBar = InfoBar.configureInfoBar(this.root);
        currentMode = Mode.POINTER;

        stage.setTitle("ATI");
        stage.setScene(scene);
        stage.show();
    }

//
//    public static void borderDetectionFilter(Image image) {
//        ImageFilter filter = new BorderDetectionFilter(MainWindow.filterFirstParameter);
//        Image mediaImage = new FilterApplier().applyFilter(filter, image);
//        ImageByteDrawer.printImage(mediaImage, "Border detection filter");
//    }

//    public static void scalarProduct(Image image) {
//        PunctualOperator operator = new ScalarPunctualOperator(MainWindow.operatorInput);
//        ImagePunctualFunction punctualFunction = new ImagePunctualFunction(operator);
//        Image mediaImage = punctualFunction.apply(image);
//        ImageByteDrawer.printImage(mediaImage, "Scalar product");
//    }
//
//    public static void contrastConstantsFinder(Image image) {
//        final Optional<Band> band = image.getBand(Color.GRAY);
//
//        if (!band.isPresent()) {
//            return;
//        }
//
//        final Pair constants = ContrastIncreaseConstantsFinder.findConstants(band.get());
//
//        InfoBar.setContrastValueR1(constants.getValue1());
//        InfoBar.setContrastValueR2(constants.getValue2());
//
//        imageToApplyContrast = image;
//    }
/*
    public static void gaussFilter(Image image) {
        ImageFilter filter = new GaussFilter(MainWindow.selectedFilterSigma);
        Image mediaImage = new FilterApplier().applyFilter(filter, image);
        ImageByteDrawer.printImage(mediaImage, "Gauss filter");
    }

    public static void borderDetectionFilter(Image image) {
        ImageFilter filter = new BorderDetectionFilter(MainWindow.selectedFilterSize);
        Image mediaImage = new FilterApplier().applyFilter(filter, image);
        ImageByteDrawer.printImage(mediaImage, "Border detection filter");
    }*/

    public static void bidPrewittBorderDetectionFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators
                .bidPrewittBorder(imageByte);
        ImageByteDrawer.printImage(result, "Bidirectional Border Prewitt detection filter");
    }

    public static void bidSobelBorderDetectionFilter(final ImageByte imageByte) {
        final ImageByte result = filtersOperators
                .bidSobelBorder(imageByte);
        ImageByteDrawer.printImage(result, "Bidirectional Border Sobel detection filter");
    }

    public static void globalThreshold(final ImageByte imageByte) {
        final ImageByte result = thresholdsOperators
                .global(imageByte);
        ImageByteDrawer.printImage(result, "Global threshold");
    }

    public static void otsuThreshold(final ImageByte imageByte) {
        final ImageByte result = thresholdsOperators
                .otsu(imageByte);
        ImageByteDrawer.printImage(result, "Otsu threshold");
    }

    public static void canny(final ImageByte imageByte, final double sigma1, final double sigma2) {
      final ImageByte result = filtersOperators
              .canny(imageByte, sigma1, sigma2);
      ImageByteDrawer.printImage(result, "Canny");
    }

    public static void susan(final ImageByte imageByte) {
        final ImageByte result = filtersOperators
                .susan(imageByte);
        ImageByteDrawer.printImage(result, "Susan");
    }

    public static void hough(final ImageByte imageByte) {
        final ImageByte result = filtersOperators
                .houghTransform(imageByte, (int) doubleParam1, (int) doubleParam2, (int) doubleParam3,
                        (int) doubleParam4, (int) doubleParam5, (int) doubleParam6, doubleParam7, doubleParam8);
        ImageByteDrawer.printImage(result, "Hough");
    }

    public static void harris(final ImageByte image) {
      final List<IntPoint> points = trackingOperators.harris(image, doubleParam1);

      final ImageByte imageWithCorners = utilsAdapters.drawPixels(image, points);
      ImageByteDrawer.printImage(imageWithCorners, "Harris");
    }

    public static void sift(ImageByte image) {
        final ImageByte result = trackingOperators.sift(binaryOperatorImage, image, doubleParam1);
        ImageByteDrawer.printImage(result, "SIFT result");
    }

    public static void levelSet(ImageByte image, ImageSelection selection) {

        final List<IntPoint> listIn = getSquarePoints(selection.getX0(), selection.getY0(),
                selection.getX1(), selection.getY1());
        final List<IntPoint> listOut = getSquarePoints(selection.getX0() - 1, selection.getY0() - 1,
                selection.getX1() + 1, selection.getY1() + 1);

        MainWindow.phi = filtersOperators.levelSet(image, (int)doubleParam1, listIn, listOut);

        ImageByte imageClone = utilsAdapters.drawPixels(image, listIn, listOut);
        Pane imagePane = ImageByteDrawer.printImage(imageClone, "Level Set", imageDirectory);

        if (currentMode == Mode.LEVEL_SET_VIDEO) {
            lastPrint = System.currentTimeMillis();

            File f = new File(imageDirectory);

            String directoryName = imageDirectory.split(f.getName())[0];

            // Not loading all the images at once, just in case there are too may frames in directory.
            List<File> files = Arrays.asList(new File(directoryName).listFiles());

            // Super complex comparator because of number sufix
            //files.sort((file1, file2) -> file1.getName().compareTo(file2.getName()));
            Collections.sort(files, new Comparator<File>() {
                public int compare(File o1, File o2) {
                    return extractInt(o1.getName()) - extractInt(o2.getName());
                }
                int extractInt(String s) {
                    String num = s.replaceAll("\\D", "");
                    // return 0 if no digits found
                    return num.isEmpty() ? 0 : Integer.parseInt(num);
                }
            });

            Iterator<File> fileIterator = files.iterator();
            timeline = new Timeline(new KeyFrame(Duration.millis(200), ev -> {
                drawVideoFrame(fileIterator, directoryName, listIn, listOut, imagePane);
                timeline.setRate(System.currentTimeMillis() - lastPrint);
                lastPrint = System.currentTimeMillis();
            }));
            timeline.setCycleCount(Animation.INDEFINITE);
            timeline.play();
        }
    }

    private static void drawVideoFrame(Iterator<File> fileIterator, String directoryName,
                                       List<IntPoint> listIn, List<IntPoint> listOut,
                                       Pane imagePane){
        if(!fileIterator.hasNext()){
            timeline.stop();
            return;
        }
        File file = fileIterator.next();

        ImageByte frameImage = fileHandlers.loadImage(directoryName + file.getName());
        MainWindow.phi = filtersOperators.levelSet(frameImage, (int)doubleParam1, listIn, listOut, MainWindow.phi);
        ImageByte imageFrameClone = utilsAdapters.drawPixels(frameImage, listIn, listOut);
        ImageByteDrawer.renderImageByte(imageFrameClone, imagePane);
    }

    public static List<IntPoint> getSquarePoints(final int xInitial, final int yInitial,
                                                  final int xEnd, final int yEnd) {

        final List<IntPoint> points = new LinkedList<>();

        for (int y = yInitial; y <= yEnd; y++) {
            points.add(new IntPoint(xInitial, y));
            points.add(new IntPoint(xEnd, y));
        }

        for (int x = xInitial + 1; x <= xEnd - 1; x++) {
            points.add(new IntPoint(x, yInitial));
            points.add(new IntPoint(x, yEnd));
        }

        return points;
    }


/*
    public static void scalarProduct(Image image) {
        PunctualOperator operator = new ScalarPunctualOperator(MainWindow.operatorInput);
        ImagePunctualFunction punctualFunction = new ImagePunctualFunction(operator);
        Image mediaImage = punctualFunction.apply(image);
        ImageByteDrawer.printImage(mediaImage, "Scalar product");
    }

    public static void threshold(Image image) {
        PunctualOperator operator = new ThresholdPunctualOperator((int) MainWindow.operatorInput);
        ImagePunctualFunction punctualFunction = new ImagePunctualFunction(operator);
        Image mediaImage = punctualFunction.apply(image);
        ImageByteDrawer.printImage(mediaImage, "Threshold");
    }

    public static void negative(Image image) {
        final PunctualOperator operator = new NegativePunctualOperator();
        final ImagePunctualFunction punctualFunction = new ImagePunctualFunction(operator);
        final Image mediaImage = punctualFunction.apply(image);
        ImageByteDrawer.printImage(mediaImage, "Negative");
    }

    public static void contrastConstantsFinder(Image image) {
        final Optional<Band> band = image.getBand(Color.GRAY);

        if (!band.isPresent()) {
            return;
        }

        final Pair constants = ContrastIncreaseConstantsFinder.findConstants(band.get());

        InfoBar.setContrastValueR1(constants.getValue1());
        InfoBar.setContrastValueR2(constants.getValue2());

        imageToApplyContrast = image;
    }

    public static void contrastApply() {
        if (imageToApplyContrast == null) {
            return;
        }
        System.out.println(MainWindow.doubleParam1);
        System.out.println(MainWindow.doubleParam2);
        System.out.println(MainWindow.doubleParam3);
        System.out.println(MainWindow.doubleParam4);
        final ContrastIncreaseOperator operator = new ContrastIncreaseOperator(
                MainWindow.doubleParam1, MainWindow.doubleParam3, MainWindow.doubleParam2, MainWindow.doubleParam4);
        final Map<Color, ContrastIncreaseOperator> functions = new HashMap<>();
        functions.put(Color.GRAY, operator);
        final ContrastIncreaseFunction punctualFunction = new ContrastIncreaseFunction(functions);
        final Image mediaImage = punctualFunction.apply(imageToApplyContrast);
        ImageByteDrawer.printImage(mediaImage, "Contrast Increase");

        imageToApplyContrast = null;
    }

    public static void subtraction(Image image) {
        IntBinaryOperator operator = (left, right) -> left - right;
        ImagePunctualBinaryOperator punctualFunction = new ImagePunctualBinaryOperator(operator);
        Image mediaImage = punctualFunction.apply(MainWindow.binaryOperatorImage, image);
        ImageByteDrawer.printImage(mediaImage, "Subtraction");
    }

    public static void addition(Image image) {
        IntBinaryOperator operator = Integer::sum;
        ImagePunctualBinaryOperator punctualFunction = new ImagePunctualBinaryOperator(operator);
        Image mediaImage = punctualFunction.apply(MainWindow.binaryOperatorImage, image);
        ImageByteDrawer.printImage(mediaImage, "Addition");
    }

    public static void product(Image image) {
        IntBinaryOperator operator = (left, right) -> left * right;
        ImagePunctualBinaryOperator punctualFunction = new ImagePunctualBinaryOperator(operator);
        Image mediaImage = punctualFunction.apply(MainWindow.binaryOperatorImage, image);
        ImageByteDrawer.printImage(mediaImage, "Product");
    }

    public static void gammaPower(final Image image) {
        final double gamma = MainWindow.operatorInput;
        final GammaPower gammaPower = new GammaPower(gamma);
        final ImagePunctualFunction imagePunctualFunction = new ImagePunctualFunction(gammaPower);
        final Image imageTransformed = imagePunctualFunction.apply(image);

        ImageByteDrawer.printImage(imageTransformed, "Gamma Power");
    }

    public static void linearTransformation(final Image image) {
        final Image imageTransformed = new LinearTransformationPunctualFunction().apply(image);

        ImageByteDrawer.printImage(imageTransformed, "Linear Transformation");
    }*/
//    public static void contrastApply() {
//        if (imageToApplyContrast == null) {
//            return;
//        }
//        System.out.println(MainWindow.doubleParam1);
//        System.out.println(MainWindow.doubleParam2);
//        System.out.println(MainWindow.doubleParam3);
//        System.out.println(MainWindow.doubleParam4);
//        final ContrastIncreaseOperator operator = new ContrastIncreaseOperator(
//                MainWindow.doubleParam1, MainWindow.doubleParam3, MainWindow.doubleParam2, MainWindow.doubleParam4);
//        final Map<Color, ContrastIncreaseOperator> functions = new HashMap<>();
//        functions.put(Color.GRAY, operator);
//        final ContrastIncreaseFunction punctualFunction = new ContrastIncreaseFunction(functions);
//        final Image mediaImage = punctualFunction.apply(imageToApplyContrast);
//        ImageByteDrawer.printImage(mediaImage, "Contrast Increase");
//
//        imageToApplyContrast = null;
//    }
//
//    public static void subtraction(Image image) {
//        IntBinaryOperator operator = (left, right) -> left - right;
//        ImagePunctualBinaryOperator punctualFunction = new ImagePunctualBinaryOperator(operator);
//        Image mediaImage = punctualFunction.apply(MainWindow.binaryOperatorImage, image);
//        ImageByteDrawer.printImage(mediaImage, "Subtraction");
//    }
//
//    public static void addition(Image image) {
//        IntBinaryOperator operator = Integer::sum;
//        ImagePunctualBinaryOperator punctualFunction = new ImagePunctualBinaryOperator(operator);
//        Image mediaImage = punctualFunction.apply(MainWindow.binaryOperatorImage, image);
//        ImageByteDrawer.printImage(mediaImage, "Addition");
//    }
//
//    public static void product(Image image) {
//        IntBinaryOperator operator = (left, right) -> left * right;
//        ImagePunctualBinaryOperator punctualFunction = new ImagePunctualBinaryOperator(operator);
//        Image mediaImage = punctualFunction.apply(MainWindow.binaryOperatorImage, image);
//        ImageByteDrawer.printImage(mediaImage, "Product");
//    }
//
//    public static void gammaPower(final Image image) {
//        final double gamma = MainWindow.operatorInput;
//        final GammaPower gammaPower = new GammaPower(gamma);
//        final ImagePunctualFunction imagePunctualFunction = new ImagePunctualFunction(gammaPower);
//        final Image imageTransformed = imagePunctualFunction.apply(image);
//
//        ImageByteDrawer.printImage(imageTransformed, "Gamma Power");
//    }
//
//    public static void linearTransformation(final Image image) {
//        final Image imageTransformed = new LinearTransformationPunctualFunction().apply(image);
//
//        ImageByteDrawer.printImage(imageTransformed, "Linear Transformation");
//    }

}
