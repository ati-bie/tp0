package ar.edu.itba.ati.front.NoiseGeneratorQuickDemo;

import ar.edu.itba.ati.back.handlers.writers.WriterManager;
import ar.edu.itba.ati.back.models.Image;
import ar.edu.itba.ati.front.ImageByteDrawer;
import ar.edu.itba.ati.front.InfoBar;
import ar.edu.itba.ati.front.Mode;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

// Static library for Navbar setup and behaviour
public abstract class NoiseToolsBar {

  private static final String initialDirectory =
      System.getProperty("user.dir") + "/src/sift/resources";
  private static WriterManager writerManager = new WriterManager();

  public static void configureNavbar(BorderPane root, Stage stage) {
    MenuBar menuBar = new MenuBar();

    // FILE I/O behaviour
    Menu fileMenu = new Menu("File");

    MenuItem openItem = new MenuItem("Open");
    openItem.setOnAction(e -> openImage(e, stage));

    MenuItem saveItem = new MenuItem("Save");
    saveItem.setOnAction(e -> saveImage(e, stage, NoiseHistogramDisplayer.selectedImage));

    fileMenu.getItems().addAll(openItem, new SeparatorMenuItem(), saveItem);

    // MODE OPTIONS
    Menu selectionMenu = new Menu("Mode");

    MenuItem pointItem = new MenuItem("Pointer");
    pointItem.setOnAction(e -> NoiseHistogramDisplayer.setCurrentState(Mode.POINTER));

    MenuItem selectItem = new MenuItem("Select");
    selectItem.setOnAction(e -> NoiseHistogramDisplayer.setCurrentState(Mode.SELECTION));

    MenuItem copyItem = new MenuItem("Copy");
    copyItem.setOnAction(e -> NoiseHistogramDisplayer.setCurrentState(Mode.COPY));

    selectionMenu.getItems().addAll(pointItem, selectItem, copyItem);

    // OPERATIONS OPTIONS
    Menu operationsMenu = new Menu("Operations");

    MenuItem subtractionItem = new MenuItem("Subtraction");
    subtractionItem.setOnAction(e -> NoiseHistogramDisplayer.setCurrentState(Mode.SUBTRACTION_1));

    MenuItem additionItem = new MenuItem("Addition");
    additionItem.setOnAction(e -> NoiseHistogramDisplayer.setCurrentState(Mode.ADDITION_1));

    MenuItem productItem = new MenuItem("Product");
    productItem.setOnAction(e -> NoiseHistogramDisplayer.setCurrentState(Mode.PRODUCT_1));

    MenuItem scalarProductItem = new MenuItem("Scalar Product");
    scalarProductItem.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.SCALAR_PRODUCT);
      InfoBar.showOperatorInput();
    });

    MenuItem thresholdItem = new MenuItem("Threshold");
    thresholdItem.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.THRESHOLD);
      InfoBar.showOperatorInput();
    });

    MenuItem negativeItem = new MenuItem("Negative");
    negativeItem.setOnAction(e -> NoiseHistogramDisplayer.setCurrentState(Mode.NEGATIVE));

    MenuItem linearTransformationItem = new MenuItem("Linear Transformation");
    linearTransformationItem
        .setOnAction(e -> NoiseHistogramDisplayer.setCurrentState(Mode.LINEAR_TRANSFORMATION));

    MenuItem gammaPowerItem = new MenuItem("Gamma Power");
    gammaPowerItem.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.GAMMA_POWER);
      InfoBar.showOperatorInput();
    });

    MenuItem contrastConstantsFinder = new MenuItem("Contrast Increase");
    contrastConstantsFinder.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.CONTRAST);
      InfoBar.showContrastInput();
    });

    operationsMenu.getItems().addAll(
        linearTransformationItem, // General for any image
        additionItem, subtractionItem, productItem, // Exercise 1 - a
        scalarProductItem,  // Exercise 1 - b
        gammaPowerItem,     // Exercise 1 - d

        negativeItem,       // Exercise 2
        contrastConstantsFinder,    // Exercise 4
        thresholdItem      // Exercise 5
    );

    //HISTOGRAM OPTIONS
    Menu histogramMenu = new Menu("Histogram");

    MenuItem histogramItem = new MenuItem("Make histogram");
    histogramItem.setOnAction(e -> NoiseHistogramDisplayer.setCurrentState(Mode.HISTOGRAM));

    MenuItem histogramEqualizationItem = new MenuItem("EQUALIZE");
    histogramEqualizationItem
        .setOnAction(e -> NoiseHistogramDisplayer.setCurrentState(Mode.EQUALIZE));

    histogramMenu.getItems().addAll(histogramItem, histogramEqualizationItem);

    // SHAPE OPTIONS
    Menu shapeMenu = new Menu("Shape");

    MenuItem circleItem = new MenuItem("Circle");
    circleItem.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.CIRCLE);
      InfoBar.showColorInput();
    });

    MenuItem rectangleItem = new MenuItem("Square");
    rectangleItem.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.SQUARE);
      InfoBar.showColorInput();
    });

    shapeMenu.getItems().addAll(circleItem, rectangleItem);

    // NOISES OPTIONS
    Menu noisesMenu = new Menu("Noises");

    MenuItem rayleighNoise = new MenuItem("Rayleigh");
    rayleighNoise.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.NOISE_RAYLEIGH);
      InfoBar.showRayleighInput();
    });

    MenuItem gaussNoise = new MenuItem("Gauss");
    gaussNoise.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.NOISE_GAUSS);
      InfoBar.showGaussInput();
    });

    MenuItem expMultNoise = new MenuItem("Exponential multiplicative");
    expMultNoise.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.NOISE_EXPMULT);
      InfoBar.showExpMultInput();
    });

    MenuItem pepperSaltNoise = new MenuItem("Pepper & Salt");
    pepperSaltNoise.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.NOISE_PEPPERSALT);
      InfoBar.showPepperSaltInput();
    });

    noisesMenu.getItems().addAll(rayleighNoise, gaussNoise, expMultNoise, pepperSaltNoise);

    // NOISES DOUBLE OPTIONS
    Menu noisesDoubleMenu = new Menu("Noises-Histogram");

    MenuItem rayleighDoubleNoise = new MenuItem("Rayleigh");
    rayleighDoubleNoise.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.DOUBLE_NOISE_RAYLEIGH);
      InfoBar.showRayleighInput();
    });

    MenuItem gaussDoubleNoise = new MenuItem("Gauss");
    gaussDoubleNoise.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.DOUBLE_NOISE_GAUSS);
      InfoBar.showGaussInput();
    });

    MenuItem expMultDoubleNoise = new MenuItem("Exponential multiplicative");
    expMultDoubleNoise.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.DOUBLE_NOISE_EXPMULT);
      InfoBar.showExpMultInput();
    });

    MenuItem pepperSaltDoubleNoise = new MenuItem("Pepper & Salt");
    pepperSaltDoubleNoise.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.DOUBLE_NOISE_PEPPERSALT);
      InfoBar.showPepperSaltInput();
    });

    MenuItem applyDoubleNoise = new MenuItem("Show selected noise!");
    applyDoubleNoise.setOnAction(e -> {
      NoiseHistogramDisplayer.applySelectedNoise();
    });

    noisesDoubleMenu.getItems().addAll(rayleighDoubleNoise, gaussDoubleNoise,
        expMultDoubleNoise, pepperSaltDoubleNoise, applyDoubleNoise);

    //FILTER OPTIONS
    Menu filterMenu = new Menu("Filter");

    MenuItem mediaFilter = new MenuItem("Media filter");
    mediaFilter.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.FILTER_AVERAGE);
      InfoBar.showFilterInput("Select filter window size:");
    });

    MenuItem medianFilter = new MenuItem("Median filter");
    medianFilter.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.FILTER_MEDIAN);
      InfoBar.showFilterInput("Select filter window size:");
    });

    MenuItem weightedMedianFilter = new MenuItem("Weighted Median filter");
    weightedMedianFilter.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.FILTER_WEIGHTED_MEDIAN);
      InfoBar.clearDisplay();
    });

    MenuItem gaussFilter = new MenuItem("Gauss filter");
    gaussFilter.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.FILTER_GAUSS);
      InfoBar.showFilterInput("Select SIGMA value:");
    });
//
//    MenuItem prewittVerticalFilter = new MenuItem("Prewitt Vertical");
//    prewittVerticalFilter.setOnAction(e ->{ NoiseHistogramDisplayer.setCurrentState(Mode.FILTER_PREWITT_VERTICAL); InfoBar.showFilterInput("Select filter window size:");});
//
//    MenuItem prewittHorizontalFilter = new MenuItem("Prewitt Horizontal");
//    prewittVerticalFilter.setOnAction(e ->{ NoiseHistogramDisplayer.setCurrentState(Mode.FILTER_PREWITT_HORIZONTAL); InfoBar.showFilterInput("Select filter window size:");});

    MenuItem borderDetectionFilter = new MenuItem("Border Detection filter");
    borderDetectionFilter.setOnAction(e -> {
      NoiseHistogramDisplayer.setCurrentState(Mode.FILTER_BORDER_DETECTION);
      InfoBar.showFilterInput("Select filter window size:");
    });

    filterMenu.getItems().addAll(mediaFilter, medianFilter, weightedMedianFilter, gaussFilter,
        borderDetectionFilter);

    menuBar.getMenus()
        .addAll(fileMenu, selectionMenu, operationsMenu, histogramMenu, shapeMenu, noisesMenu,
            noisesDoubleMenu, filterMenu);

    root.setTop(menuBar);
  }

  private static void openImage(ActionEvent e, Stage stage) {
    File file = openFileChooser(stage);
    if (file != null) {
      ImageByteDrawer.printFileImage(file);
    }
  }

  private static void saveImage(ActionEvent e, Stage stage, Image image) {
    File file = saveFileChooser(stage);
    if (file != null) {
      System.out.println(String.format("Saving: %s", file.getPath()));
      writerManager.writeImage(image, file.getPath());
    }
  }

  private static File openFileChooser(final Stage stage) {
    final FileChooser fileChooser = new FileChooser();

    fileChooser.setTitle("Open image file");
    fileChooser.setInitialDirectory(new File(initialDirectory));
    fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("Image Files", "*.ppm", "*.PPM", "*.pgm", "*.PGM", "*.raw",
            "*.RAW"));
    final File file = fileChooser.showOpenDialog(stage);

    return file;
  }

  private static File saveFileChooser(Stage stage) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Save image file");
    fileChooser.setInitialDirectory(new File(initialDirectory));
    fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("Image Files", "*.ppm", "*.PPM", "*.pgm", "*.PGM", "*.raw",
            "*.RAW"));
    final File file = fileChooser.showSaveDialog(stage);

    return file;
  }
}
