package ar.edu.itba.ati.front;

import ar.edu.itba.ati.back.models.Band;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.adapter.ImagesUtilsAdapter;
import ar.edu.itba.ati.back_2.models.ChannelType;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class HistogramHandler {

  private static final ImagesUtilsAdapter imagesUtils = new ImagesUtilsAdapter();

  private HistogramHandler() {
  }

  /* package-private */
  static Pane createHistogramsInAPane(final ImageByte imageByte) {
    final Map<ChannelType, Map<Integer, Double>> histograms = imagesUtils
        .getHistogramPerChannelType(imageByte);

    final Pane flowPane = new FlowPane();
    flowPane.setPrefSize(1200, 500);

    for (final ChannelType channelType : histograms.keySet()) {
      final CategoryAxis xAxis = new CategoryAxis();
      xAxis.setLabel("Range");
      final NumberAxis yAxis = new NumberAxis();
      yAxis.setLabel("Population");

      final BarChart<String, Number> barChart = new BarChart<>(xAxis, yAxis);
      barChart.setCategoryGap(10);
      barChart.setBarGap(10);
      barChart.setHorizontalGridLinesVisible(false);
      barChart.setHorizontalZeroLineVisible(false);
      barChart.setPrefSize(400, 400);

      final XYChart.Series series = new XYChart.Series();
      series.setName(channelType + " Percentage");

      final Map<Integer, Double> channelTypeHistogram = histograms.get(channelType);
      final IntPair pair = getMinMaxFromHistogram(channelTypeHistogram);
      for (int i = pair.getMin(); i <= pair.getMax(); i++) {
        if (channelTypeHistogram.containsKey(i)) {
          series.getData().add(new Data<>(i + "", channelTypeHistogram.get(i)));
        } else {
          series.getData().add(new XYChart.Data(i + "", 0));
        }
      }
      barChart.getData().addAll(series);

      final Label colorLabel = new Label(channelType + " Histogram");
      VBox vBox = new VBox();
      vBox.getChildren().addAll(colorLabel, barChart);

      flowPane.getChildren().add(vBox);
    }

    return flowPane;
  }

  private static IntPair getMinMaxFromHistogram(final Map<Integer, Double> histogram) {
    final Integer minKey = histogram.keySet().stream().min(Comparator.naturalOrder())
        .orElseThrow(IllegalStateException::new);
    final Integer maxKey = histogram.keySet().stream().max(Comparator.naturalOrder())
        .orElseThrow(IllegalStateException::new);

    return new IntPair(minKey, maxKey);
  }

  public static Pane drawHistogram(Image image) {
    Pane root = new FlowPane();
    root.setPrefSize(1200, 500);

    Map<Color, Map<Integer, Double>> histograms = calculateHistogram(image);

    for (Color color : histograms.keySet()) {
      Label labelInfo = new Label();
      labelInfo.setText(color.toString() + " Histogram");

      Map<Integer, Double> histogramMap = histograms.get(color);
      final CategoryAxis xAxis = new CategoryAxis();
      final NumberAxis yAxis = new NumberAxis();
      final BarChart<String, Number> barChart =
          new BarChart<>(xAxis, yAxis);
      barChart.setCategoryGap(10);
      barChart.setBarGap(10);
      barChart.setHorizontalGridLinesVisible(false);
      barChart.setHorizontalZeroLineVisible(false);
      barChart.setPrefSize(400, 400);
      xAxis.setLabel("Range");
      yAxis.setLabel("Population");

      XYChart.Series series1 = new XYChart.Series();
      series1.setName(color.toString() + " percentage");
      for (int i = 0; i < 256; i++) {
        if (histogramMap.containsKey(i)) {
          series1.getData().add(new XYChart.Data(i + "", histogramMap.get(i)));
        } else {
          series1.getData().add(new XYChart.Data(i + "", 0));
        }
      }

      barChart.getData().addAll(series1);

      VBox vBox = new VBox();
      vBox.getChildren().addAll(labelInfo, barChart);

      root.getChildren().add(vBox);
    }
    return root;
  }

  public static Image equalizeHistogram(Image image) {

    Map<Color, Map<Integer, Double>> histograms = calculateHistogram(image);

    List<Band> newBands = new LinkedList<>();
    for (Color color : histograms.keySet()) {
      Map<Integer, Double> histogramMap = histograms.get(color);
      Map<Integer, Integer> transformation = new HashMap<>();

      double lowestFrequency = histogramMap.get(Collections.min(histogramMap.keySet()));

      //Gets the transformation for each pixel value and loads it into the map
      for (Integer pixelValue : histogramMap.keySet()) {
        double acumFreq = 0;
        for (int k = 0; k <= pixelValue; k++) {
          if (histogramMap.containsKey(k)) {
            acumFreq += histogramMap.get(k);
          }
        }
        int equalizedValue = (int) Math
            .floor(((acumFreq - lowestFrequency) / (1 - lowestFrequency)) * 255);
        transformation.put(pixelValue, equalizedValue);
      }

      //Generates a new matrix with the transformed values
      byte[][] equalizedMatrix = new byte[image.getHeight()][image.getWidth()];

      for (int i = 0; i < image.getHeight(); i++) {
        for (int j = 0; j < image.getWidth(); j++) {
          int pixelValue = image.getBand(color).get().getFromPixel(i, j) & 0xFF;
          equalizedMatrix[i][j] = (byte) ((int) transformation.get(pixelValue));
        }
      }

      Band newBand = new Band(equalizedMatrix, color);
      newBands.add(newBand);
    }

    return new Image(newBands);
  }

  //Returns the RELATIVE FREQUENCY by band for each pixel in the image
  private static Map<Color, Map<Integer, Double>> calculateHistogram(final Image image) {
    final Map<Color, Map<Integer, Double>> histograms = new HashMap<>();
    final int imageSize = image.getWidth() * image.getHeight();

    for (final Band band : image.getBandsList()) {
      final Map<Integer, Double> pixelsBandMap = new HashMap<>();

      for (int x = 0; x < image.getHeight(); x++) {
        for (int y = 0; y < image.getWidth(); y++) {
          final int pixelValue = band.getFromPixel(x, y) & 0xFF;
          final double valueCount;

          if (pixelsBandMap.containsKey(pixelValue)) {
            valueCount = pixelsBandMap.get(pixelValue) + 1.0;
          } else {
            valueCount = 1;
          }
          pixelsBandMap.put(pixelValue, valueCount);
        }
      }

      for (final Integer pixelValue : pixelsBandMap.keySet()) {
        pixelsBandMap.put(pixelValue, pixelsBandMap.get(pixelValue) / imageSize);
      }
      histograms.put(band.getColor(), pixelsBandMap);
    }

    return histograms;
  }
}
