package ar.edu.itba.ati.front;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;


public class ImgMouseEventHandler {

  private int startingX, startingY;

  private Rectangle newRectangle = null;
  private boolean isBeingDrawn = false;
  private ImageByte image;
  private Pane gridPane;
  private String fileName;

    public ImgMouseEventHandler(final ImageByte imageByte, final Pane originalImageGrid, String fileName) {
        this.image = imageByte;
        this.gridPane = originalImageGrid;
        this.fileName = fileName;
        MainWindow.selectedImage = imageByte;
    }

  public void setImgMouseHandlers(Pane pane) {

    pane.setOnMousePressed((MouseEvent event) ->
    {
      startingX = (int) event.getSceneX();
      startingY = (int) event.getSceneY();
      MainWindow.selectedImage = image;

      if (MainWindow.currentMode != Mode.LEVEL_SET && MainWindow.currentMode != Mode.LEVEL_SET_VIDEO) {
        ImageActionHandler.triggerAction(this.image);
      }else if (!isBeingDrawn) {
        startingX = (int) event.getSceneX();
        startingY = (int) event.getSceneY();
        newRectangle = new Rectangle();
        newRectangle.setStroke(Color.RED);
        newRectangle.setFill(Color.TRANSPARENT);
        newRectangle.setVisible(true);

        pane.getChildren().add(newRectangle);

        isBeingDrawn = true;
      }
    });

    pane.setOnMouseDragged((MouseEvent event) ->
    {
      if (isBeingDrawn) {
        adjustProperties(startingX,
                startingY,
                (int) event.getX(),
                (int) event.getY(),
                newRectangle);
      }
    });

    pane.setOnMouseReleased((MouseEvent event) ->
    {
      if (isBeingDrawn) {
        newRectangle.setVisible(false);
        pane.getChildren().remove(newRectangle);
        newRectangle = null;
        isBeingDrawn = false;

        ImageSelection selection = new ImageSelection(startingY, startingX,
                (int) event.getY(), (int) event.getX(), image.getWidth(), image.getHeight());
        if(MainWindow.currentMode == Mode.LEVEL_SET || MainWindow.currentMode == Mode.LEVEL_SET_VIDEO){
            MainWindow.imageDirectory = fileName;
          ImageActionHandler.triggerAction(image, selection);
        }
      }
    });

  }



//      if (MainWindow.currentMode == Mode.SELECTION || MainWindow.currentMode == Mode.COPY
//                    || MainWindow.currentMode == Mode.CIRCLE || MainWindow.currentMode == Mode.SQUARE) {
//                newRectangle = new Rectangle();
//                newRectangle.setStroke(javafx.scene.paint.Color.RED);
//                newRectangle.setFill(javafx.scene.paint.Color.TRANSPARENT);
//                newRectangle.setVisible(true);
//                pane.getChildren().add(newRectangle);
//            }
//        });


//
//        pane.setOnMouseReleased((MouseEvent event) ->
//        {
//            ImageSelection selection = new ImageSelection(startingY, startingX,
//                    (int) event.getY(), (int) event.getX(), image.getWidth(), image.getHeight());
//
//            if (MainWindow.currentMode == Mode.CIRCLE) {
//                this.drawCircle(selection, MainWindow.selectedColor);
//            } else if (MainWindow.currentMode == Mode.SQUARE) {
//                this.drawSquare(selection, MainWindow.selectedColor);
//            } else if (MainWindow.currentMode == Mode.SELECTION || MainWindow.currentMode == Mode.COPY) {
//                newRectangle.setVisible(false);
//                pane.getChildren().remove(newRectangle);
//                newRectangle = null;
//            }
//            if (MainWindow.currentMode == Mode.SELECTION) {
//                MainWindow.printPixelsAmountAndAvg(selection, image);
//            } else if (MainWindow.currentMode == Mode.COPY) {
//                MainWindow.copyData = new ImageCopyData(image, selection);
//                MainWindow.setCurrentState(Mode.PASTE);
//            }
//        });

//    private void drawCircle(ImageSelection selection, int selectedColor) {
//
//        if (!image.getBand(Color.GRAY).isPresent()) {
//            MainWindow.displayBottomString("I can only draw circles over gray pictures right now =/ ");
//            return;
//        }
//
//        CircleGenerator generator = new CircleGenerator();
//
//        int xCenter = (selection.getX1() + selection.getX0()) / 2;
//        int yCenter = (selection.getY1() + selection.getY0()) / 2;
//        generator.drawCircle(image, (byte) selectedColor, xCenter, yCenter, selection.getX1() - xCenter);
//
//        gridPane.getChildren().clear();
//
//        ImageDrawer.renderImage(image, gridPane);
//    }
//
//    private void drawSquare(ImageSelection selection, int selectedColor) {
//        if (!image.getBand(Color.GRAY).isPresent()) {
//            MainWindow.displayBottomString("I can only draw rectangles over gray pictures right now =/ ");
//            return;
//        }
//
//        SquareGenerator generator = new SquareGenerator();
//
//        int xCenter = (selection.getX1() + selection.getX0()) / 2;
//        int yCenter = (selection.getY1() + selection.getY0()) / 2;
//        generator.drawSquare(image, (byte) selectedColor, xCenter, yCenter,
//                Math.min(selection.getX1() - selection.getX0(), selection.getY1() - selection.getY0()));
//
//        gridPane.getChildren().clear();
//
//        ImageDrawer.renderImage(image, gridPane);
//    }
//
//    private void pasteSelection(ImageCopyData copyData, int X, int Y) {
//        Image sourceImage = copyData.getImage();
//        ImageSelection selection = copyData.getSelection();
//
//        boolean isGrayscale = image.getBand(Color.GRAY).isPresent();
//        Color[] rgb = new Color[]{Color.RED, Color.GREEN, Color.BLUE};
//
//        for (int i = selection.getX0(); i < selection.getX1() && i + X - selection.getX0() < image.getHeight(); i++) {
//            for (int j = selection.getY0(); j < selection.getY1() && j + Y - selection.getY0() < image.getWidth(); j++) {
//                if (isGrayscale) {
//                    byte value = sourceImage.getBand(Color.GRAY).get().getFromPixel(i, j);
//                    image.getBand(Color.GRAY).get().putInPixel(value, i + X - selection.getX0(), j + Y - selection.getY0());
//                } else {
//                    for (Color band : rgb) {
//                        byte value = sourceImage.getBand(band).get().getFromPixel(i, j);
//                        image.getBand(band).get().putInPixel(value, i + X - selection.getX0(), j + Y - selection.getY0());
//                    }
//                }
//            }
//        }
//
//        gridPane.getChildren().clear();
//
//        ImageDrawer.renderImage(image, gridPane);
//
//    }
//
//    private void displayPixelInfo(int startingX, int startingY) {
//        String pixelColor;
//        if (image.getBand(Color.GRAY).isPresent()) {
//            pixelColor = String.format("(%d)", image.getBand(Color.GRAY).get().getFromPixel(startingY, startingX) & 0xFF);
//        } else {
//            pixelColor = String.format("(%d , %d, %d )",
//                    image.getBand(Color.RED).get().getFromPixel(startingX, startingY) & 0xFF,
//                    image.getBand(Color.GREEN).get().getFromPixel(startingX, startingY) & 0xFF,
//                    image.getBand(Color.BLUE).get().getFromPixel(startingX, startingY) & 0xFF);
//        }
//
//        MainWindow.printPixelInfo(startingX, startingY, pixelColor);
//    }
//
    private void adjustProperties(int starting_point_x, int starting_point_y,
                                  int ending_point_x, int ending_point_y,
                                  Rectangle given_rectangle) {
        given_rectangle.setX(starting_point_x);
        given_rectangle.setY(starting_point_y);
        given_rectangle.setWidth(ending_point_x - starting_point_x);
        given_rectangle.setHeight(ending_point_y - starting_point_y);

        if (given_rectangle.getWidth() < 0) {
            given_rectangle.setWidth(-given_rectangle.getWidth());
            given_rectangle.setX(given_rectangle.getX() - given_rectangle.getWidth());
        }

        if (given_rectangle.getHeight() < 0) {
            given_rectangle.setHeight(-given_rectangle.getHeight());
            given_rectangle.setY(given_rectangle.getY() - given_rectangle.getHeight());
        }
    }
}
