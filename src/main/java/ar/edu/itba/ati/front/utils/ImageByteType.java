package ar.edu.itba.ati.front.utils;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.models.ChannelType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public enum ImageByteType {
  GREYSCALE(Collections.singletonList(ChannelType.GREY)),
  RGB(Arrays.asList(ChannelType.RED, ChannelType.GREEN, ChannelType.BLUE));

  private final List<ChannelType> channelTypes;

  ImageByteType(final List<ChannelType> channelTypes) {
    this.channelTypes = channelTypes;
  }

  public static ImageByteType getImageByteType(final ImageByte imageByte) {
    Objects.requireNonNull(imageByte, "imageByte cannot be null");

    final Collection<ChannelType> imageChannelTypes = imageByte.getChannelTypes();
    for (final ImageByteType imageByteType : ImageByteType.values()) {
      if (areTheSameSize(imageChannelTypes, imageByteType.channelTypes) &&
          areMutuallyIncluded(imageChannelTypes, imageByteType.channelTypes)) {
        return imageByteType;
      }
    }

    throw new IllegalArgumentException("ImageByteType does not exist for this imageByte");
  }

  private static boolean areTheSameSize(final Collection<ChannelType> firstChannelTypes,
      final Collection<ChannelType> secondChannelTypes) {

    return firstChannelTypes.size() == secondChannelTypes.size();
  }

  private static boolean areMutuallyIncluded(final Collection<ChannelType> firstChannelTypes,
      final Collection<ChannelType> secondChannelTypes) {

    return firstChannelTypes.containsAll(secondChannelTypes) &&
        secondChannelTypes.containsAll(firstChannelTypes);
  }
}
