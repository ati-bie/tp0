package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.front.InfoBar;
import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import java.util.List;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * Created by Marco on 10/12/2017.
 */
public class ShapesMenuPopulator implements MenuPopulator {

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {
        Menu shapeMenu = new Menu("Shape");

        MenuItem circleItem = new MenuItem("Circle");
        circleItem.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.CIRCLE);
            InfoBar.showColorInput();
        });

        MenuItem rectangleItem = new MenuItem("Square");
        rectangleItem.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.SQUARE);
            InfoBar.showColorInput();
        });

        shapeMenu.getItems().addAll(circleItem, rectangleItem);

        menus.add(shapeMenu);
        return menus;
    }
}
