package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import java.util.List;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * Created by Marco on 10/12/2017.
 */
public class BinaryMenuPopulator implements MenuPopulator {

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {

        final Menu binaryOperationsMenu = new Menu("Binary");

        MenuItem subtractionItem = new MenuItem("Subtraction");
        subtractionItem.setOnAction(e -> MainWindow.setCurrentState(Mode.SUBTRACTION_1));

        MenuItem additionItem = new MenuItem("Addition");
        additionItem.setOnAction(e -> MainWindow.setCurrentState(Mode.ADDITION_1));

        MenuItem productItem = new MenuItem("Product");
        productItem.setOnAction(e -> MainWindow.setCurrentState(Mode.PRODUCT_1));

        binaryOperationsMenu.getItems().addAll(additionItem, subtractionItem, productItem);

        menus.add(binaryOperationsMenu);
        return menus;
    }
}
