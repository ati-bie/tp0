package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.front.InfoBar;
import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import ar.edu.itba.ati.front.PlayGround.CameraDrawer;
import ar.edu.itba.ati.front.PlayGround.CameraLevelSetDrawer;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.util.List;

/**
 * Created by Marco on 10/12/2017.
 */
public class PlaygroundMenuPopulator implements MenuPopulator {

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {

        final Menu binaryOperationsMenu = new Menu("Playground");

        MenuItem cameraColorIcon = new MenuItem("Camera color");
        cameraColorIcon.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.CAMERA);
            CameraDrawer.openCamera(true);
        });

        MenuItem cameraGrayIcon = new MenuItem("Camera grayscale");
        cameraGrayIcon.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.CAMERA);
            CameraDrawer.openCamera(false);
        });

        MenuItem cameraLevelSet = new MenuItem("Camera Level Set! =)");
        cameraLevelSet.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.CAMERA);
            InfoBar.showFilterInput("Input max iterations:");
            CameraLevelSetDrawer.openCamera();
        });

        binaryOperationsMenu.getItems().addAll(cameraColorIcon, cameraGrayIcon, cameraLevelSet);

        menus.add(binaryOperationsMenu);
        return menus;
    }
}
