package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.front.InfoBar;
import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import java.util.List;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * Created by Marco on 10/12/2017.
 */
public class FilterMenuPopulator implements MenuPopulator {

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {

        final MenuItem mediaFilter = new MenuItem("Avg. filter");
        mediaFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_AVERAGE);
            InfoBar.showFilterInput("Select filter window size:");
        });

        final MenuItem medianFilter = new MenuItem("Median filter");
        medianFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_MEDIAN);
            InfoBar.showFilterInput("Select filter window size:");
        });

        final MenuItem weightedMedianFilter = new MenuItem("Weighted Median filter");
        weightedMedianFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_WEIGHTED_MEDIAN);
            InfoBar.clearDisplay();
        });

        final MenuItem gaussFilter = new MenuItem("Gauss filter");
        gaussFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_GAUSS);
            InfoBar.showFilterInput("Select SIGMA value:");
        });

        final MenuItem borderDetectionFilter = new MenuItem("Border Detection filter");
        borderDetectionFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_BORDER_DETECTION);
            InfoBar.showFilterInput("Select filter window size:");
        });

        final MenuItem prewittVerticalFilter = new MenuItem("Prewitt Vertical");
        prewittVerticalFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_PREWITT_VERTICAL);
            InfoBar.showFilterInput("Select filter window size:");
        });

        final MenuItem prewittHorizontalFilter = new MenuItem("Prewitt Horizontal");
        prewittHorizontalFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_PREWITT_HORIZONTAL);
            InfoBar.showFilterInput("Select filter window size:");
        });

        final MenuItem prewittSynthesizedFilter = new MenuItem("Prewitt Synthesized");
        prewittSynthesizedFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_PREWITT_SYNTHESIZED);
            InfoBar.showFilterInput("Select filter window size:");
        });

        final MenuItem sobelFilter = new MenuItem("Sobel");
        sobelFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_SOBEL);
            InfoBar.showFilterInput("Select filter window size:");
        });

        final MenuItem laplacianNoSlopeFilter = new MenuItem("Laplacian No Slope");
        laplacianNoSlopeFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_LAPLACIAN_NO_SLOPE);
            InfoBar.showFilterInput();
        });

        final MenuItem laplacianWithSlopeFilter = new MenuItem("Laplacian With Slope");
        laplacianWithSlopeFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_LAPLACIAN_WITH_SLOPE);
            InfoBar.showFilterInput("Select slope threshold:");
        });

        final MenuItem laplacianGaussianSlopeFilter = new MenuItem("Laplacian Gaussian Slope");
        laplacianGaussianSlopeFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_LAPLACIAN_GAUSSIAN_SLOPE);
            InfoBar.showFilterInput("Select sigma value:", "Select slope threshold:");
        });

        final MenuItem anisotropicFilter = new MenuItem("Anisotropic Diffusion Filter");
        anisotropicFilter.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_ANISOTRPIC_DIFFUSION);
            InfoBar.showAnisotropicDiffusion();
        });

        final MenuItem bidirectionalBorderDetectionFilterP  = new MenuItem("Bidirectional Prewitt Border Detection filter");
        bidirectionalBorderDetectionFilterP.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_BID_PREWITT_BORDER_DETECTION);
            InfoBar.clearDisplay();
        });

        final MenuItem bidirectionalBorderDetectionFilterS  = new MenuItem("Bidirectional Sobel Border Detection filter");
        bidirectionalBorderDetectionFilterS.setOnAction(e -> {
            MainWindow.setCurrentState(Mode.FILTER_BID_SOBEL_BORDER_DETECTION);
            InfoBar.clearDisplay();
        });

        final Menu filterMenu = new Menu("Filter");
        filterMenu.getItems().addAll(mediaFilter, medianFilter, weightedMedianFilter, gaussFilter,
                borderDetectionFilter, prewittVerticalFilter, prewittHorizontalFilter,
                prewittSynthesizedFilter, sobelFilter, laplacianNoSlopeFilter, laplacianWithSlopeFilter,
                laplacianGaussianSlopeFilter, anisotropicFilter, bidirectionalBorderDetectionFilterP, bidirectionalBorderDetectionFilterS);

        menus.add(filterMenu);
        return menus;
    }
}
