package ar.edu.itba.ati.front.NoiseGeneratorQuickDemo;

import static java.lang.Math.random;

import ar.edu.itba.ati.back.generators.random.ExponentialRandomGenerator;
import ar.edu.itba.ati.back.generators.random.GaussianRandomGenerator;
import ar.edu.itba.ati.back.generators.random.RayleighRandomGenerator;
import ar.edu.itba.ati.back.interfaces.NoiseOperator;
import ar.edu.itba.ati.back.interfaces.RandomGenerator;
import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import java.util.Random;

public class NoiseHandlerDoubles {

    public static double[][] applyRayleighNoise(final double psi) {
        return applyNoiseOperation(new RayleighRandomGenerator(psi), (pixel, random) -> pixel * random);
    }

    public static double[][] applyGaussNoise(final double mu, final double sigma) {
        return applyNoiseOperation(new GaussianRandomGenerator(mu, sigma), (pixel, random) -> pixel + random);
    }

    public static double[][] applyExpMultNoise(final double lambda) {
        return applyNoiseOperation(new ExponentialRandomGenerator(lambda), (pixel, random) -> pixel * random);
    }

    public static double[][] applyPeppermintAndSaltNoise(double p0, double p1) {
        return applyNoiseOperation(Math::random,
                (pixel, random) -> {
                    if (random <= p0) return 0;
                    if (random >= p1) return 255;
                    return pixel;
                });
    }

    private static double[][] applyNoiseOperation(RandomGenerator randomGenerator, NoiseOperator noiseOperator) {
        // Generates a new matrix for the result
        final double[][] newMatrix = new double[300][300];

        double random;

        for (int i = 0; i < newMatrix.length; i++) {
            for (int j = 0; j < newMatrix[0].length; j++) {

            random = randomGenerator.getNext();
            newMatrix[i][j] = noiseOperator.getNewPixel(1, random);
        }
        }
        return newMatrix;
    }



    private static Color selectBandColor(Image image) {
        Color selectedBand = Color.GRAY;
        final Random random = new Random();

        if (!image.getBand(Color.GRAY).isPresent()) {
            int colorNumber = random.nextInt(3);

            switch (colorNumber) {
                case 0:
                    selectedBand = Color.RED;
                    break;
                case 1:
                    selectedBand = Color.GREEN;
                    break;
                case 2:
                    selectedBand = Color.BLUE;
            }
        }
        return selectedBand;
    }





    private static int getIntRandom(int initialValue, int rankWidth) {
        return (int) (random() * rankWidth + initialValue);
    }
}
