package ar.edu.itba.ati.front;

import ar.edu.itba.ati.back.models.Color;
import ar.edu.itba.ati.back.models.Image;
import ar.edu.itba.ati.back_2.adapter.ImageByte;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public abstract class InfoBar {

    private static FlowPane infoBar;

    private static Pane pointerInfo;
    private static TextField pointerPixelPosition;
    private static TextField pointerPixelValue;

    private static Pane selectionInfo;
    private static TextField selectionPixelsAmount;
    private static TextField selectionPixelsAvg;

    private static Pane colorInfo;
    private static TextField inputTextField;
    private static TextField selectColor;

    private static TextField contrastValueR1;
    private static TextField contrastValueR2;
    private static TextField contrastValueY1;
    private static TextField contrastValueY2;

    private static Pane rayleighInfo;
    private static Pane gaussInfo;
    private static Pane expMultInfo;
    private static Pane pepperSaltInfo;
    private static Pane contrastInput;

    private static Pane filterInfo1;
    private static Label filterLabel1;
    private static TextField selectedFilterSize1;
    private static Pane filterInfo2;
    private static Label filterLabel2;
    private static TextField selectedFilterSize2;
    private static Label filterLabel3;
    private static TextField selectedFilterSize3;
    private static Pane filterInfo4;
    private static Label filterLabel4;
    private static Label filterLabel5;
    private static TextField filterInput4;
    private static TextField filterInput5;

    private static Pane anisotropicInfo;

    private static Pane operatorInfo;
    private static TextField operatorInput;

    public static FlowPane configureInfoBar(BorderPane root) {
        infoBar = new FlowPane();
        infoBar.setPadding(new Insets(10, 10, 10, 10));
        infoBar.setStyle("-fx-border-style: solid;");

        //Pointer info
        pointerInfo = new Pane();
        Label pointerPositionLabel = new Label("Position:");
        pointerPixelPosition = new TextField();
        pointerPixelPosition.setDisable(true);
        Label pointerValueLabel = new Label("Value:");
        pointerPixelValue = new TextField();
        pointerPixelValue.setDisable(true);
        GridPane pointerHb = new GridPane();
        pointerHb.add(pointerPositionLabel, 0, 0);
        pointerHb.add(pointerPixelPosition, 1, 0);
        pointerHb.add(pointerValueLabel, 0, 1);
        pointerHb.add(pointerPixelValue, 1, 1);
        pointerInfo.getChildren().add(pointerHb);

        // Selection info
        selectionInfo = new Pane();
        Label selectionAmountLabel = new Label("Pixels:");
        selectionPixelsAmount = new TextField();
        selectionPixelsAmount.setDisable(true);
        Label selectorAvgLabel = new Label("Average:");
        selectionPixelsAvg = new TextField("0");
        selectionPixelsAvg.setDisable(true);
        GridPane selectorHb = new GridPane();
        selectorHb.add(selectionAmountLabel, 0, 0);
        selectorHb.add(selectionPixelsAmount, 1, 0);
        selectorHb.add(selectorAvgLabel, 0, 1);
        selectorHb.add(selectionPixelsAvg, 1, 1);
        selectionInfo.getChildren().add(selectorHb);

        // Color info
        colorInfo = new Pane();
        Label colorLabel = new Label("Select color (0-255):");
        selectColor = new TextField();
        selectColor.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                int value = Integer.valueOf(newValue);
                if (value <= 255 && value >= 0) {
                    MainWindow.selectedColor = value;
                    return;
                }
            } catch (Exception e) {
            }
            MainWindow.displayBottomString("Select an integer between 0 and 255 =)");
            MainWindow.selectedColor = 0;
        });
        selectColor.setDisable(false);
        GridPane colorHb = new GridPane();
        colorHb.add(colorLabel, 0, 0);
        colorHb.add(selectColor, 1, 0);
        colorInfo.getChildren().add(colorHb);


        // Operator info
        operatorInfo = new Pane();
        Label operatorLabel = new Label("Operation value:");
        operatorInput = new TextField();
        operatorInput.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                double value = Double.valueOf(newValue);
                MainWindow.operatorInput = value;
                return;
            } catch (Exception e) {
            }
            MainWindow.displayBottomString("Input a numeric value!");
            MainWindow.selectedColor = 0;
        });
        operatorInput.setDisable(false);
        GridPane operatorHb = new GridPane();
        operatorHb.add(colorLabel, 0, 0);
        operatorHb.add(operatorInput, 1, 0);
        operatorInfo.getChildren().add(colorHb);

        // Rayleigh info
        rayleighInfo = new Pane();
        createInputInfo(rayleighInfo, "Insert psi: ");

        // Gauss info
        gaussInfo = new Pane();
        createInputInfo(gaussInfo, "Insert mu: ", "Insert sigma: ");

        // ExpMult info
        expMultInfo = new Pane();
        createInputInfo(expMultInfo, "Insert lambda: ");

        // PepperSalt info
        pepperSaltInfo = new Pane();
        createInputInfo(pepperSaltInfo, "Insert p0: ", "Insert p1: ");

        // Contrast info
        createContrastInfo();

        // WindowFilter info
        filterInfo1 = new Pane();
        filterLabel1 = new Label("Select slope threshold:");
        selectedFilterSize1 = new TextField();
        selectedFilterSize1.textProperty().addListener((observable, oldValue, newValue) -> {
            MainWindow.doubleParam1 = Double.valueOf(newValue);
        });
        selectedFilterSize1.setDisable(false);

        filterInfo2 = new Pane();
        filterLabel2 = new Label("Select sigma value:");
        selectedFilterSize2 = new TextField();
        selectedFilterSize2.textProperty().addListener((observable, oldValue, newValue) -> {
            MainWindow.doubleParam2 = Double.valueOf(newValue);
        });
        selectedFilterSize2.setDisable(false);

        filterLabel3 = new Label("Select slope threshold:");
        selectedFilterSize3 = new TextField();
        selectedFilterSize3.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                MainWindow.doubleParam1 = Double.valueOf(newValue);
            } catch (Exception ignored) {
            }
        });
        selectedFilterSize3.setDisable(false);
        GridPane filterHb1 = new GridPane();
        filterHb1.add(filterLabel1, 0, 0);
        filterHb1.add(selectedFilterSize1, 1, 0);
        filterInfo1.getChildren().add(filterHb1);

        GridPane filterHb2 = new GridPane();
        filterHb2.add(filterLabel2, 0, 0);
        filterHb2.add(selectedFilterSize2, 1, 0);
        filterHb2.add(filterLabel3, 0, 1);
        filterHb2.add(selectedFilterSize3, 1, 1);
        filterInfo2.getChildren().add(filterHb2);

        // Canny border detection
        filterInfo4 = new Pane();
        filterLabel4 = new Label("Umbral 1:");
        filterLabel5 = new Label("Umbral 2:");
        filterInput4 = new TextField();
        filterInput5 = new TextField();
        filterInput4.textProperty().addListener((observable, oldValue, newValue) -> {
            MainWindow.setParam(0, Double.valueOf(newValue));
        });
        filterInput5.textProperty().addListener((observable, oldValue, newValue) -> {
            MainWindow.setParam(1, Double.valueOf(newValue));
        });
        filterInput4.setDisable(false);
        filterInput5.setDisable(false);

        GridPane filterCannyGrid = new GridPane();
        filterCannyGrid.add(filterLabel4, 0, 0);
        filterCannyGrid.add(filterInput4, 1, 0);
        filterCannyGrid.add(filterLabel5, 0, 1);
        filterCannyGrid.add(filterInput5, 1, 1);
        filterInfo4.getChildren().add(filterCannyGrid);

        // Anisotropic Difussion
        anisotropicInfo = new Pane();

        Label anisotropicMethodLabel = new Label("Method:");
        final ComboBox anisotropicMethodInput = new ComboBox();
        anisotropicMethodInput.getItems().addAll(
            "Lorentz",
            "Leclerk",
            "Gauss Kernel"
        );
        anisotropicMethodInput.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                switch (t1){
                    case "Lorentz":
                        MainWindow.anisotropicMethod = 0;
                        break;
                    case "Leclerk":
                        MainWindow.anisotropicMethod = 1;
                        break;
                    case "Gauss Kernel":
                        MainWindow.anisotropicMethod = 2;
                        break;
                }
            }
        });

        Label anisotropicSigmaLabel = new Label("Sigma:");
        TextField anisotropicSigmaInput = new TextField();
        anisotropicSigmaInput.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                MainWindow.anisotropicSigma = Double.valueOf(newValue);
            } catch (Exception ignored) {
            }
        });

        Label anisotropicIterationsLabel = new Label("Iterations number:");
        TextField anisotropicIterationsInput = new TextField();
        anisotropicIterationsInput.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                MainWindow.anisotropicIterations = Integer.valueOf(newValue);
            } catch (Exception ignored) {
            }
        });
        GridPane anisotropicHb = new GridPane();
        anisotropicHb.add(anisotropicMethodLabel, 0, 0);
        anisotropicHb.add(anisotropicMethodInput, 1, 0);
        anisotropicHb.add(anisotropicSigmaLabel, 0, 1);
        anisotropicHb.add(anisotropicSigmaInput, 1, 1);
        anisotropicHb.add(anisotropicIterationsLabel, 0, 2);
        anisotropicHb.add(anisotropicIterationsInput, 1, 2);
        anisotropicInfo.getChildren().add(anisotropicHb);






        root.setCenter(infoBar);
        return infoBar;
    }

    private static void createContrastInfo() {
        contrastInput = new Pane();
        Label contrastLabelR1 = new Label(" r1 calculated:");
        Label contrastLabelR2 = new Label(" r2 calculated:");
        Label contrastLabelY1 = new Label(" Insert y1:");
        Label contrastLabelY2 = new Label(" Insert y2:");

        contrastValueR1 = new TextField();
        contrastValueR1.textProperty().addListener((observable, oldValue, newValue) -> MainWindow.doubleParam1 = Double.valueOf(newValue));
        contrastValueR1.setDisable(true);
        contrastValueR2 = new TextField();
        contrastValueR2.textProperty().addListener((observable, oldValue, newValue) -> MainWindow.doubleParam2 = Double.valueOf(newValue));
        contrastValueR2.setDisable(true);
        contrastValueY1 = new TextField();
        contrastValueY1.textProperty().addListener((observable, oldValue, newValue) -> MainWindow.doubleParam3 = Double.valueOf(newValue));
        contrastValueY1.setDisable(false);
        contrastValueY2 = new TextField();
        contrastValueY2.textProperty().addListener((observable, oldValue, newValue) -> MainWindow.doubleParam4 = Double.valueOf(newValue));
        contrastValueY2.setDisable(false);

        final Button applyContrastButton = new Button("Apply contrast");
//        applyContrastButton.setOnAction(e -> MainWindow.contrastApply());

        GridPane contrastGrid = new GridPane();
        contrastGrid.add(contrastLabelR1, 0, 0);
        contrastGrid.add(contrastValueR1, 1, 0);
        contrastGrid.add(contrastLabelR2, 0, 1);
        contrastGrid.add(contrastValueR2, 1, 1);
        contrastGrid.add(contrastLabelY1, 2, 0);
        contrastGrid.add(contrastValueY1, 3, 0);
        contrastGrid.add(contrastLabelY2, 2, 1);
        contrastGrid.add(contrastValueY2, 3, 1);
        contrastGrid.add(applyContrastButton, 0, 2);

        contrastInput.getChildren().add(contrastGrid);
    }

    public static void createInputInfo(Pane pane, String... labels) {
        if (labels == null) {
            System.out.println("labels parameter cannot be null");
            return;
        }

        GridPane inputGrid = new GridPane();

        for (int i = 0; i < labels.length; i++) {
            int aux = i;
            Label inputLabel = new Label(labels[i]);
            inputTextField = new TextField();
            inputTextField.textProperty().addListener((observable, oldValue, newValue) -> MainWindow.setParam(aux, Double.valueOf(newValue)));
            inputTextField.setDisable(false);
            inputGrid.add(inputLabel, 0, i);
            inputGrid.add(inputTextField, 1, i);
        }

        inputGrid.add(getPixelsPercentageLabel(), 0, labels.length);
        inputGrid.add(getPixelsPercentageTextField(), 1, labels.length);

        pane.getChildren().add(inputGrid);
    }

    public static TextField getPixelsPercentageTextField() {
        TextField pixelsPercentageLabel = new TextField();
        pixelsPercentageLabel.textProperty().addListener(
            (observable, oldValue, newValue) -> MainWindow.pixelsPercentage = Double.valueOf(newValue));
        pixelsPercentageLabel.setDisable(false);
        return pixelsPercentageLabel;
    }

    public static Label getPixelsPercentageLabel() {
        return new Label("% pixels: ");
    }

    public static void showColorInput() {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(colorInfo);
    }

    public static void showRayleighInput() {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(rayleighInfo);
    }

    public static void showGaussInput() {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(gaussInfo);
    }

    public static void showExpMultInput() {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(expMultInfo);
    }

    public static void showPepperSaltInput() {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(pepperSaltInfo);
    }

    public static void showFilterInput() {
        infoBar.getChildren().clear();
    }

    public static void showFilterInput(String messageText1) {
        infoBar.getChildren().clear();
        filterLabel1.setText(messageText1);
        infoBar.getChildren().add(filterInfo1);
    }

    //Overload for 2-parameters filters
    public static void showFilterInput(String messageText1, String messageText2) {
        infoBar.getChildren().clear();
        filterLabel2.setText(messageText1);
        filterLabel3.setText(messageText2);
        infoBar.getChildren().add(filterInfo2);
    }

    public static void showCanny() {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(filterInfo4);
    }

    public static void showAnisotropicDiffusion() {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(anisotropicInfo);
    }

    public static void printPixelInfo(int x, int y, String value) {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(pointerInfo);

        pointerPixelPosition.setText(String.format("( %d, %d )", x, y));
        pointerPixelValue.setText(value);
    }

    public static void printPixelsAmountAndAvg(ImageSelection selection, Image image) {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(selectionInfo);

        int height = Math.abs((selection.getX1() - selection.getX0()));
        int width = Math.abs((selection.getY1() - selection.getY0()));
        int totalAmount = height * width;
        String amountMessage = String.format("%d", totalAmount);
        selectionPixelsAmount.setText(amountMessage);


        boolean isGreyscale = image.getBand(Color.GRAY).isPresent();

        double[] avgs = new double[]{0, 0, 0};
        for (int i = selection.getX0(); i < selection.getX1(); i++) {
            for (int j = selection.getY0(); j < selection.getY1(); j++) {
                if (isGreyscale) {
                    avgs[0] += hexToInt(image.getBand(Color.GRAY).get().getFromPixel(i, j));
                } else {
                    avgs[0] += hexToInt(image.getBand(Color.RED).get().getFromPixel(i, j));
                    avgs[1] += hexToInt(image.getBand(Color.GREEN).get().getFromPixel(i, j));
                    avgs[2] += hexToInt(image.getBand(Color.BLUE).get().getFromPixel(i, j));
                }
            }
        }

        String avgMessage;
        if (isGreyscale) {
            avgs[0] = totalAmount == 0 ? 0 : avgs[0] / totalAmount;
            avgMessage = String.format("(%f)", avgs[0]);
        } else {
            avgs[0] = totalAmount == 0 ? 0 : avgs[0] / totalAmount;
            avgs[1] = totalAmount == 0 ? 0 : avgs[1] / totalAmount;
            avgs[2] = totalAmount == 0 ? 0 : avgs[2] / totalAmount;
            avgMessage = String.format("(%.02f, %.02f, %.02f)", avgs[0], avgs[1], avgs[2]);
        }

        selectionPixelsAvg.setText(avgMessage);
    }

    private static int hexToInt(byte hexByte) {
        String strValue = String.format("%02X", hexByte);
        return Integer.parseInt(strValue, 16);
    }

    private static String hexToUnsignedInt(byte hexByte) {
        return String.format("%d", hexByte & 0xFF);
    }

    public static void showHistogram(final ImageByte imageByte) {
        final Pane histograms = HistogramHandler.createHistogramsInAPane(imageByte);

        infoBar.getChildren().clear();
        infoBar.getChildren().add(histograms);
    }

//    public static void equalizeHistogram(Image image) {
//        Image equalizedImage = HistogramHandler.equalizeHistogram(image);
//        ImageDrawer.printImage(equalizedImage, "Equalized");
//    }

    public static void clearDisplay() {
        infoBar.getChildren().clear();
    }

    public static void showOperatorInput() {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(operatorInput);
    }

    public static void showContrastInput() {
        infoBar.getChildren().clear();
        infoBar.getChildren().add(contrastInput);
    }

    public static void setContrastValueR1(final double r1) {
        contrastValueR1.textProperty().setValue(Double.valueOf(r1).toString());
    }

    public static void setContrastValueR2(final double r2) {
        contrastValueR2.textProperty().setValue(Double.valueOf(r2).toString());
    }

    public static void displayPane(Pane histWindow) {

        infoBar.getChildren().clear();
        infoBar.getChildren().add(histWindow);
    }

    public static void showHoughInput() {
        infoBar.getChildren().clear();

        final TextField rhoMinTextField = new TextField();
        rhoMinTextField.textProperty().addListener(
            (observable, oldValue, newValue) -> MainWindow.doubleParam1 = Double.valueOf(newValue));

        final TextField rhoMaxTextField = new TextField();
        rhoMaxTextField.textProperty().addListener(
            (observable, oldValue, newValue) -> MainWindow.doubleParam2 = Double.valueOf(newValue));

        final TextField rhoStepTextField = new TextField();
        rhoStepTextField.textProperty().addListener(
            (observable, oldValue, newValue) -> MainWindow.doubleParam3 = Double.valueOf(newValue));

        final TextField thetaMinTextField = new TextField();
        thetaMinTextField.textProperty().addListener(
            (observable, oldValue, newValue) -> MainWindow.doubleParam4 = Double.valueOf(newValue));

        final TextField thetaMaxTextField = new TextField();
        thetaMaxTextField.textProperty().addListener(
            (observable, oldValue, newValue) -> MainWindow.doubleParam5 = Double.valueOf(newValue));

        final TextField thetaStepTextField = new TextField();
        thetaStepTextField.textProperty().addListener(
            (observable, oldValue, newValue) -> MainWindow.doubleParam6 = Double.valueOf(newValue));

        final TextField acceptanceThresholdTextField = new TextField();
        acceptanceThresholdTextField.textProperty().addListener(
            (observable, oldValue, newValue) -> MainWindow.doubleParam7 = Double.valueOf(newValue));

        final TextField epsilonTextField = new TextField();
        epsilonTextField.textProperty().addListener(
            (observable, oldValue, newValue) -> MainWindow.doubleParam8 = Double.valueOf(newValue));

        final GridPane houghPane = new GridPane();

//        final Label rhoMinLabel = new Label(" rhoMin:");
//        final Label rhoMaxLabel = new Label(" rhoMax:");
        final Label rhoStepLabel = new Label(" rhoStep");
//        houghPane.add(rhoMinLabel, 0, 0);
//        houghPane.add(rhoMinTextField, 1, 0);
//        houghPane.add(rhoMaxLabel, 2, 0);
//        houghPane.add(rhoMaxTextField, 3, 0);
      houghPane.add(rhoStepLabel, 0, 0);
      houghPane.add(rhoStepTextField, 1, 0);

        final Label thetaMinLabel = new Label(" thetaMin");
        final Label thetaMaxLabel = new Label(" thetaMax");
        final Label thetaStepLabel = new Label(" thetaStep");
        houghPane.add(thetaMinLabel, 0, 1);
        houghPane.add(thetaMinTextField, 1, 1);

        houghPane.add(thetaMaxLabel, 2, 1);
        houghPane.add(thetaMaxTextField, 3, 1);

        houghPane.add(thetaStepLabel, 4, 1);
        houghPane.add(thetaStepTextField, 5, 1);

        final Label acceptanceThresholdLabel = new Label(" Acceptance Threshold");
        houghPane.add(acceptanceThresholdLabel, 0, 2);
        houghPane.add(acceptanceThresholdTextField, 1, 2);

        final Label epsilonLabel = new Label(" Epsilon");
        houghPane.add(epsilonLabel, 2, 2);
        houghPane.add(epsilonTextField, 3, 2);

        infoBar.getChildren().add(houghPane);
    }
}
