package ar.edu.itba.ati.front.utils;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.models.ChannelType;
import java.util.Objects;

public class ImageByteToByteArrayConverter {

  private ImageByte imageByte;

  public ImageByteToByteArrayConverter(final ImageByte imageByte) {
    Objects.requireNonNull(imageByte);
    this.imageByte = imageByte;
  }

  public byte[] getByteArrayforChannelType(final ChannelType channelType) {
    Objects.requireNonNull(imageByte);
    Objects.requireNonNull(channelType);

    final byte[] stream = new byte[imageByte.getDimensions()];
    final int width = imageByte.getWidth();

    for (int x = 0; x < imageByte.getHeight(); x++) {
      for (int y = 0; y < imageByte.getWidth(); y++) {
        stream[x * width + y] = imageByte.getPixel(channelType, x, y);
      }
    }

    return stream;
  }
}
