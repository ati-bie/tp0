package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.adapter.ImagesFileHandlersAdapter;
import ar.edu.itba.ati.front.ImageByteDrawer;
import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import javafx.event.ActionEvent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.List;

/**
 * Created by Marco on 10/12/2017.
 */
public class FileMenuPopulator implements MenuPopulator {


    private static final String initialDirectory =
            System.getProperty("user.dir") + "/src/main/resources";
    private static final ImagesFileHandlersAdapter fileHandlers = new ImagesFileHandlersAdapter();

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {


        final MenuItem openItem = new MenuItem("Open");
        openItem.setOnAction(e -> openImage(e, stage));

        final MenuItem saveItem = new MenuItem("Save");
        saveItem.setOnAction(e -> saveImage(e, stage, MainWindow.selectedImage));

        final Menu fileMenu = new Menu("File");
        fileMenu.getItems().addAll(openItem, new SeparatorMenuItem(), saveItem);

        menus.add(fileMenu);

        return menus;
    }

    private static void saveImage(ActionEvent ignored, final Stage stage, ImageByte imageByte) {
        File file = saveFileChooser(stage);
        if (file != null) {
            System.out.println(String.format("Saving: %s", file.getPath()));
            fileHandlers.writeImage(file.getPath(), imageByte);
        }
    }
    private static void openImage(final ActionEvent ignored, final Stage stage) {
        final File file = openFileChooser(stage);

        if (file != null) {
            ImageByteDrawer.printFileImage(file);
        }
    }


    private static File openFileChooser(final Stage stage) {
        final FileChooser fileChooser = new FileChooser();

        fileChooser.setTitle("Open image file");
        fileChooser.setInitialDirectory(new File(initialDirectory));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.ppm", "*.PPM", "*.pgm", "*.PGM", "*.raw",
                        "*.RAW","*.jpg","*.JPG"));
        final File file = fileChooser.showOpenDialog(stage);

        return file;
    }

    private static File saveFileChooser(Stage stage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save image file");
        fileChooser.setInitialDirectory(new File(initialDirectory));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.ppm", "*.PPM", "*.pgm", "*.PGM", "*.raw",
                        "*.RAW"));
        final File file = fileChooser.showSaveDialog(stage);

        return file;
    }

}
