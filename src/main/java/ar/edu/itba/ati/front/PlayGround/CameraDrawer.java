package ar.edu.itba.ati.front.PlayGround;

import ar.edu.itba.ati.back_2.adapter.*;
import ar.edu.itba.ati.front.ImageByteDrawer;
import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import com.github.sarxos.webcam.Webcam;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Marco on 10/12/2017.
 */
public class CameraDrawer {

  private static final PlaygroundOperatorsAdapter adapter = new PlaygroundOperatorsAdapter();
  private static long lastPrint;
  private static Pane originalImageGrid;
  private static ImageByte image;
  private static BufferedImage cameraImg;
  private static Webcam webcam;
  private static Timeline timeline;

  private static Mode cameraMode;
  private static boolean showColor;

  public static void openCamera(boolean isColor) {

    showColor = isColor;
    webcam = Webcam.getDefault();
    if(webcam.isOpen()){
      webcam.close();
    }
    webcam.setViewSize(new Dimension(640, 480));
    webcam.open();

    cameraImg = webcam.getImage();

    if(showColor) {
      image = new ImagesFileHandlersAdapter().getEmptyColorImage(cameraImg.getHeight(), cameraImg.getWidth());
    }else{
      image = new ImagesFileHandlersAdapter().getEmptyGrayscaleImage(cameraImg.getHeight(), cameraImg.getWidth());
    }

    adapter.byteStreamToImage(image, cameraImg);
    initPanel(image);

    lastPrint = System.currentTimeMillis();

    System.out.println("Camera started");

    timeline = new Timeline(new KeyFrame(Duration.millis(200), ev -> {
      drawFrame();
      timeline.setRate(System.currentTimeMillis() - lastPrint);
      lastPrint = System.currentTimeMillis();
    }));

    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.play();

  }

  private static void drawFrame() {
    cameraImg = webcam.getImage();
    adapter.byteStreamToImage(image, cameraImg);
    ImageByteDrawer.renderImageByte(CameraActionHandler.transformActual(image, cameraMode), originalImageGrid);
  }


  private static void initPanel(ImageByte imageByte) {

    Pane originalImgWindow = new Pane();
    originalImgWindow.setVisible(true);

    originalImgWindow.setOnMousePressed((MouseEvent event) ->
    {
      cameraMode = MainWindow.currentMode;
    });

    originalImageGrid = new StackPane();
    originalImgWindow.getChildren().add(originalImageGrid);
    originalImgWindow.setPrefSize(imageByte.getWidth(), imageByte.getHeight());
    ImageByteDrawer.renderImageByte(imageByte, originalImageGrid);

    Stage newStage = new Stage();
    newStage.setWidth(imageByte.getWidth());
    newStage.setHeight(imageByte.getHeight());
    Scene imageScene = new Scene(originalImgWindow);

    newStage.setScene(imageScene);
    newStage.setTitle("Say cheese!");
    newStage.show();
    newStage.sizeToScene();
    newStage.setResizable(false);

    originalImgWindow.setLayoutX(0);
    originalImgWindow.setLayoutY(0);


  }
}
