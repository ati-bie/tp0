package ar.edu.itba.ati.front.menus.menuPopulators;

import ar.edu.itba.ati.front.MainWindow;
import ar.edu.itba.ati.front.Mode;
import ar.edu.itba.ati.front.menus.MenuPopulator;
import java.util.List;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * Created by Marco on 10/12/2017.
 */
public class SelectorMenuPopulator implements MenuPopulator {

    @Override
    public List<Menu> addMenus(Stage stage, List<Menu> menus) {

        final MenuItem pointItem = new MenuItem("Pointer");
        pointItem.setOnAction(e -> MainWindow.setCurrentState(Mode.POINTER));

        final MenuItem selectItem = new MenuItem("Select");
        selectItem.setOnAction(e -> MainWindow.setCurrentState(Mode.SELECTION));

        final MenuItem copyItem = new MenuItem("Copy");
        copyItem.setOnAction(e -> MainWindow.setCurrentState(Mode.COPY));

        final Menu modeMenu = new Menu("Mode");
        modeMenu.getItems().addAll(pointItem, selectItem, copyItem);

        menus.add(modeMenu);
        return menus;
    }
}
