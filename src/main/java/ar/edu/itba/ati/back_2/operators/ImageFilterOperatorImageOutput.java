package ar.edu.itba.ati.back_2.operators;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilterImageOutput;
import java.util.Objects;

public class ImageFilterOperatorImageOutput {

  private final WindowFilterImageOutput filter;

  public ImageFilterOperatorImageOutput(final WindowFilterImageOutput filter) {
    Objects.requireNonNull(filter);

    this.filter = filter;
  }

  public Image apply(final Image grayscaleImage, final Image outputImage) {
    Objects.requireNonNull(grayscaleImage);
    Objects.requireNonNull(outputImage);

      for (int x = 0; x < grayscaleImage.getHeight(); x++) {
        for (int y = 0; y < grayscaleImage.getWidth(); y++) {
          filter.applyFilter(grayscaleImage, outputImage, x, y);
        }
    }

    return outputImage;
  }

}
