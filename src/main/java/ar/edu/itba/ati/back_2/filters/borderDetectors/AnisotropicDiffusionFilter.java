package ar.edu.itba.ati.back_2.filters.borderDetectors;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.models.ChannelType;

public class AnisotropicDiffusionFilter implements WindowFilter {

  private final int method;
  private final double sigma;

  public AnisotropicDiffusionFilter(int method, double sigma) {
    this.method = method;
    this.sigma = sigma;
  }

  @Override
  public double applyFilter(final Image image, final ChannelType channelType, final int x,
      final int y) {

    double currentPixel = image.getPixel(channelType, x,y);
    //Consider repeating border condition when the pixel is at the limit of the image
    double DnIij = image.getPixel(channelType, Math.min(image.getHeight()-1, x+1) ,y) - currentPixel;
    double DsIij = image.getPixel(channelType, Math.max(0, x-1) ,y) - currentPixel;
    double DeIij = image.getPixel(channelType, x, Math.min(image.getWidth()-1, y+1)) - currentPixel;
    double DoIij = image.getPixel(channelType, x, Math.max(0, y-1)) - currentPixel;

    double cn = getCValue(DnIij);
    double cs = getCValue(DsIij);
    double ce = getCValue(DeIij);
    double co = getCValue(DoIij);

    return currentPixel + 0.25 * (cn * DnIij + cs * DsIij
            + ce * DeIij  + co * DoIij );
  }

  /*
  0 = Lorentz
  1 = Leclerk
  2 = Gauss Kernel
   */
  private double getCValue(double dIij) {
    if(this.method == 0){
      return Math.exp(((-1)*(dIij*dIij))/(sigma*sigma));
    }else if(this.method == 1){
      return 1/(((dIij*dIij)/(sigma*sigma))+1);
    }else{
      return 1;
    }
  }
}
