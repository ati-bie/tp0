package ar.edu.itba.ati.back_2.file_handlers.writers;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.file_handlers.FileExtension;
import ar.edu.itba.ati.back_2.file_handlers.ImageFileUtils;
import java.util.Objects;

public class ImageWriterManager {

  public static void writeImageToFile(final String path, final ImageByte imageToSave) {
    Objects.requireNonNull(path, "path must not be null");
    Objects.requireNonNull(imageToSave, "imageToSave must not be null");

    final String extension = ImageFileUtils.getExtension(path);

    try {
      writeImageWithExtension(extension, path, imageToSave);
    } catch (final IllegalArgumentException e) {
      throw new IllegalArgumentException("extension "+extension +" is not supported");
    }
  }

  private static void writeImageWithExtension(final String extension, final String path,
      final ImageByte imageToSave) {

    final FileExtension fileExtension = FileExtension.valueOf(extension.toUpperCase());
    final ImageWriter imageLoader = ImageFileUtils.getImageWriter(fileExtension);

    imageLoader.writeImageToFile(imageToSave, path);
  }


}
