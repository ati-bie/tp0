package ar.edu.itba.ati.back_2.interfaces;

public interface ThresholdOperator {
    Image apply(final Image image);
}
