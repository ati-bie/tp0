package ar.edu.itba.ati.back_2.utils;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.IntPoint;
import java.util.List;
import java.util.Objects;

public final class ImageDrawer {

  private static final ChannelType[] channels = new ChannelType[]{ChannelType.RED,
      ChannelType.GREEN, ChannelType.BLUE};

  public static Image drawPixels(Image image, List<IntPoint>[] lists) {
    Objects.requireNonNull(image, "image must not be null");

    int channelIndex = 0;
    for (List<IntPoint> list : lists) {
      for (IntPoint point : list) {
        ChannelType color = channels[channelIndex];
        for (ChannelType channelType : channels) {
          image.setPixel(channelType, point.getX(), point.getY(), channelType == color ? 255 : 0);
        }
      }
      channelIndex = (channelIndex + 1) % channels.length;

    }

    return image;
  }
}
