package ar.edu.itba.ati.back_2.models;

public final class Point {

  private final double x;
  private final double y;

  public Point(final double x, final double y) {
    this.x = x;
    this.y = y;
  }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }

  public Point subtract(final Point otherPoint) {
    if (otherPoint == null) {
      throw new IllegalArgumentException("point must not be null");
    }

    return new Point(this.getX() - otherPoint.getX(), this.getY() - otherPoint.getY());
  }

  @Override
  public String toString() {
    return "Point{" +
        "x=" + x +
        ", y=" + y +
        '}';
  }
}
