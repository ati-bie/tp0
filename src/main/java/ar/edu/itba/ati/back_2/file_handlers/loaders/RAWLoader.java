package ar.edu.itba.ati.back_2.file_handlers.loaders;

import ar.edu.itba.ati.back_2.file_handlers.FileExtension;
import ar.edu.itba.ati.back_2.file_handlers.ImageFileUtils;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.utils.ChannelTypeImagesFactory;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class RAWLoader implements ImageLoader {

  @Override
  public Image loadImageFromFile(final String path) {
    Objects.requireNonNull(path, "path must not be null");

    final RAWHeader rawHeader = loadRAWHeader(path);

    return loadRAWImage(path, rawHeader);
  }

  private Image loadRAWImage(final String path, final RAWHeader RAWHeader) {
    try {
      final byte[] RAWBytes = Files.readAllBytes(Paths.get(path));

      final Image image;
      if (RAWHeader.isGreyscale) {
        image = ChannelTypeImagesFactory.getGreyscaleImage(RAWHeader.height, RAWHeader.width);
      } else {
        image = ChannelTypeImagesFactory.getRGBImage(RAWHeader.height, RAWHeader.width);
      }

      loadImageWithValues(image, RAWBytes, RAWHeader);

      return image;
    } catch (final IOException e) {
      return null;
    }
  }

  private void loadImageWithValues(final Image image, final byte[] RAWBytes,
      final RAWHeader RAWHeader) {

    if (RAWHeader.isGreyscale) {
      for (int x = 0; x < image.getHeight(); x++) {
        for (int y = 0; y < image.getWidth(); y++) {
          image.setPixel(ChannelType.GREY, x, y, RAWBytes[x * image.getWidth() + y] & 0xFF);
        }
      }
    } else {
      final List<ChannelType> channelTypes = Arrays
          .asList(ChannelType.RED, ChannelType.GREEN, ChannelType.BLUE);

      for (int channelTypeIndex = 0; channelTypeIndex < channelTypes.size(); channelTypeIndex++) {
        for (int x = 0; x < image.getHeight(); x++) {
          for (int y = 0; y < image.getWidth(); y++) {
            image.setPixel(channelTypes.get(channelTypeIndex), x, y,
                RAWBytes[(x * image.getWidth() + y) * 3 + channelTypeIndex] & 0xFF);
          }
        }
      }
    }
  }

  // Below this, lie RAW Header methods

  // TODO Create loader for RHDR file
  private RAWHeader loadRAWHeader(final String path) {
    final String RAWHeaderPath = getRAWHeaderPath(path);

    try (final BufferedReader reader = getRAWReader(RAWHeaderPath)) {
      return getRAWHeaderFromReader(reader);
    } catch (final IOException e) {
      throw new IllegalArgumentException("an error has occurred while trying to read the file");
    }

  }

  private RAWHeader getRAWHeaderFromReader(final BufferedReader reader) throws IOException {
    reader.readLine();

    final String[] attributes = reader.readLine().split(",");
    final int width = Integer.valueOf(attributes[0]);
    final int height = Integer.valueOf(attributes[1]);
    final boolean isGreyscale = Boolean.valueOf(attributes[2]);

    return new RAWHeader(height, width, isGreyscale);
  }

  private String getRAWHeaderPath(final String path) {
    return ImageFileUtils.replaceExtensionAtFilePath(path, FileExtension.RAW.toString(),
        FileExtension.RHDR.toString());
  }

  private BufferedReader getRAWReader(final String rawHeaderPath) throws FileNotFoundException {
    return new BufferedReader(new FileReader(rawHeaderPath));
  }

  private class RAWHeader {

    private final int height;
    private final int width;
    private final boolean isGreyscale;

    RAWHeader(final int height, final int width, final boolean isGreyscale) {
      this.height = height;
      this.width = width;
      this.isGreyscale = isGreyscale;
    }
  }
}
