package ar.edu.itba.ati.back_2.filters.borderDetectors;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilterImageOutput;
import ar.edu.itba.ati.back_2.models.ChannelType;

public class SusanFilter implements WindowFilterImageOutput {

  private static final int threshold = 27; // Based on paper

  private static final double epsilon = 0.1;

  private static final int[][] window = new int[][]{
          {0,0,1,1,1,0,0},
          {0,1,1,1,1,1,0},
          {1,1,1,1,1,1,1},
          {1,1,1,1,1,1,1},
          {1,1,1,1,1,1,1},
          {0,1,1,1,1,1,0},
          {0,0,1,1,1,0,0},
          };

  public SusanFilter() {

  }

  @Override
  public void applyFilter(Image image, Image outputImage, int x, int y) {
    final ChannelType channelType = ChannelType.GREY;
    final int minOffset = (window.length - 1) / 2;

    int nr0 = 0;
    for (int i = -1 * minOffset; i <= minOffset; i++) {
      for (int j = -1 * minOffset; j <= minOffset; j++) {
        // Avoids getting elements outside window by repetition method
        int useI = Math.max(Math.min(x + i, image.getHeight() - 1), 0);
        int useJ = Math.max(Math.min(y + j, image.getWidth() - 1), 0);

        if(window[i + minOffset][j + minOffset] == 1){
          double dif = Math.abs(image.getPixel(channelType,useI,useJ) - image.getPixel(channelType,x,y));
          if(dif < threshold){
            nr0++;
          }
        }
      }
    }

    double sr0 = 1.0 - (nr0 / 37.0);

    if(Math.abs(sr0 - 0.5) <= epsilon){
      outputImage.setPixel(ChannelType.GREEN, x,y,255);
      outputImage.setPixel(ChannelType.BLUE, x,y,0);
      outputImage.setPixel(ChannelType.RED, x,y,0);
  //  }else if(Math.abs(sr0 - 0.75) <= epsilon){
    }else if(sr0 >= (0.75-epsilon)){

      //Needs to paint the corner bigger. If not, it wont be visible

      for(int i = -1; i <= 1; i++){
        for(int j = -1; j <= 1; j++) {
          int useI = Math.max(Math.min(x + i, image.getHeight() - 1), 0);
          int useJ = Math.max(Math.min(y + j, image.getWidth() - 1), 0);
          outputImage.setPixel(ChannelType.RED, useI, useJ,255);
          outputImage.setPixel(ChannelType.GREEN, useI, useJ,0);
          outputImage.setPixel(ChannelType.BLUE, useI, useJ,0);
        }
      }
    }
  }
}
