package ar.edu.itba.ati.back_2.interfaces;

public interface WindowFilterImageOutput {

  //Used to show the result of a filter on a different image, without altering the original one
  void applyFilter(final Image image, Image outputImage, int x, int y);
}
