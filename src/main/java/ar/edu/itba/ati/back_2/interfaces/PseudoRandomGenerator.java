package ar.edu.itba.ati.back_2.interfaces;

public interface PseudoRandomGenerator {

  double getNext();
}
