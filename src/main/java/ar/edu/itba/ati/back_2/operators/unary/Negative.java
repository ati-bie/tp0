package ar.edu.itba.ati.back_2.operators.unary;

import java.util.function.DoubleUnaryOperator;

public final class Negative implements DoubleUnaryOperator {

  private static final int HIGHEST_VALUE = 255;

  @Override
  public double applyAsDouble(final double x) {
    return HIGHEST_VALUE - x;
  }
}
