package ar.edu.itba.ati.back_2.operators.unary;

import static java.lang.Math.pow;

import java.util.function.DoubleUnaryOperator;

public final class GammaPower implements DoubleUnaryOperator {

  private static final double HIGHEST_VALUE = 255;

  private final double constant;
  private final double gamma;

  public GammaPower(final double gamma) {
    if (Double.compare(gamma, 0) < 0 || Double.compare(gamma, 2.0) > 0) {
      throw new IllegalArgumentException("gamma must be between 0 and 2.0");
    }

    constant = pow(HIGHEST_VALUE, 1 - gamma);
    this.gamma = gamma;
  }

  @Override
  public double applyAsDouble(final double x) {
    return constant * pow(x, gamma);
  }
}
