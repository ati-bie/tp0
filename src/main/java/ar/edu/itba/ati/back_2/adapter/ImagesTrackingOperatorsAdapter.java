package ar.edu.itba.ati.back_2.adapter;

import ar.edu.itba.ati.back_2.facades.ImagesTrackingOperatorsFacade;
import ar.edu.itba.ati.back_2.models.IntPoint;

import java.util.List;

public class ImagesTrackingOperatorsAdapter {
  private final ImagesTrackingOperatorsFacade facade;

  public ImagesTrackingOperatorsAdapter() {
    facade = new ImagesTrackingOperatorsFacade();
  }

  public List<IntPoint> harris(final ImageByte imageByte, final double threshold) {
    return facade.harris(imageByte.getImage(), threshold);
  }

  public ImageByte sift(ImageByte imgByte1, ImageByte imgByte2, double delta) {
    return new ImageByte(facade.sift(imgByte1, imgByte2, delta));
  }
}
