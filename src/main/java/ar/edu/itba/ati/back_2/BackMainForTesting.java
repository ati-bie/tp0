package ar.edu.itba.ati.back_2;


import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.facades.ImagesFiltersOperatorsFacade;
import ar.edu.itba.ati.back_2.file_handlers.loaders.ImageLoaderManager;
import ar.edu.itba.ati.back_2.file_handlers.writers.ImageWriterManager;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.IntPoint;

import java.util.LinkedList;
import java.util.List;

public class BackMainForTesting {

  private static final String resourcesPath =
      System.getProperty("user.dir") + "/src/sift/resources/";

  public static void main(final String[] args) {
//    final Image whiteImage = ChannelTypeImagesFactory
//        .getGreyscaleImageWithDefaultValue(300, 300, 255);

//    ImageWriterManager.writeImageToFile(resourcesPath + "white.pgm", new ImageByte(whiteImage));
//    final Image image2 = ChannelTypeImagesFactory.getRGBImage(300, 300);
//    final Image image3 = ChannelTypeImagesFactory.getGreyscaleImage(300, 300);

    // Testing comparison of types
//    System.out.println(ImageComparatorByChannelTypes.areOfTheSameType(image, image2));
//    System.out.println(ImageComparatorByChannelTypes.areOfTheSameType(image, image3));

    /**
     * Testing unary operators
     */
    //final ImagesUnaryOperatorsFacade unaryOperatorsFacade = new ImagesUnaryOperatorsFacade();

//    final Image negativeImage = unaryOperatorsFacade.negative(image);

    /**
     * Testing reading and writing of images
     */

//    final Image barcoImage = ImageLoaderManager.loadImageFromFile(resourcesPath + "BARCO.pgm");
//    final Image lenaRAWImage = ImageLoaderManager.loadImageFromFile(resourcesPath + "LENA.RAW");

//    ImageWriterManager.writeImageToFile(resourcesPath + "barco_4.pgm", new ImageByte(barcoImage));
//
//    final Image boxesImage = ImageLoaderManager.loadImageFromFile(resourcesPath + "boxes_1.ppm");
//
//    ImageWriterManager.writeImageToFile(resourcesPath + "boxes_2.ppm", new ImageByte(boxesImage));
//
//    final Image barcoRAWImage = ImageLoaderManager.loadImageFromFile(resourcesPath + "BARCO.RAW");
//
//    ImageWriterManager.writeImageToFile(resourcesPath + "barco_3.pgm", new ImageByte(barcoRAWImage));
//
//    ImageWriterManager.writeImageToFile(resourcesPath + "barco_2.raw", new ImageByte(barcoImage));
//
//    final Image barco2RAWImage = ImageLoaderManager
//        .loadImageFromFile(resourcesPath + "barco_2.raw");
//
//    ImageWriterManager.writeImageToFile(resourcesPath + "barco_3.pgm", new ImageByte(barco2RAWImage));

//    // Testing noise
//
//    final ImagesNoiseOperatorsFacade noiseOperatorsFacade = new ImagesNoiseOperatorsFacade();
//
//    final Image barcoWithFilter = noiseOperatorsFacade.saltAndPepper(barcoImage, 0.5, 0.2, 0.9);
//    ImageWriterManager
//        .writeImageToFile(resourcesPath + "barco_with_filter.pgm", new ImageByte(barcoWithFilter));
//
//    final Image lenaRAWImage = ImageLoaderManager.loadImageFromFile(resourcesPath + "LENA.RAW");
//    final Image lenaGauss = noiseOperatorsFacade.gauss(lenaRAWImage, 1.0, 0, 20);
//    ImageWriterManager.writeImageToFile(resourcesPath + "lena_gauss.pgm", new ImageByte(lenaGauss));
//
////    final Image barcoSaltPepper = noiseOperatorsFacade.saltAndPepper(barcoImage, 0.5, 0.25, 0.75);
////    ImageWriterManager.writeImageToFile(resourcesPath + "barco_salt_pepper.pgm", barcoSaltPepper);
//
////    final Image barcoExp = noiseOperatorsFacade.exponential(barcoImage, 1.0, 1.5);
////    ImageWriterManager.writeImageToFile(resourcesPath + "barco_exp.pgm", barcoExp);
//
//    // Testing filters
    final ImagesFiltersOperatorsFacade filtersOperatorsFacade = new ImagesFiltersOperatorsFacade();
//
//    final Image lenaSobel = filtersOperatorsFacade.laplacianGaussianSlope(lenaRAWImage, 0.6, -255 , 0, 255);
//    ImageWriterManager.writeImageToFile(resourcesPath + "lena_filter_slope_gaussian.pgm", new ImageByte(lenaSobel));

//    ImagesBinaryOperatorsFacade binaryOperatorsFacade = new ImagesBinaryOperatorsFacade();
//
//    final Image lenaBarco = binaryOperatorsFacade.sum(barcoRAWImage, lenaRAWImage);
//    ImageWriterManager.writeImageToFile(resourcesPath + "lenaBarco.pgm", new ImageByte(lenaBarco));

//
//    /**
//     * Testing noise
//     */
//    final Image testImage = ImageLoaderManager.loadImageFromFile(resourcesPath + "TEST.pgm");
//    final ImagesFiltersOperatorsFacade filtersOperatorsFacade = new ImagesFiltersOperatorsFacade();
//    final Image barcoWithFilter = filtersOperatorsFacade.weightedMedian(barcoImage);
//
//    ImageWriterManager.writeImageToFile(resourcesPath + "barco_with_filter.pgm", new ImageByte(barcoWithFilter));

//    final Image lenaRAWImage = ImageLoaderManager.loadImageFromFile(resourcesPath + "LENAX.RAW");

//    ImageWriterManager.writeImageToFile(resourcesPath + "LENAX.PGM", new ImageByte(lenaRAWImage));

    final Image image2 = ImageLoaderManager.loadImageFromFile(resourcesPath + "a1.ppm");


    ImageWriterManager
        .writeImageToFile(resourcesPath + "levelSet2.ppm", new ImageByte(image2));
  }

  private static boolean areEqual(final int[][] phi1, final int[][] phi2) {
    for (int i = 0; i < phi1.length; i++) {
      for (int j = 0; j < phi1[0].length; j++) {
        if (phi1[i][j] != phi2[i][j]) {
          return false;
        }
      }
    }

    return true;
  }

  private static int[][] deepCopy(final int[][] array) {
    final int[][] newArray = new int[array.length][array[0].length];

    for (int i = 0; i < newArray.length; i++) {
      for (int j = 0; j < newArray[0].length; j++) {
        newArray[i][j] = array[i][j];
      }
    }

    return newArray;
  }

  private static List<IntPoint> getSquarePoints(final int xInitial, final int yInitial,
      final int xEnd, final int yEnd) {

    final List<IntPoint> points = new LinkedList<>();

    for (int y = yInitial; y <= yEnd; y++) {
      points.add(new IntPoint(xInitial, y));
      points.add(new IntPoint(xEnd, y));
    }

    for (int x = xInitial + 1; x <= xEnd - 1; x++) {
      points.add(new IntPoint(x, yInitial));
      points.add(new IntPoint(x, yEnd));
    }

    return points;
  }
}
