package ar.edu.itba.ati.back_2.facades;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.operators.ImageBinaryOperator;
import java.util.function.DoubleBinaryOperator;

public final class ImagesBinaryOperatorsFacade {

  public Image sum(final Image firstImage, final Image secondImage) {
    return applyOperator(Double::sum, firstImage, secondImage);
  }

  public Image subtract(final Image firstImage, final Image secondImage) {
    return applyOperator((left, right) -> left - right, firstImage, secondImage);
  }

  public Image product(final Image firstImage, final Image secondImage) {
    return applyOperator((left, right) -> left * right, firstImage, secondImage);
  }

  private Image applyOperator(final DoubleBinaryOperator operator, final Image firstImage,
      final Image secondImage) {
    return getImageBinaryOperator(operator).apply(firstImage, secondImage);
  }

  // TODO check if the ImageBinaryOperator may be recycled || use a pool || create new one
  private ImageBinaryOperator getImageBinaryOperator(final DoubleBinaryOperator operator) {
    return new ImageBinaryOperator(operator);
  }
}
