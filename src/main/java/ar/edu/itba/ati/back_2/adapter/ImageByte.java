package ar.edu.itba.ati.back_2.adapter;

import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.Pair;
import ar.edu.itba.ati.back_2.operators.unary.LinearTransformation;
import ar.edu.itba.ati.back_2.utils.ChannelStatistics;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Objects;
import java.util.function.DoubleUnaryOperator;

/**
 * Image adapter class to be used by the front-end. If the values contained in the original image
 * are below 0 or above 255 then a linear transformation will be applied to return a value between
 * those bounds.
 */
public class ImageByte {

  private final Image image;
  private final EnumMap<ChannelType, LinearTransformation> linearTransformations;

  public ImageByte(final Image image) {
    Objects.requireNonNull(image);

    this.image = image;
    linearTransformations = new EnumMap<>(ChannelType.class);

    for (final ChannelType channelType : image.getChannelTypes()) {
      final Pair minAndMax = ChannelStatistics.getMinAndMaxPixel(image, channelType);

      if (isLinearTransformationRequired(minAndMax)) {
        linearTransformations.put(channelType, new LinearTransformation(minAndMax.getMin(),
            minAndMax.getMax()));
      }
    }
  }

  public byte getPixel(final ChannelType channelType, final int x, final int y) {
    final double pixel = image.getPixel(channelType, x, y);
    final DoubleUnaryOperator operator;

    if (isLinearTransformationRequired(channelType)) {
      operator = getLinearTransformation(channelType);
    } else {
      operator = value -> value;
    }

    final int result = toIntExact(round(operator.applyAsDouble(pixel)));

    if (result < 0 || result > 255) {
      throw new IllegalStateException("result should be between 0 and 255: "+result);
    }

    return (byte) result;
  }

  public int getDimensions() {
    return image.getDimensions();
  }

  public int getHeight() {
    return image.getHeight();
  }

  public int getWidth() {
    return image.getWidth();
  }

  public Collection<ChannelType> getChannelTypes() {
    return image.getChannelTypes();
  }

  private LinearTransformation getLinearTransformation(final ChannelType channelType) {
    return linearTransformations.get(channelType);
  }

  private boolean isLinearTransformationRequired(final Pair minAndMax) {
    return Double.compare(minAndMax.getMin(), 0) < 0 ||
        Double.compare(minAndMax.getMax(), 255) > 0;
  }

  private boolean isLinearTransformationRequired(final ChannelType channeltype) {
    return linearTransformations.containsKey(channeltype);
  }

  public Image getImage() {
    return image;
  }
}