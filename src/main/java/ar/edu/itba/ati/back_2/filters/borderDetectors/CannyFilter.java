package ar.edu.itba.ati.back_2.filters.borderDetectors;

import ar.edu.itba.ati.back_2.facades.ImagesFiltersOperatorsFacade;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.Pair;
import ar.edu.itba.ati.back_2.operators.ImageUnaryOperator;
import ar.edu.itba.ati.back_2.operators.unary.LinearTransformation;
import ar.edu.itba.ati.back_2.thresholds.OtsuThreshold;
import ar.edu.itba.ati.back_2.utils.ChannelStatistics;
import ar.edu.itba.ati.back_2.utils.ImageDuplicator;

import java.util.Arrays;
import java.util.Objects;

public class CannyFilter {

    public Image applyFilter(Image image, Image outputImage, double... thresholds) {
        Image[] images = { ImageDuplicator.createImageWithSamePixelValues(image) };

        // 1. Suavizamiento y diferenciación
        /*for (int i = 0; i < sigmas.length; i++) {
            images[i] = new ImagesFiltersOperatorsFacade().applyFilter(image, new GaussWindowFilter(sigmas[i]));
        }*/

        Image G, Gx, Gy;
        int prewittWindowSize = 3;

        for (Image currentImage : images) {
            // 2. Dirección perpendicular al borde
            G = new ImagesFiltersOperatorsFacade().applyFilter(currentImage, new PrewittSynthesizedFilter(prewittWindowSize));
            Gx = new ImagesFiltersOperatorsFacade().applyFilter(currentImage, new PrewittHorizontalFilter(prewittWindowSize));
            Gy = new ImagesFiltersOperatorsFacade().applyFilter(currentImage, new PrewittVerticalFilter(prewittWindowSize));

            // 3&4. Supresión de no máximos
            nonMaximumSupression(G, Gx, Gy);

            // 5. Umbralización con histéresis
            hysteresis(G, currentImage, thresholds);
        }
        return imagesIntersection(images);
    }

    private void printImage(Image image) {
        double pixel;
        for (int i = 90; i < image.getHeight()/2; i++) {
            for (int j = 90; j < image.getWidth(); j++) {
                pixel = image.getPixel(ChannelType.GREY, i, j);
                if (j == image.getWidth()-1) System.out.println((int)pixel);
                else System.out.print(pixel + "-");
            }
        }
    }

    private Image imagesIntersection(Image[] images) {
        Objects.requireNonNull(images, "images must not be null");
        Objects.requireNonNull(images[0], "there must be at least 1 image");

        ChannelType channelType = ChannelType.GREY;
        if (!images[0].getChannelTypes().contains(channelType)) throw new IllegalArgumentException("The images must be in greyscale");

        Image newImage = ImageDuplicator.createImageWithSameChannelTypes(images[0]);

        for (int i = 0; i < newImage.getHeight(); i++) {
            for (int j = 0; j < newImage.getHeight(); j++) {
                final int x = i, y = j;
                boolean check = Arrays.asList(images).stream().anyMatch(im -> im.getPixel(channelType, x, y) == 255);
                if (check) {
                    newImage.setPixel(channelType, i, j, 255);
                }
            }
        }

        return newImage;
    }

    private void hysteresis(Image g, Image image, double... ths) {
        double currentPixel, newValue = 0;
        for (ChannelType channel : g.getChannelTypes()) {
            Pair thresholds = new Pair(ths[0], ths[1]);

            g.setPixel(channel, 0, 0, 0);

            for (int x = 0; x < g.getHeight(); x++) {
                for (int y = 1; y < g.getWidth(); y++) {
                    currentPixel = g.getPixel(channel, x, y);
                    if (currentPixel <= thresholds.getMin()) {
                        newValue = 0;
                    } else if (currentPixel >= thresholds.getMax()) {
                        newValue = 255;
                    } else {
                        double n;
                        boolean finished = false;
                        for (int i = x-1; !finished && i <= x; i++) {
                            for (int j = y-1; !finished && j <= y+1; j++) {
                                if ( i > 0 && j > 0 && i < g.getHeight() && j < g.getWidth()) {
                                    n = g.getPixel(channel, i, j);
                                    if (n == 255) {
                                        newValue = 255;
                                        finished = true;
                                    }
                                }
                            }
                        }
                    }
                    image.setPixel(channel, x, y, newValue);
                }
            }
        }
    }

    private Pair getThresholds(Image image, ChannelType channel) {
        Pair pair = ChannelStatistics.getMinAndMaxPixel(image, channel);
        Image tlImage = new ImageUnaryOperator(new LinearTransformation(pair.getMin(), pair.getMax()))
                .apply(image); // TODO: cambiar a image[channel]

        //Image tlImage2 = new ImageImp(image.getChannelTypes(), image.getHeight(), image.getWidth());
        //new ImageUnaryOperator(new LinearTransformation(pair.getMin(), pair.getMax()))
        //       .applyToNewChannel(tlImage2, channel, image);

        double t = new OtsuThreshold().getThreshold(tlImage);
        double standardDeviation = ChannelStatistics.findStandardDeviation(image, channel);
        double t1 = t - standardDeviation/2;
        double t2 = t + standardDeviation/2;

        return new Pair(t1, t2);
    }

    private void nonMaximumSupression(Image g, Image gx, Image gy) {
        int currentAngle;
        double[] neighbors;
        double currentPixel;

        for (int x = 0; x < g.getHeight(); x++) {
            for (int y = 0; y < g.getWidth(); y++) {
                for (ChannelType channel : g.getChannelTypes()) {
                    currentPixel = g.getPixel(channel, x, y);
                    if (currentPixel > 0) {
                        currentAngle = getMaxAngle(g, channel, gx, gy, x, y);
                        neighbors = getNeighborPixels(currentAngle, g, channel, x, y);

                        if (currentPixel <= neighbors[0] || currentPixel <= neighbors[1]) {
                            g.setPixel(channel, x, y, 0.0);
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns the magnitud of the 2 neighbors according to the angle
     *
     * @param angle
     * @param g
     * @param channelType
     * @param x
     * @param y
     * @return [neighbor1, neighbor2]
     */
    private double[] getNeighborPixels(int angle, Image g, ChannelType channelType, int x, int y) {
        int x1, y1, x2, y2;

        switch (angle) {
            case 0: x1 = 0; y1 = -1; x2 = 0; y2 = 1; break;
            case 45: x1 = -1; y1 = 1; x2 = 1; y2 = -1; break;
            case 90: x1 = -1; y1 = 0; x2 = 1; y2 = 0; break;
            case 135: x1 = -1; y1 = -1; x2 = 1; y2 = 1; break;
            default: throw new IllegalArgumentException("The angle must be 0, 45, 90 or 135");
        }

        int X1 = x + x1;
        int X2 = x + x2;
        int Y1 = y + y1;
        int Y2 = y + y2;
        boolean restrictions1Passed = X1 >= 0 && X1 < g.getHeight() &&  Y1 >= 0 && Y1 < g.getWidth();
        boolean restrictions2Passed = X2 >= 0 && X2 < g.getHeight() && Y2 >= 0 && Y2 < g.getWidth();
        double firstNeighbor = restrictions1Passed ? g.getPixel(channelType, X1, Y1) : -1;
        double secondNeighbor = restrictions2Passed ? g.getPixel(channelType, X2, Y2) : -1;

        return new double[] {firstNeighbor, secondNeighbor};
    }

    private int getMaxAngle(Image image, ChannelType channel, Image gx, Image gy, int x, int y) {
        double currentPixel = image.getPixel(channel, x, y);
        double currentGX = gx.getPixel(channel, x, y);
        double currentGY = gy.getPixel(channel, x, y);

        if (currentPixel == 0) return 0;
        if (currentGX == 0) return 0;

        double angle = Math.atan(currentGY / currentGX) + Math.PI / 2;
        angle = Math.toDegrees(angle);

        if ( (angle > 0 && angle <= 22.5) || (angle >= 157.5 && angle < 180)) {
            return 0;
        } else if (angle > 22.5 && angle <= 67.5) {
            return 45;
        } else if (angle > 67.5 && angle <= 112.5) {
            return 90;
        } else {
            return 135;
        }
    }

}
