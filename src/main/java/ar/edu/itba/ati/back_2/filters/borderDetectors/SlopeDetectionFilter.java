package ar.edu.itba.ati.back_2.filters.borderDetectors;


import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.utils.ImageDuplicator;
import java.util.Objects;

/**
 * Created by Marco on 9/17/2017.
 */
public class SlopeDetectionFilter {
    public SlopeDetectionFilter() {
    }

    public Image detectSlopeBorder(final Image image, double slopeThreshold) {
        Objects.requireNonNull(image);

        final Image newImage = ImageDuplicator.createImageWithSameChannelTypes(image);

        for (final ChannelType channelType : newImage.getChannelTypes()) {
            for (int x = 0; x < newImage.getHeight(); x++) {
                double lastY = 0;
                for (int y = 0; y < newImage.getWidth(); y++) {
                    double pixelValue = image.getPixel(channelType, x, y);
                    if (pixelValue != 0) {
                        if (pixelValue * lastY < 0) {
                            if (Math.abs(pixelValue) + Math.abs(lastY) >= slopeThreshold) {
                                newImage.setPixel(channelType, x, y, 0xFF);
                            }
                        }
                        lastY = pixelValue;
                    }
                }
            }

            for (int y = 0; y < newImage.getWidth(); y++) {
                double lastX = 0;
                for (int x = 0; x < newImage.getHeight(); x++) {
                    double pixelValue = image.getPixel(channelType, x, y);
                    if (pixelValue != 0) {
                        if (pixelValue * lastX < 0) {
                            if (Math.abs(pixelValue) + Math.abs(lastX) >= slopeThreshold) {
                                newImage.setPixel(channelType, x, y, 0xFF);

                            }
                        }
                        lastX = pixelValue;
                    }
                }
            }
        }

        return newImage;
    }
}
