package ar.edu.itba.ati.back_2.facades;

import ar.edu.itba.ati.back_2.filters.*;
import ar.edu.itba.ati.back_2.filters.borderDetectors.*;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.interfaces.WindowFilterImageOutput;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.IntPoint;
import ar.edu.itba.ati.back_2.operators.ImageFilterOperator;
import ar.edu.itba.ati.back_2.operators.ImageFilterOperatorImageOutput;
import ar.edu.itba.ati.back_2.placeholders.HoughTransformBoundaries;
import ar.edu.itba.ati.back_2.tracking.LevelSet;
import ar.edu.itba.ati.back_2.utils.ImageDuplicator;

import java.util.List;

public final class ImagesFiltersOperatorsFacade {

  public Image gauss(final Image image, final double standardDeviation) {
    return applyFilter(image, new GaussWindowFilter(standardDeviation));
  }

  public Image median(final Image image, final int side) {
    return applyFilter(image, new MedianWindowFilter(side));
  }

  public Image average(final Image image, final int side) {
    return applyFilter(image, new AverageWindowFilter(side));
  }

  public Image weightedMedian(final Image image) {
    return applyFilter(image, new WeightedMedianWindowFilter());
  }

  public Image borderDetection(final Image image, final int side) {
    return applyFilter(image, new BorderDetectionWindowFilter(side));
  }

  public Image prewittVertical(final Image image, final int side) {
    return applyFilter(image, new PrewittVerticalFilter(side));
  }

  public Image prewittHorizontal(final Image image, final int side) {
    return applyFilter(image, new PrewittHorizontalFilter(side));
  }

  public Image prewittSynthesized(final Image image, final int side) {
    return applyFilter(image, new PrewittSynthesizedFilter(side));
  }

  public Image sobel(final Image image) {
    return applyFilter(image, new SobelSynthesizedFilter());
  }

  public Image laplacianNoSlope(final Image image) {
    return applyFilter(image, new LaplacianNoSlopeFilter());
  }

  public Image laplacianWithSlope(final Image image, double slopeThreshold) {
    Image laplacianWithoutSlope = applyFilter(image, new LaplacianNoSlopeFilter());
    return new SlopeDetectionFilter().detectSlopeBorder(laplacianWithoutSlope, slopeThreshold);
  }

  public Image laplacianGaussianSlope(final Image image, final double sigma, double slopeThreshold) {
    final Image laplacianWithoutSlope = applyFilter(image, new LaplacianGaussianFilter(sigma));
    final Image slopedImage = new SlopeDetectionFilter().detectSlopeBorder(laplacianWithoutSlope, slopeThreshold);
    return slopedImage;
  }


  public Image anisotropicDiffusion(Image image, int method, double anisotropicSigma, int anisotropicIterations) {
    AnisotropicDiffusionFilter filter = new AnisotropicDiffusionFilter(method, anisotropicSigma);
    Image finalImage = image;
    for(int i = 0; i < anisotropicIterations; i++ ){
      finalImage = applyFilter(finalImage, filter);
    }
    return finalImage;
  }

  public Image bidPrewittBorder(Image image) {
    return applyFilter(image, new BidPrewittBorderFilter());
  }

  public Image bidSobelBorder(Image image) { return applyFilter(image, new BidSobelBorderFilter()); }

  public Image globalThreshold(Image image) {
    return applyFilter(image, new BidSobelBorderFilter());
  }

  public Image canny(Image image, final double sigma1, final double sigma2) {
    Image colorOutputImage;
    if (image.getChannelTypes().contains(ChannelType.GREY)) {
      colorOutputImage = ImageDuplicator.createColorImageFromGrayscale(image);
    } else {
      colorOutputImage = ImageDuplicator.createImageWithSamePixelValues(image);
    }

    return new CannyFilter().applyFilter(image, colorOutputImage, sigma1, sigma2);
  }

  public Image susan(Image image) {
    Image colorOutputImage = ImageDuplicator.createColorImageFromGrayscale(image);
    return applyFilterImageOutput(image, colorOutputImage, new SusanFilter());
  }

  public int[][] levelSet(Image image, int maxIterations, List<IntPoint> listIn, List<IntPoint> listOut) {
    LevelSet levelset =  new LevelSet(image, maxIterations, listIn, listOut);
    levelset.firstCycle();
    return levelset.getPhi();
  }

  public int[][] levelSet(Image image, int maxIterations, List<IntPoint> listIn, List<IntPoint> listOut, int[][] phi) {
    LevelSet levelset =  new LevelSet(image, maxIterations, listIn, listOut, phi);
    levelset.firstCycle();
    return levelset.getPhi();
  }

  public Image applyFilter(final Image image, final WindowFilter filter) {
    return getImageFilterOperator(filter).apply(image);
  }

  private ImageFilterOperator getImageFilterOperator(final WindowFilter filter) {
    return new ImageFilterOperator(filter);
  }

  private Image applyFilterImageOutput(final Image grayscaleImage, Image outputImage, final WindowFilterImageOutput filter) {
    return getImageFilterOperatorImageOutput(filter).apply(grayscaleImage, outputImage);
  }

  private ImageFilterOperatorImageOutput getImageFilterOperatorImageOutput(final WindowFilterImageOutput filter) {
    return new ImageFilterOperatorImageOutput(filter);
  }

  public Image houghTransform(final Image image,
      final HoughTransformBoundaries houghTransformBoundaries, final double acceptanceThreshold,
      final double epsilon) {

    Image colorOutputImage = ImageDuplicator.createColorImageFromGrayscale(image);

    return new HoughTransform(image, colorOutputImage, houghTransformBoundaries, acceptanceThreshold, epsilon)
        .apply();
  }
}
