package ar.edu.itba.ati.back_2.adapter;

import ar.edu.itba.ati.back_2.facades.ImagesNoiseOperatorsFacade;

/**
 * Facade adapter to apply noise of different types to an image.
 *
 * Note: this is intended to be used only by the front-end
 */
public class ImagesNoiseOperatorsAdapter {

  private final ImagesNoiseOperatorsFacade facade;

  public ImagesNoiseOperatorsAdapter() {
    facade = new ImagesNoiseOperatorsFacade();
  }

  public ImageByte gauss(final ImageByte imageByte, final double noisePercentage, final double mean,
      final double standardDeviation) {

    return new ImageByte(facade.gauss(imageByte.getImage(), noisePercentage, mean,
        standardDeviation));
  }

  public ImageByte exponential(final ImageByte imageByte, final double noisePercentage,
      final double lambda) {

    return new ImageByte(facade.exponential(imageByte.getImage(), noisePercentage, lambda));
  }

  public ImageByte rayleigh(final ImageByte imageByte, final double noisePercentage,
      final double psi) {

    return new ImageByte(facade.rayleigh(imageByte.getImage(), noisePercentage, psi));
  }

  public ImageByte saltAndPepper(final ImageByte imageByte, final double noisePercentage,
      final double p0, final double p1) {

    return new ImageByte(facade.saltAndPepper(imageByte.getImage(), noisePercentage, p0, p1));
  }
}
