package ar.edu.itba.ati.back_2.operators.unary;

import java.util.function.DoubleUnaryOperator;

public final class ScalarProduct implements DoubleUnaryOperator {

  private final double scalar;

  public ScalarProduct(final double scalar) {
    this.scalar = scalar;
  }

  @Override
  public double applyAsDouble(final double x) {
    return scalar * x;
  }
}
