package ar.edu.itba.ati.back_2.placeholders;

public class HoughTransformBoundaries {

  private final int rhoMin;
  private final int rhoMax;
  private final int rhoStep;
  private final int thetaMin;
  private final int thetaMax;
  private final int thetaStep;

  public HoughTransformBoundaries(final int rhoMin, final int rhoMax, final int rhoStep,
      final int thetaMin, final int thetaMax, final int thetaStep) {

    requireSorted(rhoMin, rhoMax);
    requireSorted(thetaMin, thetaMax);

    this.rhoMin = rhoMin;
    this.rhoMax = rhoMax;
    this.rhoStep = requirePositive(rhoStep);
    this.thetaMin = requireBetweenBoundaries(thetaMin, 0, 360);
    this.thetaMax = requireBetweenBoundaries(thetaMax, 0, 360);
    this.thetaStep = requirePositive(thetaStep);

    requireCorrectStep(rhoMin, rhoMax, rhoStep);
    requireCorrectStep(thetaMin, thetaMax, thetaStep);
  }

  private void requireSorted(final int tentativeMin, final int tentativeMax) {
    if(tentativeMin > tentativeMax) throw new IllegalArgumentException("Min value should be smaller than max value");
  }

  private int requirePositive(final int value) {
    if (value <= 0) throw new IllegalArgumentException("Argument must be positive");
    return value;
  }

  private int requireBetweenBoundaries(final int value, final int min, final int max) {
    if(value < min || value > max) throw new IllegalArgumentException("Argument must be between bounds");
    return value;
  }

  private void requireCorrectStep(final int min, final int max, final int step) {
    if((max - min) % step != 0) throw new IllegalArgumentException("Step must be valid");
  }

  public int getRhoMin() {
    return rhoMin;
  }

  public int getRhoMax() {
    return rhoMax;
  }

  public int getRhoStep() {
    return rhoStep;
  }

  public int getThetaMin() {
    return thetaMin;
  }

  public int getThetaMax() {
    return thetaMax;
  }

  public int getThetaStep() {
    return thetaStep;
  }
}
