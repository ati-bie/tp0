package ar.edu.itba.ati.back_2.file_handlers.loaders;

import static ar.edu.itba.ati.back_2.models.ChannelType.GREY;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.utils.ChannelTypeImagesFactory;
import ij.ImagePlus;
import ij.io.Opener;

public class PGMLoader implements ImageLoader {

  @Override
  public Image loadImageFromFile(final String path) {
    System.out.println(path);
    final ImagePlus imagePlus = new Opener().openImage(path);

    return mapImagePlusToImage(imagePlus);
  }

  private Image mapImagePlusToImage(final ImagePlus imagePlus) {
    final Image image = ChannelTypeImagesFactory.getGreyscaleImage(imagePlus.getHeight(),
        imagePlus.getWidth());

    mapPixelsFromImagePlusToImage(imagePlus, image);

    return image;
  }

  private void mapPixelsFromImagePlusToImage(final ImagePlus imagePlus, final Image image) {
    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        image.setPixel(GREY, x, y, imagePlus.getPixel(y, x)[0]);
      }
    }
  }
}
