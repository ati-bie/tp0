package ar.edu.itba.ati.back_2.utils;

import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.Pair;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class ImagesStatistics {

  private ImagesStatistics() {
  }

  /**
   * Get the relative frequency for each pixel of every channel in the image.
   * @param image the image to calculate its histogram
   * @return a map whose key is a channel type and its value a map which contains the pixel value as
   * key and its relative frequency as its value
   */
  public static Map<ChannelType, Map<Integer, Double>> getHistogramPerChannelType(
      final Image image) {
    Objects.requireNonNull(image, "image must not be null");

    final Map<ChannelType, Map<Integer, Double>> histogram = new EnumMap<>(ChannelType.class);
    final double ratioToSum = 1.0 / image.getDimensions();

    for (final ChannelType channelType : image.getChannelTypes()) {
      final Map<Integer, Double> pixelCounts = new HashMap<>();

      for (int x = 0; x < image.getHeight(); x++) {
        for (int y = 0; y < image.getWidth(); y++) {
          final int pixelValue = toIntExact(round(image.getPixel(channelType, x, y)));

          final Double existentValue = pixelCounts.putIfAbsent(pixelValue, ratioToSum);
          if (existentValue != null) {
            pixelCounts.replace(pixelValue, existentValue + ratioToSum);
          }
        }
      }

      histogram.put(channelType, pixelCounts);
    }

    return histogram;
  }

  /**
   * Get the min and max pixel taking into account every channel type of the image.
   *
   * @param image the image to calculate is global min and max
   * @return the pair with the min and max pixel
   */
  public static Pair getChannelGlobalMinAndMaxPixel(final Image image) {
    Objects.requireNonNull(image, "image must not be null");

    double channelGlobalMin = Integer.MAX_VALUE;
    double channelGlobalMax = Integer.MIN_VALUE;

    for (final ChannelType channelType : image.getChannelTypes()) {
      final Pair channelPair = ChannelStatistics.getMinAndMaxPixel(image, channelType);

      if (channelGlobalMin > channelPair.getMin()) {
        channelGlobalMin = channelPair.getMin();
      }
      if (channelGlobalMax < channelPair.getMax()) {
        channelGlobalMax = channelPair.getMax();
      }
    }

    return new Pair(channelGlobalMin, channelGlobalMax);
  }
}
