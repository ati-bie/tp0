package ar.edu.itba.ati.back_2.models;

public class IntPoint {

  private final int x;
  private final int y;

  public IntPoint(final int x, final int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof IntPoint)) {
      return false;
    }

    final IntPoint intPoint = (IntPoint) o;

    if (getX() != intPoint.getX()) {
      return false;
    }
    return getY() == intPoint.getY();
  }

  @Override
  public int hashCode() {
    int result = getX();
    result = 31 * result + getY();
    return result;
  }

  @Override
  public String toString() {
    return "IntPoint{" +
        "x=" + x +
        ", y=" + y +
        '}';
  }
}
