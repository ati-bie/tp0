package ar.edu.itba.ati.back_2.filters;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.exp;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.pow;
import static java.lang.Math.round;
import static java.lang.Math.sqrt;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.models.ChannelType;

public class GaussWindowFilter implements WindowFilter {

  private double[][] window;
  private double windowSum;

  public GaussWindowFilter(final double sigma) {
    int side = (int) round((2 * sigma) + 1);
    side += (side + 1) % 2;

    int midPoint = max(3, ((side - 1) / 2) + 1);

    this.window = new double[side][side];
    for (int i = 0; i < side; i++) {
      for (int j = 0; j < side; j++) {
        int midCenterX = abs(i - midPoint) - 1;
        int midCenterY = abs(j - midPoint) - 1;

        window[i][j] =
            (1 / sqrt(2 * PI * sigma * sigma)) *
                exp(
                    -(midCenterX * midCenterX + midCenterY * midCenterY) / (2 * sigma * sigma));
        windowSum += window[i][j];
      }
    }
  }

  public GaussWindowFilter(final double sigma, final int side) {

//    int midPoint = Math.max(3, ((side - 1) / 2) + 1);
//
//    this.window = new double[side][side];
//    for (int i = 0; i < side; i++) {
//      for (int j = 0; j < side; j++) {
//        int midCenterX = Math.abs(i - midPoint) - 1;
//        int midCenterY = Math.abs(j - midPoint) - 1;
//
//        window[i][j] =
//                (1 / Math.sqrt(2 * Math.PI * sigma * sigma)) *
//                        Math.exp(
//                                -(midCenterX * midCenterX + midCenterY * midCenterY) / (2 * sigma * sigma));
//        windowSum += window[i][j];
//      }
//    }
    int midPoint = (side - 1) / 2;
    this.window = new double[side][side];
    for (int i = -midPoint; i < midPoint + 1; i++) {
      for (int j = -midPoint; j < midPoint + 1; j++) {
        window[midPoint + i][midPoint + j] = (1 / sqrt(2 * PI * pow(sigma, 2))) *
            exp(-(pow(i, 2) + pow(j, 2)) / (2 * pow(sigma, 2)));
        windowSum += window[midPoint + i][midPoint + j];
      }
    }


  }

  @Override
  public double applyFilter(final Image image, final ChannelType channelType, final int x,
      final int y) {

    final int midPoint = (window.length - 1) / 2;
    double sum = 0;

    for (int i = -1 * midPoint; i < midPoint + 1; i++) {
      for (int j = -1 * midPoint; j < midPoint + 1; j++) {
        // Avoids getting elements outside window by repetition method
        int useI = max(min(x + i, image.getHeight() - 1), 0);
        int useJ = max(min(y + j, image.getWidth() - 1), 0);

        sum += image.getPixel(channelType, useI, useJ) * window[i + midPoint][j + midPoint]
            / windowSum;
      }
    }
    return sum;
  }

//Too tired to make an abstraction of this function. Sorry u_u

  public double applyFilter(final int[][] phi, final int x,
      final int y) {

    final int minOffset = (window.length - 1) / 2;
    double sum = 0;

    for (int i = -1 * minOffset; i <= minOffset; i++) {
      for (int j = -1 * minOffset; j <= minOffset; j++) {
        // Avoids getting elements outside window by repetition method
        int useI = max(min(x + i, phi.length - 1), 0);
        int useJ = max(min(y + j, phi[0].length - 1), 0);

        sum += phi[useI][useJ] * window[i + minOffset][j + minOffset]
            / windowSum;
      }
    }
    return sum;
  }
}
