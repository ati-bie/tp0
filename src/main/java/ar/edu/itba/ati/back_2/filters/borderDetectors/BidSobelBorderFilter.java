package ar.edu.itba.ati.back_2.filters.borderDetectors;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.models.ChannelType;

public class BidSobelBorderFilter implements WindowFilter {
    private static final int[][] window1 = {{1,2,1},{0,0,0},{-1,-2,-1}};
    private static final int[][] window2 = {{-1,0,1},{-2,0,2},{-1,0,1}};
    private static final int[][] window3 = {{0,1,2},{-1,0,1},{-2,-1,0}};
    private static final int[][] window4 = {{-2, -1, 0},{-1,0,1},{0,1,2}};


    @Override
    public double applyFilter(Image image, ChannelType channelType, int x, int y) {
        double sum1 = 0;
        double sum2 = 0;
        double sum3 = 0;
        double sum4 = 0;

        double pixel;

        int minOffset = (window1.length-1) / 2;

        for(int i = -1 * minOffset; i <= minOffset; i++){
            for(int j = -1 * minOffset; j <= minOffset; j++) {
                int useI = Math.max(Math.min(x+i, image.getHeight()-1), 0);
                int useJ = Math.max(Math.min(y+j, image.getWidth()-1), 0);

                pixel = image.getPixel(channelType, useI, useJ);
                sum1 += pixel * window1[i+minOffset][j+minOffset];
                sum2 += pixel * window2[i+minOffset][j+minOffset];
                sum3 += pixel * window3[i+minOffset][j+minOffset];
                sum4 += pixel * window4[i+minOffset][j+minOffset];
            }
        }
        return Math.max(sum1, Math.max(sum2, Math.max(sum3, sum4)));
    }
}
