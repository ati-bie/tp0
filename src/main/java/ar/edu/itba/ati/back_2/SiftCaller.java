package ar.edu.itba.ati.back_2;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.Point;
import ar.edu.itba.ati.back_2.operators.unary.LinearFunction;
import ar.edu.itba.ati.back_2.utils.ChannelTypeImagesFactory;
import ar.edu.itba.ati.back_2.utils.ImageDuplicator;
import ar.edu.itba.ati.front.utils.ImageByteToByteArrayConverter;
import ar.edu.itba.ati.front.utils.ImageByteType;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.process.ImageProcessor;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import mpicbg.ij.SIFT;
import mpicbg.imagefeatures.Feature;
import mpicbg.imagefeatures.FloatArray2DSIFT;

/*
* @author Fernando Bejarano
* @author Marcho Boschetti
* @author Gonzalo Ibars Ingman
*/
public class SiftCaller {

  final static private FloatArray2DSIFT.Param p = new FloatArray2DSIFT.Param();
  private static final int ANS_SEPARATION = 0; // Change this value to separate two images this number of pixels

  private static String containsMsg = "The image contains objects from the other";
  private static String equalsMsg = "The images are the same";
  private static String differentMsg = "The images are different";

  /**
   * Draw a rotated square around a center point having size and orientation
   *
   * @param o center point
   * @param scale size
   * @param orient orientation
   */
  static void drawSquare(ImageProcessor ip, double[] o, double scale, double orient) {
    scale /= 2;

    double sin = Math.sin(orient);
    double cos = Math.cos(orient);

    int[] x = new int[6];
    int[] y = new int[6];

    x[0] = (int) (o[0] + (sin - cos) * scale);
    y[0] = (int) (o[1] - (sin + cos) * scale);

    x[1] = (int) o[0];
    y[1] = (int) o[1];

    x[2] = (int) (o[0] + (sin + cos) * scale);
    y[2] = (int) (o[1] + (sin - cos) * scale);
    x[3] = (int) (o[0] - (sin - cos) * scale);
    y[3] = (int) (o[1] + (sin + cos) * scale);
    x[4] = (int) (o[0] - (sin + cos) * scale);
    y[4] = (int) (o[1] - (sin - cos) * scale);
    x[5] = x[0];
    y[5] = y[0];

    ip.drawPolygon(new Polygon(x, y, x.length));
  }

  public static List<Feature> getFeatures(BufferedImage img) {
    List<Feature> fs = new ArrayList<Feature>();

    final ImagePlus imp = new ImagePlus("", img);
    // if ( imp == null ) { System.err.println( "There are no images open"
    // ); return; }

    final GenericDialog gd = new GenericDialog("Test SIFT");

    SIFT.addFields(gd, p);
    // gd.showDialog();
    // if (gd.wasCanceled())
    // return;
    SIFT.readFields(gd, p);

    final ImageProcessor ip1 = imp.getProcessor().convertToFloat();
//		final ImageProcessor ip2 = imp.getProcessor().duplicate().convertToRGB();

    final SIFT ijSift = new SIFT(new FloatArray2DSIFT(p));
    fs.clear();

    final long start_time = System.currentTimeMillis();
    ijSift.extractFeatures(ip1, fs);
//        System.out.println(" took " + (System.currentTimeMillis() - start_time) + "ms");

    // ip2.setLineWidth( 1 );
    // ip2.setColor( Color.red );
    // for ( final Feature f : fs )
    // drawSquare( ip2, new double[]{ f.location[ 0 ], f.location[ 1 ] },
    // p.fdSize * 4.0 * ( double )f.scale, ( double )f.orientation );
    //
    // new ImagePlus( imp.getTitle() + " Features ", ip2 ).show();

    return fs;
  }

  private static BufferedImage copyBufferedImage(BufferedImage buffer) {
    int width = buffer.getWidth();
    int height = buffer.getHeight();

    BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

    // se copia imagen original
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        int color = buffer.getRGB(i, j);
        result.setRGB(i, j, color);
      }
    }
    return result;
  }

  public ar.edu.itba.ati.back_2.interfaces.Image compareKeypoints(ImageByte imgByte1,
      ImageByte imgByte2, double delta) {
    BufferedImage img1 = imageByteToBufferImage(imgByte1);
    // List<Feature> f1=getFeatures(img);
    BufferedImage img2 = imageByteToBufferImage(imgByte2);

    List<Feature> keyPoints1 = getFeatures(img1);
    List<Feature> keyPoints2 = getFeatures(img2);
    List<Feature> matchFrom1 = new ArrayList<Feature>();// keypoint en la
    // primera imagen
    // que coincidio
    List<Feature> matchFrom2 = new ArrayList<Feature>();// keypoint en la
    // segunda que
    // coincidio

    for (final Feature currLeftKeypoint : keyPoints1) {

      double minDis = Double.MAX_VALUE;
      Feature minDistFeature = null;
      for (final Feature currRightKeypoint : keyPoints2) {

        double dist = currLeftKeypoint.descriptorDistance(currRightKeypoint);

        if (dist < minDis) {
          minDis = dist;
          minDistFeature = currRightKeypoint;
        }

      }
      //System.out.println("Min dist: " + minDis);
      if (minDis < delta) {
        matchFrom2.add(minDistFeature);
        matchFrom1.add(currLeftKeypoint);
      }

    }

    double percentage = ((double) matchFrom1.size())
        / ((keyPoints1.size() < keyPoints2.size()) ? keyPoints2.size() : keyPoints1.size());

    // String resultMsg=(percentage<0.90)?containsMsg:equalsMsg;
    String resultMsg;
    if (percentage >= 0.8) {
      resultMsg = equalsMsg;
    } else if (percentage >= 0.5) {
      resultMsg = containsMsg;
    } else {
      resultMsg = differentMsg;
    }

    String msg = String
        .format("Matched:%d keypoints! =) \n Percentage match:%f %%. \n => %s", matchFrom1.size(),
            percentage * 100, resultMsg);
    System.out.println(msg);

//    return buildMatchingImage(imgByte1.getImage(), imgByte2.getImage(), matchFrom1, matchFrom2);
    return mergeImagesWithMatchingKeyPoints(imgByte1.getImage(), imgByte2.getImage(), matchFrom1,
        matchFrom2);
  }

  private Image mergeImagesWithMatchingKeyPoints(final Image image1, final Image image2,
      final List<Feature> keypointMatches1, final List<Feature> keypointMatches2) {

    final int resultingWidth = image1.getWidth() + image2.getWidth() + ANS_SEPARATION;
    final int resultingHeight = Math.max(image1.getHeight(), image2.getHeight());

    final Image resultImage = ChannelTypeImagesFactory
        .getGreyscaleImage(resultingHeight, resultingWidth);

    for (final ChannelType channelType : resultImage.getChannelTypes()) {
      for (int x = 0; x < image1.getHeight(); x++) {
        for (int y = 0; y < image1.getWidth(); y++) {
          final double originalImagePixel = image1.getPixel(channelType, x, y);

          resultImage.setPixel(channelType, x, y, originalImagePixel);
        }
      }
    }

    for (final ChannelType channelType : resultImage.getChannelTypes()) {
      for (int x = 0; x < image2.getHeight(); x++) {
        for (int y = 0; y < image2.getWidth(); y++) {
          final double originalImagePixel = image2.getPixel(channelType, x, y);

          resultImage
              .setPixel(channelType, x, y + image1.getWidth() + ANS_SEPARATION, originalImagePixel);
        }
      }
    }

    final Image realResultImage = ImageDuplicator.createColorImageFromGrayscale(resultImage);

    for (int i = 0; i < keypointMatches1.size(); i++) {
      // Axes here are ok
      int startX = (int) keypointMatches1.get(i).location[1];
      int startY = (int) keypointMatches1.get(i).location[0];
      int endX = (int) keypointMatches2.get(i).location[1];
      int endY = (int) keypointMatches2.get(i).location[0] + image1.getWidth() + ANS_SEPARATION;

      // Linear function was made for inverted axes, so the coordinates need to be inverted so that it matches this system
      final Point startPoint = new Point(startY, startX);
      final Point endPoint = new Point(endY, endX);
      final LinearFunction linearFunction = new LinearFunction(startPoint,
          endPoint);

      int randR = (int) (Math.random() * 256);
      int randG = (int) (Math.random() * 256);
      int randB = (int) (Math.random() * 256);
      // Draw slope
      for (int y = startY; y < endY; y++) {
        final int x = (int) linearFunction.applyAsDouble(y);

        realResultImage.setPixel(ChannelType.RED, x
            , y, randR);
        realResultImage.setPixel(ChannelType.GREEN, x
            , y, randG);
        realResultImage.setPixel(ChannelType.BLUE, x
            , y, randB);
      }

    }

    return realResultImage;
  }

  public BufferedImage imageByteToBufferImage(ImageByte imageByte) {
    final ImageByteToByteArrayConverter converter = new ImageByteToByteArrayConverter(imageByte);
    final ImageByteType imageByteType = ImageByteType.getImageByteType(imageByte);
    final BufferedImage bufferedImage;

    if (imageByteType == ImageByteType.GREYSCALE) {
      bufferedImage = new BufferedImage(imageByte.getWidth(), imageByte.getHeight(),
          BufferedImage.TYPE_BYTE_GRAY);

      bufferedImage.getRaster().setDataElements(0, 0, imageByte.getWidth(),
          imageByte.getHeight(), converter.getByteArrayforChannelType(ChannelType.GREY));
      return bufferedImage;
    } else if (imageByteType == ImageByteType.RGB) {
      final byte[] redStream = converter.getByteArrayforChannelType(ChannelType.RED);
      final byte[] greenStream = converter.getByteArrayforChannelType(ChannelType.GREEN);
      final byte[] blueStream = converter.getByteArrayforChannelType(ChannelType.BLUE);

      final int[] RGBStream = new int[imageByte.getDimensions() * 3];
      for (int i = 0; i < imageByte.getDimensions(); i++) {
        int fullPixel = redStream[i];
        fullPixel = (fullPixel << 8) + greenStream[i];
        fullPixel = (fullPixel << 8) + blueStream[i];

        RGBStream[i] = fullPixel;
      }
      bufferedImage = new BufferedImage(imageByte.getWidth(), imageByte.getHeight(),
          BufferedImage.TYPE_INT_RGB);
      bufferedImage.getRaster().setDataElements(0, 0, imageByte.getWidth(),
          imageByte.getHeight(), RGBStream);
      return bufferedImage;
    }
    return null;
  }
}
