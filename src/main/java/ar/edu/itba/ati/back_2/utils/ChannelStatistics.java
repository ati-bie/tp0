package ar.edu.itba.ati.back_2.utils;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.Pair;
import java.util.Objects;

public final class ChannelStatistics {

  private ChannelStatistics() {
  }

  public static Pair getMinAndMaxPixel(final Image image, final ChannelType channelType) {
    double currMin = Double.MAX_VALUE;
    double currMax = Double.MIN_VALUE;

    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        final double pixel = image.getPixel(channelType, x, y);

        if (pixel > currMax) {
          currMax = pixel;
        }
        if (pixel < currMin) {
          currMin = pixel;
        }
      }
    }

    return new Pair(currMin, currMax);
  }

  public static double findMean(final Image image, final ChannelType channelType) {
    Objects.requireNonNull(image);

    double cumSum = 0;

    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        cumSum += image.getPixel(channelType, x, y);
      }
    }

    return cumSum / (image.getHeight() * image.getWidth());
  }

  public static double findStandardDeviation(final Image image, final ChannelType channelType) {
    Objects.requireNonNull(image);

    final double mean = findMean(image, channelType);
    double cumSum = 0;

    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        cumSum += pow((image.getPixel(channelType, x, y)) - mean, 2);
      }
    }

    return sqrt(cumSum / (image.getHeight() * image.getWidth()));
  }
}
