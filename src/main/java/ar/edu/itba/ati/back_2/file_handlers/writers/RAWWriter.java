package ar.edu.itba.ati.back_2.file_handlers.writers;

import static ar.edu.itba.ati.back_2.file_handlers.FileExtension.RAW;
import static ar.edu.itba.ati.back_2.file_handlers.FileExtension.RHDR;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.file_handlers.ImageFileUtils;
import ar.edu.itba.ati.back_2.models.ChannelType;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class RAWWriter implements ImageWriter {

  private static final String HEADER_FORMAT = "#[Width],[Height],[isGreyscale]";

  @Override
  public void writeImageToFile(final ImageByte image, final String path) {
    writeImageHeader(image, path);
    writeImageData(image, path);
  }

  private void writeImageData(final ImageByte image, final String path) {
    final boolean isGreyscale = image.getChannelTypes().contains(ChannelType.GREY);
    final int pixelSize = isGreyscale ? 3 : 1;
    byte data[] = new byte[image.getDimensions() * pixelSize];

    if (isGreyscale) {
      data = ImageFileUtils.convertChannelToByteArray(image, ChannelType.GREY);
    } else {
      byte[][] bandsData = new byte[][]{
          ImageFileUtils.convertChannelToByteArray(image, ChannelType.RED),
          ImageFileUtils.convertChannelToByteArray(image, ChannelType.GREEN),
          ImageFileUtils.convertChannelToByteArray(image, ChannelType.BLUE)
      };

      for (int i = 0; i < image.getDimensions() * 3; i++) {
        data[i] = bandsData[i % 3][i / 3];
      }
    }

    try (final FileOutputStream out = new FileOutputStream(path)) {
      out.write(data);
      out.close();
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  private void writeImageHeader(final ImageByte image, final String path) {
    final String RAWHeaderPath = ImageFileUtils.replaceExtensionAtFilePath(path,
        RAW.toString(), RHDR.toString());

    try {
      final PrintWriter headerWriter = new PrintWriter(RAWHeaderPath, "UTF-8");

      headerWriter.println(HEADER_FORMAT);
      headerWriter.println(String.format("%d,%d,%s", image.getWidth(), image.getHeight(),
          image.getChannelTypes().contains(ChannelType.GREY)));
      headerWriter.close();
    } catch (FileNotFoundException | UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }
}
