package ar.edu.itba.ati.back_2.interfaces;

import ar.edu.itba.ati.back_2.models.ChannelType;
import java.util.Collection;

public interface Image {

  void setPixel(ChannelType channelType, int x, int y, double value);

  double getPixel(ChannelType channelType, int x, int y);

  int getDimensions();

  int getHeight();

  int getWidth();

  Collection<ChannelType> getChannelTypes();
}
