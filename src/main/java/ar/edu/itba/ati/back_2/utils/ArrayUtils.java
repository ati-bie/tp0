package ar.edu.itba.ati.back_2.utils;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

import java.util.Objects;

public final class ArrayUtils {

  private ArrayUtils() {
  }

  public static double euclideanNorm2(final double[] array) {
    Objects.requireNonNull(array);

    int sum = 0;
    for (int i = 0; i < array.length; i++) {
      sum += pow(array[i], 2);
    }

    return sqrt(sum);
  }

  public static double[][] deepCopy(final double[][] array) {
    Objects.requireNonNull(array);

    final double[][] arrayCopy = new double[array.length][array[0].length];
    for (int i = 0; i < array.length; i++) {
      System.arraycopy(array[i], 0, arrayCopy[i], 0, array[0].length);
    }

    return arrayCopy;
  }
}
