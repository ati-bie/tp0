package ar.edu.itba.ati.back_2.filters.borderDetectors;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.models.ChannelType;

public class PrewittVerticalFilter implements WindowFilter {

  private double[][] window;
  private int side;

  public PrewittVerticalFilter(final int side) {
    if (side % 2 == 0) {
      throw new IllegalArgumentException("Window Side should be odd");
    }
    this.side = side;

    this.window = new double[side][side];
    for (int i = 0; i < side; i++) {
      for (int j = 0; j < side; j++) {
        if(j == 0){
          window[i][j] = -1;
        }else if(j == side-1){
          window[i][j] = 1;
        }
      }
    }
  }

  @Override
  public double applyFilter(final Image image, final ChannelType channelType, final int x,
      final int y) {

    final int minOffset = (window.length - 1) / 2;
    double sum = 0;

    for (int i = -1 * minOffset; i <= minOffset; i++) {
      for (int j = -1 * minOffset; j <= minOffset; j++) {

        // Avoids getting elements outside window by repetition method
        int useI = Math.max(Math.min(x + i, image.getHeight() - 1), 0);
        int useJ = Math.max(Math.min(y + j, image.getWidth() - 1), 0);

        sum +=  window[i + minOffset][j + minOffset] * image.getPixel(channelType, useI, useJ);
      }
    }
    return sum;
  }
}
