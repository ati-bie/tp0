package ar.edu.itba.ati.back_2.facades;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.IntPoint;
import ar.edu.itba.ati.back_2.utils.ImageDrawer;
import ar.edu.itba.ati.back_2.utils.ImageDuplicator;
import ar.edu.itba.ati.back_2.utils.ImagesStatistics;

import java.util.List;
import java.util.Map;

public final class UtilsFacade {

  public Map<ChannelType, Map<Integer, Double>> getHistogramPerChannelType(final Image image) {
    return ImagesStatistics.getHistogramPerChannelType(image);
  }

  public Image drawPixels(ImageByte image, List<IntPoint>[] lists) {

    Image cloneImage = ImageDuplicator.createImageWithSamePixelValues(image.getImage());

    if(cloneImage.getChannelTypes().size() < 3){
      cloneImage = ImageDuplicator.createColorImageFromGrayscale(cloneImage);
    }

    return ImageDrawer.drawPixels(cloneImage, lists);
  }
}
