package ar.edu.itba.ati.back_2.adapter;

import ar.edu.itba.ati.back_2.facades.ImagesUnaryOperatorsFacade;

/**
 * Facade adapter to apply a unary operation to an image.
 *
 * Note: this is intended to be used only by the front-end
 */
public class ImagesUnaryOperatorsAdapter {

  private final ImagesUnaryOperatorsFacade facade;

  public ImagesUnaryOperatorsAdapter() {
    facade = new ImagesUnaryOperatorsFacade();
  }

  public ImageByte gammaPower(final ImageByte imageByte, final double gamma) {
    return new ImageByte(facade.gammaPower(imageByte.getImage(), gamma));
  }

  public ImageByte DRC(final ImageByte imageByte, final double highestValueToCompress) {
    return new ImageByte(facade.DRC(imageByte.getImage(), highestValueToCompress));
  }

  public ImageByte negative(final ImageByte imageByte) {
    return new ImageByte(facade.negative(imageByte.getImage()));
  }

  public ImageByte scalarProduct(final ImageByte imageByte, final double constant) {
    return new ImageByte(facade.scalarProduct(imageByte.getImage(), constant));
  }

  public ImageByte threshold(final ImageByte imageByte, final double threshold) {
    return new ImageByte(facade.threshold(imageByte.getImage(), 0, threshold, 255));
  }
}
