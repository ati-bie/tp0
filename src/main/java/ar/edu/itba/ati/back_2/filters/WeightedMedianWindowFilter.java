package ar.edu.itba.ati.back_2.filters;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.models.ChannelType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WeightedMedianWindowFilter implements WindowFilter {

  private static final int side = 3;
  private static final int[][] window = {{1, 2, 1}, {2, 4, 2}, {1, 2, 1}};

  @Override
  public double applyFilter(final Image image, final ChannelType channelType, final int x,
      final int y) {

    List<Double> pixels = new ArrayList<>(side * side);
    int minOffset = (side - 1) / 2;

    for (int i = -1 * minOffset; i <= minOffset; i++) {
      for (int j = -1 * minOffset; j <= minOffset; j++) {
        // Avoids getting elements outside window by repetition method
        int useI = Math.max(Math.min(x + i, image.getHeight() - 1), 0);
        int useJ = Math.max(Math.min(y + j, image.getWidth() - 1), 0);
        for (int count = 0; count < window[i + minOffset][j + minOffset]; count++) {
          pixels.add(image.getPixel(channelType, useI, useJ));
        }
      }
    }
    Collections.sort(pixels);

    return pixels.get((((side * side) - 1) / 2) + 1);
  }
}
