package ar.edu.itba.ati.back_2.models;

import ar.edu.itba.ati.back_2.interfaces.Channel;
import ar.edu.itba.ati.back_2.interfaces.Image;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;

public final class ImageImp implements Image {

  private final Map<ChannelType, Channel> channels;
  private final int height;
  private final int width;

  public ImageImp(final Collection<ChannelType> channelTypes, final int height, final int width) {
    if (height <= 0 || width <= 0) {
      throw new IllegalArgumentException("Height and width must be greater than zero");
    }

    this.height = height;
    this.width = width;
    channels = new EnumMap<>(ChannelType.class);

    for (final ChannelType channelType : channelTypes) {
      channels.put(channelType, new ChannelImp(height, width));
    }
  }

  public final void setPixel(final ChannelType channelType, final int x, final int y,
      final double value) {
    final Channel channel = channels.get(channelType);

    if (channel == null) {
      throw new IllegalArgumentException("Channel type does not exist in this image");
    }
    channel.setPixel(x, y, value);
  }

  public final double getPixel(final ChannelType channelType, final int x, final int y) {
    final Channel channel = channels.get(channelType);

    if (channel == null) {
      throw new IllegalArgumentException("Channel type does not exist in this image");
    }

    return channel.getPixel(x, y);
  }

  public final int getDimensions() {
    return getHeight() * getWidth();
  }

  public final int getHeight() {
    return height;
  }

  public final int getWidth() {
    return width;
  }

  @Override
  public Collection<ChannelType> getChannelTypes() {
    return new HashSet<>(channels.keySet());
  }
}
