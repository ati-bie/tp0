package ar.edu.itba.ati.back_2.facades;

import ar.edu.itba.ati.back_2.SiftCaller;
import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.IntPoint;
import ar.edu.itba.ati.back_2.tracking.Harris;

import java.util.List;

public class ImagesTrackingOperatorsFacade {

  public List<IntPoint> harris(final Image image, final double threshold) {
    return new Harris(image, threshold).run();
  }

  public Image sift(ImageByte imgByte1, ImageByte imgByte2, double delta) {
    return new SiftCaller().compareKeypoints(imgByte1, imgByte2, delta);
  }
}
