package ar.edu.itba.ati.back_2.facades;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.ThresholdOperator;
import ar.edu.itba.ati.back_2.thresholds.GlobalThreshold;
import ar.edu.itba.ati.back_2.thresholds.OtsuThreshold;

public class ImagesThresholdOperatorsFacade {

    public Image global(final Image image) {
        return applyThreshold(image, new GlobalThreshold());
    }

    public Image otsu(final Image image) {
        return applyThreshold(image, new OtsuThreshold());
    }

    private Image applyThreshold(final Image image, final ThresholdOperator op) {
        return op.apply(image);
    }
}
