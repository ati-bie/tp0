package ar.edu.itba.ati.back_2.adapter;

import ar.edu.itba.ati.back_2.facades.UtilsFacade;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.IntPoint;

import java.util.List;
import java.util.Map;

/**
 * Facade adapter to get general data from an image.
 *
 * Note: this is intended to be used only by the front-end
 */
public final class ImagesUtilsAdapter {

  private final UtilsFacade utilsFacade;

  public ImagesUtilsAdapter() {
    utilsFacade = new UtilsFacade();
  }

  public Map<ChannelType, Map<Integer, Double>> getHistogramPerChannelType(
      final ImageByte imageByte) {
    return utilsFacade.getHistogramPerChannelType(imageByte.getImage());
  }

  @SafeVarargs
  public final ImageByte drawPixels(final ImageByte image, final List<IntPoint>... lists){
    return new ImageByte(utilsFacade.drawPixels(image, lists));
  }
}
