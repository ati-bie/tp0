package ar.edu.itba.ati.back_2.operators.unary;

import ar.edu.itba.ati.back_2.models.Point;
import java.util.Objects;
import java.util.function.DoubleUnaryOperator;

public final class ContrastIncrease implements DoubleUnaryOperator {

  private static final int LOWEST_VALUE = 0;
  private static final int HIGHEST_VALUE = 255;

  private final LinearFunction decreasePixelFunction;
  private final LinearFunction middleFunction;
  private final LinearFunction increasePixelFunction;
  private final double lowPointX;
  private final double highPointX;

  public ContrastIncrease(final Point lowPoint, final Point highPoint) {
    Objects.requireNonNull(lowPoint, "lowPoint must not be null");
    Objects.requireNonNull(lowPoint, "highPoint must not be null");

    if (Double.compare(lowPoint.getX(), highPoint.getX()) >= 0) {
      throw new IllegalArgumentException(
          "highPoint x coordinate must be greater than lowPoint x coordinate");
    }

    decreasePixelFunction = new LinearFunction(new Point(LOWEST_VALUE, LOWEST_VALUE), lowPoint);
    middleFunction = new LinearFunction(lowPoint, highPoint);
    increasePixelFunction = new LinearFunction(highPoint, new Point(HIGHEST_VALUE, HIGHEST_VALUE));

    this.lowPointX = lowPoint.getX();
    this.highPointX = highPoint.getX();
  }

  @Override
  public double applyAsDouble(final double x) {
    if (Double.compare(x, LOWEST_VALUE) < 0 || Double.compare(x, HIGHEST_VALUE) > 0) {
      throw new IllegalArgumentException(
          "x must be between " + LOWEST_VALUE + " and " + HIGHEST_VALUE);
    }

    final double result;
    if (Double.compare(x, lowPointX) < 0) {
      result = decreasePixelFunction.applyAsDouble(x);
    } else if (Double.compare(x, highPointX) < 0) {
      result = middleFunction.applyAsDouble(x);
    } else {
      result = increasePixelFunction.applyAsDouble(x);
    }

    return result;
  }
}
