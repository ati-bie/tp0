package ar.edu.itba.ati.back_2.adapter;

import ar.edu.itba.ati.back_2.facades.ImagesBinaryOperatorsFacade;

/**
 * Facade adapter to apply an operation between two images.
 *
 * Note: this is intended to be used only by the front-end
 */
public class ImagesBinaryOperatorsAdapter {

  private final ImagesBinaryOperatorsFacade facade;

  public ImagesBinaryOperatorsAdapter() {
    this.facade = new ImagesBinaryOperatorsFacade();
  }

  public ImageByte sum(final ImageByte imageByte, final ImageByte secondImageByte) {
    return new ImageByte(facade.sum(imageByte.getImage(), secondImageByte.getImage()));
  }

  public ImageByte substract(final ImageByte imageByte, final ImageByte secondImageByte) {
    return new ImageByte(facade.subtract(imageByte.getImage(), secondImageByte.getImage()));
  }

  public ImageByte product(final ImageByte imageByte, final ImageByte secondImageByte) {
    return new ImageByte(facade.product(imageByte.getImage(), secondImageByte.getImage()));
  }
}