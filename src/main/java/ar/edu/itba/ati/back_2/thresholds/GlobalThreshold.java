package ar.edu.itba.ati.back_2.thresholds;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.ThresholdOperator;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.operators.ImageUnaryOperator;
import ar.edu.itba.ati.back_2.operators.unary.Threshold;
import ar.edu.itba.ati.back_2.utils.ImageDuplicator;

public class GlobalThreshold implements ThresholdOperator {
    private static final double deltaT = 1;

    @Override
    public Image apply(Image image) {
        double threshold = initThreshold();
        double prevThreshold;
        double diferential = deltaT + 1;

        double currentPixel, m1, n1, m2, n2;

        Image newImg = ImageDuplicator.createImageWithSamePixelValues(image);

        for (ChannelType channel : image.getChannelTypes()) {
            while (diferential > deltaT) {
                prevThreshold = threshold;
                m1 = 0;
                n1 = 0;
                m2 = 0;
                n2 = 0;

                for (int i = 0; i < image.getHeight(); i++) {
                    for (int j = 0; j < image.getWidth(); j++) {
                        currentPixel = image.getPixel(channel, i, j);
                        if (currentPixel < threshold) {
                            n1++;
                            m1 += currentPixel;
                        } else {
                            n2++;
                            m2 += currentPixel;
                        }
                    }
                }
                threshold = (m1 / n1 + m2 / n2) / 2;
                diferential = Math.abs(threshold - prevThreshold);
            }

            new ImageUnaryOperator(new Threshold(0, threshold, 255)).applyToNewChannel(newImg, channel, image);
        }

        return newImg;
    }

    private double initThreshold() {
        double threshold;
        do {
            threshold = (int) (Math.random() * 255);
        } while (threshold == 0 || threshold == 255);

        return threshold;
    }
}
