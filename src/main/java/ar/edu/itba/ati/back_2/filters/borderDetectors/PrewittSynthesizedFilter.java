package ar.edu.itba.ati.back_2.filters.borderDetectors;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.models.ChannelType;

public class PrewittSynthesizedFilter implements WindowFilter {

  private double[][] verticalWindow;
  private double[][] horizontalWindow;
  private int side;

  public PrewittSynthesizedFilter(final int side) {
    if (side % 2 == 0) {
      throw new IllegalArgumentException("Window Side should be odd");
    }
    this.side = side;

    this.verticalWindow = new double[side][side];
    this.horizontalWindow = new double[side][side];
    for (int i = 0; i < side; i++) {
      for (int j = 0; j < side; j++) {
        if(i == 0){
          horizontalWindow[i][j] = -1;
        }else if(i == side-1){
          horizontalWindow[i][j] = 1;
        }
        if(j == 0){
          verticalWindow[i][j] = -1;
        }else if(j == side-1){
          verticalWindow[i][j] = 1;
        }
      }
    }
  }

  @Override
  public double applyFilter(final Image image, final ChannelType channelType, final int x,
      final int y) {

    final int minOffset = (verticalWindow.length - 1) / 2;
    double sumVer = 0, sumHor = 0;

    for (int i = -1 * minOffset; i <= minOffset; i++) {
      for (int j = -1 * minOffset; j <= minOffset; j++) {
        // Avoids getting elements outside window by repetition method
        int useI = Math.max(Math.min(x + i, image.getHeight() - 1), 0);
        int useJ = Math.max(Math.min(y + j, image.getWidth() - 1), 0);

        sumVer += verticalWindow[i + minOffset][j + minOffset] * image.getPixel(channelType, useI, useJ);
        sumHor += horizontalWindow[i + minOffset][j + minOffset] * image.getPixel(channelType, useI, useJ);
      }
    }
    return Math.sqrt((sumHor*sumHor) + (sumVer * sumVer));
  }
}
