package ar.edu.itba.ati.back_2.file_handlers.loaders;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.adapter.PlaygroundOperatorsAdapter;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.utils.ChannelTypeImagesFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class JPGLoader implements ImageLoader {

  // I think this can actually load anything... but Im not sure
  private static PlaygroundOperatorsAdapter playgroundAdapter = new PlaygroundOperatorsAdapter();

  @Override
  public Image loadImageFromFile(final String path) {
    Objects.requireNonNull(path, "path must not be null");

    File imgPath = new File(path);
    BufferedImage bufferedImage = null;
    try {
      bufferedImage = ImageIO.read(imgPath);
    } catch (IOException e) {
      e.printStackTrace();
    }

    Image image = ChannelTypeImagesFactory.getRGBImage(bufferedImage.getHeight(), bufferedImage.getWidth());

    return playgroundAdapter.byteStreamToImage(new ImageByte(image), bufferedImage).getImage();
  }

}
