package ar.edu.itba.ati.back_2.interfaces;

import ar.edu.itba.ati.back_2.models.ChannelType;

public interface WindowFilter {

  double applyFilter(Image image, ChannelType channelType, int x, int y);

}
