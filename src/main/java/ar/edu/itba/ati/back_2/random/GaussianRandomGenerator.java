package ar.edu.itba.ati.back_2.random;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.log;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

import ar.edu.itba.ati.back_2.interfaces.RandomGenerator;
import java.util.Random;

public class GaussianRandomGenerator implements RandomGenerator {

  private final Random uniformRandom;

  private final double mean;
  private final double standardDeviation;

  private double y2;

  private boolean shouldGenerateNewPair;

  public GaussianRandomGenerator(final double mean, final double standardDeviation) {
    uniformRandom = new Random();
    this.mean = mean;
    this.standardDeviation = standardDeviation;
    shouldGenerateNewPair = false;
  }

  @Override
  public double getNext() {
    this.shouldGenerateNewPair = !this.shouldGenerateNewPair;

    if (!shouldGenerateNewPair) {
      return applyMuAndSigma(this.y2);
    }

    final double x1 = uniformRandom.nextDouble();
    final double x2 = uniformRandom.nextDouble();
    double y1 = sqrt(-2 * log(x1)) * cos(2 * PI * x2);
    this.y2 = sqrt(-2 * log(x1)) * sin(2 * PI * x2);

    return applyMuAndSigma(y1);
  }

  private double applyMuAndSigma(final double y) {
    return this.mean + y * this.standardDeviation;
  }
}
