package ar.edu.itba.ati.back_2.operators;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.utils.ImageDuplicator;
import java.util.Objects;
import java.util.function.Function;

public class ImageFilterOperator implements Function<Image, Image> {

  private final WindowFilter filter;

  public ImageFilterOperator(final WindowFilter filter) {
    Objects.requireNonNull(filter);

    this.filter = filter;
  }

  @Override
  public Image apply(final Image image) {
    Objects.requireNonNull(image);

    final Image newImage = ImageDuplicator.createImageWithSamePixelValues(image);

    for (final ChannelType channelType : newImage.getChannelTypes()) {
      for (int x = 0; x < newImage.getHeight(); x++) {
        for (int y = 0; y < newImage.getWidth(); y++) {
          final double result = filter.applyFilter(image, channelType, x, y);

          newImage.setPixel(channelType, x, y, result);
        }
      }
    }

    return newImage;
  }
}
