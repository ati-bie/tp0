package ar.edu.itba.ati.back_2.interfaces;

/**
 * A pseudorandom double generator
 */
public interface RandomGenerator {

  /**
   * Obtain a pseudorandom double
   *
   * @return the pseudorandom double
   */
  double getNext();
}
