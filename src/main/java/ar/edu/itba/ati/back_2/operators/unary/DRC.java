package ar.edu.itba.ati.back_2.operators.unary;

import static java.lang.Math.log;

import java.util.function.DoubleUnaryOperator;

public final class DRC implements DoubleUnaryOperator {

  private static final int HIGHEST_VALUE_AFTER_COMPRESSION = 255;

  private final double constant;

  public DRC(final double highestValueToCompress) {
    if (Double.compare(highestValueToCompress, 0) < 0) {
      throw new IllegalArgumentException(
          "highestValueToCompress should be greater or equal than zero");
    }

    constant = HIGHEST_VALUE_AFTER_COMPRESSION / log(1 + highestValueToCompress);
  }

  @Override
  public double applyAsDouble(final double x) {
    return constant * log(1 + x);
  }
}
