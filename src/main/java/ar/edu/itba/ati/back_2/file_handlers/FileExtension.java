package ar.edu.itba.ati.back_2.file_handlers;

public enum FileExtension {
  RAW, RHDR, PGM, PPM, JPG
}
