package ar.edu.itba.ati.back_2.adapter;


import ar.edu.itba.ati.back_2.facades.ImagesThresholdOperatorsFacade;

/**
 * Facade adapter to apply umbralization to an image with a threshold calculated automatically.
 *
 * Note: this is intended to be used only by the front-end
 */
public class ImagesThresholdsOperatorsAdapter {
    private final ImagesThresholdOperatorsFacade facade;

    public ImagesThresholdsOperatorsAdapter() { facade = new ImagesThresholdOperatorsFacade(); }

    public ImageByte global(final ImageByte imageByte) { return new ImageByte(facade.global(imageByte.getImage())); }

    public ImageByte otsu(final ImageByte imageByte) { return new ImageByte(facade.otsu(imageByte.getImage())); }
}
