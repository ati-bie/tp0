package ar.edu.itba.ati.back_2.utils;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.ImageImp;

import java.util.*;

public final class ImageDuplicator {

  public static Image createImageWithSameChannelTypes(final Image image) {
    Objects.requireNonNull(image, "image must not be null");

    final Collection<ChannelType> channelTypes = image.getChannelTypes();

    return ChannelTypeImagesFactory
        .getImageDoubleWithChannelTypes(channelTypes, image.getHeight(), image.getWidth());
  }

  public static Image createImageWithSameChannelTypes(final Image image, final int width, final int height) {
    Objects.requireNonNull(image, "image must not be null");

    final Collection<ChannelType> channelTypes = image.getChannelTypes();

    return ChannelTypeImagesFactory
            .getImageDoubleWithChannelTypes(channelTypes, height, width);
  }

    public static Image createImageWithSamePixelValues(final Image image) {
    Objects.requireNonNull(image, "image must not be null");

    final Image newImage = createImageWithSameChannelTypes(image);

    for (final ChannelType channelType : newImage.getChannelTypes()) {
      for (int x = 0; x < newImage.getHeight(); x++) {
        for (int y = 0; y < newImage.getWidth(); y++) {
          final double originalImagePixel = image.getPixel(channelType, x, y);

          newImage.setPixel(channelType, x, y, originalImagePixel);
        }
      }
    }

    return newImage;
  }

  public static Image createImageWithSamePixelValues(final Image image, int width, int height) {
    Objects.requireNonNull(image, "image must not be null");

    final Image newImage = createImageWithSameChannelTypes(image, width, height);

    for (final ChannelType channelType : newImage.getChannelTypes()) {
      for (int x = 0; x < image.getHeight(); x++) {
        for (int y = 0; y < image.getWidth(); y++) {
          final double originalImagePixel = image.getPixel(channelType, x, y);

          newImage.setPixel(channelType, x, y, originalImagePixel);
        }
      }
    }

    return newImage;
  }

  public static Image createImageWithSamePixelValuesByChannel(final Image image, final ChannelType channel) {
    Objects.requireNonNull(image, "image must not be null");

    final Collection<ChannelType> channelTypes = new HashSet<>();
    Image imageCopy = ChannelTypeImagesFactory
            .getImageDoubleWithChannelTypes(channelTypes, image.getHeight(), image.getWidth());

    final Image newImage = createImageWithSameChannelTypes(image);

    for (int x = 0; x < newImage.getHeight(); x++) {
      for (int y = 0; y < newImage.getWidth(); y++) {
        final double originalImagePixel = image.getPixel(channel, x, y);

        newImage.setPixel(channel, x, y, originalImagePixel);
      }
    }

    return newImage;
  }

  public static Image createImageWithDimensionCombination(final Image firstImage,
      final Image secondImage, final double valueToFill) {
    Objects.requireNonNull(firstImage, "firstImage must not be null");
    Objects.requireNonNull(secondImage, "secondImage must not be null");

    final Collection<ChannelType> channelTypes = firstImage.getChannelTypes();
    final int maxHeight = Math.max(firstImage.getHeight(), secondImage.getHeight());
    final int maxWidth = Math.max(firstImage.getWidth(), secondImage.getWidth());
    final Image newImage = ChannelTypeImagesFactory.getImageDoubleWithChannelTypes(channelTypes,
        maxHeight, maxWidth);

    for (final ChannelType channelType : newImage.getChannelTypes()) {
      for (int x = 0; x < newImage.getHeight(); x++) {
        for (int y = 0; y < newImage.getWidth(); y++) {
          newImage.setPixel(channelType, x, y, valueToFill);
        }
      }
    }

    return newImage;
  }


  public static Image createColorImageFromGrayscale(Image grayscaleImage) {

    Collection<ChannelType> channels =  grayscaleImage.getChannelTypes();
    if(!channels.contains(ChannelType.GREY)){
      throw new IllegalArgumentException("Gimme a gray image");
    }

    List<ChannelType> channelTypes = new LinkedList<>();
    channelTypes.add(ChannelType.RED);
    channelTypes.add(ChannelType.GREEN);
    channelTypes.add(ChannelType.BLUE);

    Image newImage = new ImageImp(channelTypes, grayscaleImage.getHeight(), grayscaleImage.getWidth());

    for(int i = 0; i < grayscaleImage.getHeight(); i++){
      for(int j = 0; j < grayscaleImage.getWidth(); j++) {
        for(ChannelType ch : channelTypes){
          newImage.setPixel(ch, i, j, grayscaleImage.getPixel(ChannelType.GREY, i, j));
        }
      }
    }

    return newImage;
  }
}
