package ar.edu.itba.ati.back_2.interfaces;

public interface Channel {

  void setPixel(int x, int y, double value);

  double getPixel(int x, int y);
}
