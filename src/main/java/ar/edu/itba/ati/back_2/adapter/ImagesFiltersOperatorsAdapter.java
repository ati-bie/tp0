package ar.edu.itba.ati.back_2.adapter;

import ar.edu.itba.ati.back_2.facades.ImagesFiltersOperatorsFacade;
import ar.edu.itba.ati.back_2.models.IntPoint;
import ar.edu.itba.ati.back_2.placeholders.HoughTransformBoundaries;

import java.util.List;

/**
 * Facade adapter to apply a filter to an image.
 *
 * Note: this is intended to be used only by the front-end
 */
public class ImagesFiltersOperatorsAdapter {

  private final ImagesFiltersOperatorsFacade facade;

  public ImagesFiltersOperatorsAdapter() {
    facade = new ImagesFiltersOperatorsFacade();
  }

  public ImageByte gauss(final ImageByte imageByte, final double standardDeviation) {
    return new ImageByte(facade.gauss(imageByte.getImage(), standardDeviation));
  }

  public ImageByte median(final ImageByte imageByte, final int side) {
    return new ImageByte(facade.median(imageByte.getImage(), side));
  }

  public ImageByte average(final ImageByte imageByte, final int side) {
    return new ImageByte(facade.average(imageByte.getImage(), side));
  }

  public ImageByte weightedMedian(final ImageByte imageByte) {
    return new ImageByte(facade.weightedMedian(imageByte.getImage()));
  }

  public ImageByte borderDetection(final ImageByte imageByte, final int side) {
    return new ImageByte(facade.borderDetection(imageByte.getImage(), side));
  }

  public ImageByte prewittVertical(final ImageByte imageByte, final int side) {
    return new ImageByte(facade.prewittVertical(imageByte.getImage(), side));
  }

  public ImageByte prewittHorizontal(final ImageByte imageByte, final int side) {
    return new ImageByte(facade.prewittHorizontal(imageByte.getImage(), side));
  }

  public ImageByte prewittSynthesized(final ImageByte imageByte, final int side) {
    return new ImageByte(facade.prewittSynthesized(imageByte.getImage(), side));
  }

  public ImageByte sobelFilter(final ImageByte imageByte) {
    return new ImageByte(facade.sobel(imageByte.getImage()));
  }

  public ImageByte laplacianNoSlope(final ImageByte imageByte) {
    return new ImageByte(facade.laplacianNoSlope(imageByte.getImage()));
  }

  public ImageByte laplacianWithSlope(final ImageByte imageByte, double slopeThreshold) {
    return new ImageByte(facade.laplacianWithSlope(imageByte.getImage(), slopeThreshold));
  }

  public ImageByte laplacianGaussianSlope(final ImageByte imageByte, final double sigma, final double slopeThreshold) {
    return new ImageByte(facade.laplacianGaussianSlope(imageByte.getImage(), sigma, slopeThreshold));
  }

  public ImageByte anisotropicFilter(ImageByte imageByte, int method, double anisotropicSigma,
                                     int anisotropicIterations) {
    return new ImageByte(facade.anisotropicDiffusion(imageByte.getImage(), method, anisotropicSigma,
            anisotropicIterations));
  }

  public ImageByte bidPrewittBorder(final ImageByte imageByte) {
    return new ImageByte(facade.bidPrewittBorder(imageByte.getImage()));
  }

  public ImageByte bidSobelBorder(final ImageByte imageByte) {
    return new ImageByte(facade.bidSobelBorder(imageByte.getImage()));
  }

  public ImageByte globalThreshold(final ImageByte imageByte) {
    return new ImageByte(facade.globalThreshold(imageByte.getImage()));
  }

  public ImageByte canny(ImageByte imageByte, final double sigma1, final double sigma2) {
    return new ImageByte(facade.canny(imageByte.getImage(), sigma1, sigma2));
  }

  public ImageByte susan(ImageByte imageByte) {
    return new ImageByte(facade.susan(imageByte.getImage()));
  }

  public int[][] levelSet(final ImageByte imageByte, int maxIterations, List<IntPoint> listIn, List<IntPoint> listOut,
                          int[][] phi) {
   return  facade.levelSet(imageByte.getImage(), maxIterations, listIn, listOut, phi);
  }

  public int[][] levelSet(final ImageByte imageByte, int maxIterations, List<IntPoint> listIn, List<IntPoint> listOut) {
    return  facade.levelSet(imageByte.getImage(), maxIterations, listIn, listOut);
  }


  public ImageByte houghTransform(final ImageByte imageByte, final int rhoMin, final int rhoMax,
      final int rhoStep, final int thetaMin, final int thetaMax, final int thetaStep,
      final double acceptanceThreshold, final double epsilon) {

    final HoughTransformBoundaries houghTransformBoundaries = new HoughTransformBoundaries(
        rhoMin, rhoMax, rhoStep, thetaMin, thetaMax, thetaStep
    );
    return new ImageByte(facade
        .houghTransform(imageByte.getImage(), houghTransformBoundaries, acceptanceThreshold,
            epsilon));
  }
}
