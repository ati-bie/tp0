package ar.edu.itba.ati.back_2.filters.borderDetectors;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.models.ChannelType;

public class LaplacianNoSlopeFilter implements WindowFilter {

  private double[][] window;
  private int side;

  public LaplacianNoSlopeFilter() {
    this.window = new double[][]{{0, -1, 0},{-1, 4, -1},{0, -1, 0}};
  }

  @Override
  public double applyFilter(final Image image, final ChannelType channelType, final int x,
      final int y) {

    final int minOffset = (window.length - 1) / 2;
    double sum = 0;

    for (int i = -1 * minOffset; i <= minOffset; i++) {
      for (int j = -1 * minOffset; j <= minOffset; j++) {
        // Avoids getting elements outside window by repetition method
        int useI = Math.max(Math.min(x + i, image.getHeight() - 1), 0);
        int useJ = Math.max(Math.min(y + j, image.getWidth() - 1), 0);

        sum += window[i + minOffset][j + minOffset] * image.getPixel(channelType, useI, useJ);
      }
    }
    return sum;
  }
}
