package ar.edu.itba.ati.back_2.file_handlers;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.file_handlers.loaders.*;
import ar.edu.itba.ati.back_2.file_handlers.writers.ImageWriter;
import ar.edu.itba.ati.back_2.file_handlers.writers.PGMWriter;
import ar.edu.itba.ati.back_2.file_handlers.writers.PPMWriter;
import ar.edu.itba.ati.back_2.file_handlers.writers.RAWWriter;
import ar.edu.itba.ati.back_2.models.ChannelType;
import java.util.Objects;

public class ImageFileUtils {

  private static final char EXTENSION_SEPARATOR = '.';

  public static String getExtension(final String path) {
    Objects.requireNonNull(path, "path must not be null");

    final int lastIndexOfExtensionSeparator = path.lastIndexOf(EXTENSION_SEPARATOR);

    if (lastIndexOfExtensionSeparator > 0 && lastIndexOfExtensionSeparator < path.length() - 1) {
      return path.substring(lastIndexOfExtensionSeparator + 1);
    }

    throw new IllegalArgumentException("path does not contain extension");
  }

  public static ImageLoader getImageLoader(final FileExtension fileExtension) {
    switch (fileExtension) {
      case RAW:
        return new RAWLoader();
      case PGM:
        return new PGMLoader();
      case PPM:
        return new PPMLoader();
      default:
        return new JPGLoader();
    }
  }

  public static ImageWriter getImageWriter(final FileExtension fileExtension) {
    switch (fileExtension) {
      case RAW:
        return new RAWWriter();
      case PGM:
        return new PGMWriter();
      case PPM:
        return new PPMWriter();
      default:
        throw new IllegalArgumentException("extension is not supported");
    }
  }

  public static byte[] convertChannelToByteArray(final ImageByte image,
      final ChannelType channelType) {
    final byte[] byteArray = new byte[image.getDimensions()];

    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        byteArray[x * image.getWidth() + y] = image.getPixel(channelType, x, y);
      }
    }

    return byteArray;
  }

  public static String replaceExtensionAtFilePath(final String path, final String oldExtension,
      final String newExtension) {
    final String pathWithNoExtension = removeExtensionFromFilePath(path, oldExtension);

    return appendExtensionToFilePath(pathWithNoExtension, newExtension);
  }

  public static String appendExtensionToFilePath(final String pathWithNoExtension,
      final String newExtension) {

    return pathWithNoExtension + EXTENSION_SEPARATOR + newExtension;
  }

  public static String removeExtensionFromFilePath(final String path, final String extension) {
    Objects.requireNonNull(path, "path must not be null");
    Objects.requireNonNull(extension, "extension must not be null");

    return path.substring(0, path.length() - extension.length() - 1);
  }
}
