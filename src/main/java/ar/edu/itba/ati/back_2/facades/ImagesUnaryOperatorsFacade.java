package ar.edu.itba.ati.back_2.facades;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.operators.ImageUnaryOperator;
import ar.edu.itba.ati.back_2.operators.unary.DRC;
import ar.edu.itba.ati.back_2.operators.unary.GammaPower;
import ar.edu.itba.ati.back_2.operators.unary.Negative;
import ar.edu.itba.ati.back_2.operators.unary.ScalarProduct;
import ar.edu.itba.ati.back_2.operators.unary.Threshold;
import java.util.function.DoubleUnaryOperator;

public final class ImagesUnaryOperatorsFacade {

  public Image DRC(final Image image, final double highestValueToCompress) {
    return applyOperator(new DRC(highestValueToCompress), image);
  }

  // TODO check if the ImageUnaryOperator may be recycled || use a pool || create new one
  private ImageUnaryOperator getImageUnaryOperator(final DoubleUnaryOperator operator) {
    return new ImageUnaryOperator(operator);
  }

  public Image gammaPower(final Image image, final double gamma) {
    return applyOperator(new GammaPower(gamma), image);
  }

  public Image negative(final Image image) {
    return applyOperator(new Negative(), image);
  }

  public Image scalarProduct(final Image image, final double constant) {
    return applyOperator(new ScalarProduct(constant), image);
  }

  public Image threshold(final Image image, final double lowValue,
      final double threshold, final double highValue) {
    return applyOperator(new Threshold(lowValue, threshold, highValue), image);
  }

  private Image applyOperator(final DoubleUnaryOperator operator, final Image image) {
    return getImageUnaryOperator(operator).apply(image);
  }

  // TODO ContrastIncrease
}
