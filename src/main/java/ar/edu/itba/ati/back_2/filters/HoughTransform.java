package ar.edu.itba.ati.back_2.filters;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.cos;
import static java.lang.Math.max;
import static java.lang.Math.round;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toIntExact;
import static java.lang.Math.toRadians;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.IntPoint;
import ar.edu.itba.ati.back_2.placeholders.HoughTransformBoundaries;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class HoughTransform {

  private static final double WHITE_PIXEL_VALUE = 255;
  private static final double BLACK_PIXEL_VALUE = 0;

  private final Image image;
  private Image outputColorImage;
  private final HoughTransformBoundaries boundaries;
  private final double acceptanceThreshold;
  private final double epsilon;
  private final int[][] accumulator;

  private final int rhoBoundary;


  public HoughTransform(final Image image, Image outputColorImage,
      final HoughTransformBoundaries boundaries,
      final double acceptanceThreshold, final double epsilon) {
    this.image = Objects.requireNonNull(image);
    requireBinaryImage();

    this.outputColorImage = Objects.requireNonNull(outputColorImage);
    this.boundaries = Objects.requireNonNull(boundaries);
    this.acceptanceThreshold = requireBetweenBoundaries(acceptanceThreshold, 0, 1);
    this.epsilon = requirePositive(epsilon);

    final int D = max(image.getHeight(), image.getWidth());
    rhoBoundary = toIntExact(round(sqrt(2) * D));

    System.out.println("doubleRhoBoundary = " + rhoBoundary);

    final int rhoRows = rhoBoundary / boundaries.getRhoStep() + 1;
    final int thetaColumns =
        (boundaries.getThetaMax() - boundaries.getThetaMin()) / boundaries.getThetaStep();
    accumulator = new int[rhoRows][thetaColumns];
  }

  private double requirePositive(final double value) {
    if (value <= 0.0) {
      throw new IllegalArgumentException("Argument must be positive");
    }
    return value;
  }

  private double requireBetweenBoundaries(final double value, final double min, final double max) {
    if (value < min || value > max) {
      throw new IllegalArgumentException("Argument must be between bounds = " + value);
    }
    return value;
  }

  private void requireBinaryImage() {
    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {

        if (!isWhite(x, y) && !isBlack(x, y)) {
          throw new IllegalArgumentException("Image must be binary");
        }
      }
    }
  }

  public Image apply() {
    voteForWhitePoints();
    final int mostVotedValue = getMostVotedValue();
    final int acceptanceValue = toIntExact(round(mostVotedValue * acceptanceThreshold));
    final List<NormalConfiguration> configurations = getLinearFunctionsNormalConfigurations(
        acceptanceValue);

    System.out.println(configurations);

    return getImageWithLinearFunctions(configurations);
  }

  private void voteForWhitePoints() {
    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        if (isWhite(x, y)) {
          vote(new IntPoint(x, y));
        }
      }
    }
  }

  private boolean isWhite(final int x, final int y) {
    return image.getPixel(ChannelType.GREY, x, y) == WHITE_PIXEL_VALUE;
  }

  private void vote(final IntPoint whitePoint) {
    for (int i = 0; i < accumulator.length; i++) {
      for (int j = 0; j < accumulator[0].length; j++) {
        final int rho = getRhoAtIndex(i);
        final double theta = getThetaAsRadiansAtIndex(j);

        if (isInsideLinearFunction(whitePoint, rho, theta)) {
          accumulator[i][j]++;
        }
      }
    }
  }

  private boolean isInsideLinearFunction(final IntPoint point, final int rho, final double theta) {
    return abs(point.getX() * cos(theta) + point.getY() * sin(theta) - rho) < epsilon;
  }

  private int getMostVotedValue() {
    int maxVotes = Integer.MIN_VALUE;

    for (final int[] rhoAccumulator : accumulator) {
      for (final int votes : rhoAccumulator) {
        maxVotes = max(maxVotes, votes);
      }
    }

    return maxVotes;
  }

  private List<NormalConfiguration> getLinearFunctionsNormalConfigurations(
      final int acceptanceValue) {

    final List<NormalConfiguration> configurations = new ArrayList<>();

    for (int i = 0; i < accumulator.length; i++) {
      for (int j = 0; j < accumulator[0].length; j++) {
        if (isAcceptedByVotes(i, j, acceptanceValue)) {
          configurations
              .add(new NormalConfiguration(getRhoAtIndex(i), getThetaAsRadiansAtIndex(j)));
        }
      }
    }

    return configurations;
  }

  private int getRhoAtIndex(final int i) {
    return boundaries.getRhoMin() + i * boundaries.getRhoStep();
  }

  private double getThetaAsRadiansAtIndex(final int j) {
    return toRadians(boundaries.getThetaMin() + j * boundaries.getThetaStep());
  }

  private boolean isAcceptedByVotes(final int i, final int j, final int acceptanceValue) {
    return accumulator[i][j] >= acceptanceValue;
  }

  private Image getImageWithLinearFunctions(final List<NormalConfiguration> configurations) {
    //  final Image newImage = ChannelTypeImagesFactory.getGreyscaleImage(image.getHeight(), image.getWidth());
    //I Will draw the lines over a color copy of the original image =)

    for (final NormalConfiguration configuration : configurations) {
      drawLineInImage(outputColorImage, configuration);
    }

    return outputColorImage;
  }

  private void drawLineInImage(final Image newImage, final NormalConfiguration configuration) {
    int height = image.getHeight();
    int width = image.getWidth();

    int r = configuration.getRho();
    double theta = configuration.getTheta();

    // Draw edges in output array
    double tSin = sin(theta);
    double tCos = cos(theta);

    if (theta < PI * 0.25 || theta > PI * 0.75) {
      // Draw horizontal-sh lines
      for (int y = 0; y < width; y++) {
        int x = (int) ((r - (y * tSin)) / tCos);
        if (x < height && x >= 0) {
          newImage.setPixel(ChannelType.RED, x, y, 255);
          newImage.setPixel(ChannelType.GREEN, x, y, 0);
          newImage.setPixel(ChannelType.BLUE, x, y, 0);
        }
      }
    } else {
      // Draw vertical-ish lines
      for (int x = 0; x < height; x++) {
        int y = (int) ((r - (x * tCos)) / tSin);
        if (y < width && y >= 0) {
          newImage.setPixel(ChannelType.RED, x, y, 255);
          newImage.setPixel(ChannelType.GREEN, x, y, 0);
          newImage.setPixel(ChannelType.BLUE, x, y, 0);
        }
      }
    }
  }

  public boolean isBlack(final int x, final int y) {
    return image.getPixel(ChannelType.GREY, x, y) == BLACK_PIXEL_VALUE;
  }

  public class NormalConfiguration {

    private final int rho;
    private final double theta;

    private NormalConfiguration(final int rho, final double theta) {
      this.rho = rho;
      this.theta = theta;
    }

    public int getRho() {
      return rho;
    }

    public double getTheta() {
      return theta;
    }

    @Override
    public String toString() {
      return "NormalConfiguration{" +
          "rho=" + rho +
          ", theta=" + theta +
          '}';
    }
  }
}
