package ar.edu.itba.ati.back_2.tracking;

import ar.edu.itba.ati.back_2.filters.GaussWindowFilter;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.IntPoint;
import ar.edu.itba.ati.back_2.utils.ArrayUtils;
import ar.edu.itba.ati.back_2.utils.ImageComparatorByChannelTypes;

import java.util.*;

public class LevelSet {

  // This is the euclidean norm of [256,256,256]
  private static final double POSSIBILITIES_NORM = Math.sqrt(3 * Math.pow(256, 2));

  private final Image image;

  private final int maxIterations;

  private final List<IntPoint> listIn;
  private final List<IntPoint> listOut;

  private int[][] phi;

  // Trying to persist the color to avoid losing the tracked object. Not elegant, I'll model it as a returned variable, promise
  private static double[] inPixelsMean = new double[3];

  private GaussWindowFilter gaussFilter;

  public LevelSet(final Image image, final int maxIterations, final List<IntPoint> listIn,
      final List<IntPoint> listOut) {
    System.out.println(Arrays.toString(listIn.toArray()));
    this.image = ImageComparatorByChannelTypes.requireRGB(Objects.requireNonNull(image));
    this.maxIterations = requirePositive(maxIterations);
    this.listIn = Objects.requireNonNull(listIn);
    this.listOut = Objects.requireNonNull(listOut);
    createInitialPhi();
    createInPixelsMean();
    gaussFilter = new GaussWindowFilter(1, 5);
  }

  public LevelSet(final Image image, final int maxIterations, final List<IntPoint> listIn,
      final List<IntPoint> listOut, final int[][] phi) {
    this.image = ImageComparatorByChannelTypes.requireRGB(Objects.requireNonNull(image));
    this.maxIterations = requirePositive(maxIterations);
    this.listIn = Objects.requireNonNull(listIn);
    this.listOut = Objects.requireNonNull(listOut);
    this.phi = phi;
    //createInPixelsMean();
    gaussFilter = new GaussWindowFilter(1, 5);
  }

  public int[][] getPhi() {
    return phi;
  }

  private void createInitialPhi() {
    phi = new int[image.getHeight()][image.getWidth()];

    for (final IntPoint inPoint : listIn) {
      phi[inPoint.getX()][inPoint.getY()] = -1;
    }
    for (final IntPoint outPoint : listOut) {
      phi[outPoint.getX()][outPoint.getY()] = 1;
    }

    for (final IntPoint inPoint : listIn) {
      final int x = inPoint.getX();
      final int y = inPoint.getY();

      if (x - 1 >= 0 && phi[x - 1][y] == 0) {
        fillPixelsWithValue(new IntPoint(x - 1, y), -3);
      }
      if (x + 1 < image.getHeight() && phi[x + 1][y] == 0) {
        fillPixelsWithValue(new IntPoint(x + 1, y), -3);
      }
      if (y - 1 >= 0 && phi[x][y - 1] == 0) {
        fillPixelsWithValue(new IntPoint(x, y - 1), -3);
      }
      if (y + 1 < image.getWidth() && phi[x][y + 1] == 0) {
        fillPixelsWithValue(new IntPoint(x, y + 1), -3);
      }
    }

    for (int x = 0; x < phi.length; x++) {
      for (int y = 0; y < phi[0].length; y++) {
        if (phi[x][y] == 0) {
          phi[x][y] = 3;
        }
      }
    }
  }

  private void createInPixelsMean() {
    int inTotalPixels = 0;
    inPixelsMean = new double[3]; // Create one mean for each channel type

    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        if (phi[x][y] == -3) {
          inTotalPixels++;

          inPixelsMean[0] += image.getPixel(ChannelType.RED, x, y);
          inPixelsMean[1] += image.getPixel(ChannelType.GREEN, x, y);
          inPixelsMean[2] += image.getPixel(ChannelType.BLUE, x, y);
        }
      }
    }

    if (inTotalPixels > 0) {
      inPixelsMean[0] /= inTotalPixels;
      inPixelsMean[1] /= inTotalPixels;
      inPixelsMean[2] /= inTotalPixels;
    }
//    System.out.println("AVG: ("+inPixelsMean[0]+" , "+inPixelsMean[1]+" , "+inPixelsMean[2]+")");
  }

  private void fillPixelsWithValue(final IntPoint position, final int value) {
    final Queue<IntPoint> positions = new LinkedList<>();
    positions.add(position);

    while (!positions.isEmpty()) {
      final IntPoint currPosition = positions.poll();
      final int x = currPosition.getX();
      final int y = currPosition.getY();
      if (phi[x][y] != value) {
        phi[x][y] = value;

        if (x - 1 >= 0 && phi[x - 1][y] == 0) {
          positions.add(new IntPoint(x - 1, y));
        }
        if (x + 1 < image.getHeight() && phi[x + 1][y] == 0) {
          positions.add(new IntPoint(x + 1, y));
        }
        if (y - 1 >= 0 && phi[x][y - 1] == 0) {
          positions.add(new IntPoint(x, y - 1));
        }
        if (y + 1 < image.getWidth() && phi[x][y + 1] == 0) {
          positions.add(new IntPoint(x, y + 1));
        }
      }
    }
  }

  private int requirePositive(final int value) {
    if (value <= 0) {
      throw new IllegalArgumentException("value must be positive");
    }
    return value;
  }

  public void firstCycle() {
    int i = 0;
    for (; i < maxIterations && !hasConverged(); i++) {
     //First cycle
      trackOutSpeed(false);
      switchOut();
      trackInSpeed(false);
      switchIn();
    }
  //  System.out.println("Total Iterations: "+i);
    //Second cycle
    trackOutSpeed(true);
    switchOut();
    trackInSpeed(true);
    switchIn();
  }

  private boolean hasConverged() {
    for (final IntPoint position : listIn) {
      if (getF(position) < 0) {
        return false;
      }
    }

    for (final IntPoint position : listOut) {
      if (getF(position) > 0) {
        return false;
      }
    }

    return true;
  }


  private void trackOutSpeed(boolean isSmoothing) {
    final Iterator<IntPoint> outIterator = new LinkedList<>(listOut).iterator();

    while (outIterator.hasNext()) {
      final IntPoint position = outIterator.next();

      int f;
      if(isSmoothing) {
        f = getSmoothingF(position);
      }else{
        f = getF(position);
      }

      if ((!isSmoothing && f > 0) || (isSmoothing && f < 0)) {
        listOut.remove(position);
        listIn.add(position);

        phi[position.getX()][position.getY()] = -1;

        trackNeighboursWithValue(listOut, position, 3, 1);
      }
    }
  }

  private void trackNeighboursWithValue(final List<IntPoint> list, final IntPoint position,
      final int value, final int valueToReplace) {
    final int x = position.getX();
    final int y = position.getY();

    if (x - 1 >= 0 && phi[x - 1][y] == value) {
      list.add(new IntPoint(x - 1, y));
      phi[x - 1][y] = valueToReplace;
    }
    if (x + 1 < image.getHeight() && phi[x + 1][y] == value) {
      list.add(new IntPoint(x + 1, y));
      phi[x + 1][y] = valueToReplace;
    }
    if (y - 1 >= 0 && phi[x][y - 1] == value) {
      list.add(new IntPoint(x, y - 1));
      phi[x][y - 1] = valueToReplace;
    }
    if (y + 1 < image.getWidth() && phi[x][y + 1] == value) {
      list.add(new IntPoint(x, y + 1));
      phi[x][y + 1] = valueToReplace;
    }
  }

  private int getF(final IntPoint position) {
    final int x = position.getX();
    final int y = position.getY();
    final double[] delta = new double[]{
        image.getPixel(ChannelType.RED, x, y) - inPixelsMean[0],
        image.getPixel(ChannelType.GREEN, x, y) - inPixelsMean[1],
        image.getPixel(ChannelType.BLUE, x, y) - inPixelsMean[2],
    };
    final double probability =
        ArrayUtils.euclideanNorm2(delta) / POSSIBILITIES_NORM;

    return probability < 0.1 ? 1 : -1;
  }

  private int getSmoothingF(final IntPoint position){
    return (int) this.gaussFilter.applyFilter(phi, position.getX(), position.getY());
  }

  private void trackInSpeed(boolean isSmoothing) {
    final Iterator<IntPoint> inIterator = new LinkedList<>(listIn).iterator();

    while (inIterator.hasNext()) {
      final IntPoint position = inIterator.next();
      final int f;

      if(isSmoothing) {
        f = getSmoothingF(position);
      }else{
        f = getF(position);
      }

      if ((!isSmoothing && f < 0) || (isSmoothing && f > 0)) {
        listIn.remove(position);
        listOut.add(position);

        phi[position.getX()][position.getY()] = 1;

        trackNeighboursWithValue(listIn, position, -3, -1);
      }
    }
  }

  private void switchIn() {
    final Iterator<IntPoint> outIterator = listOut.iterator();

    while (outIterator.hasNext()) {
      final IntPoint position = outIterator.next();

      if (!isListOut(position)) {
        outIterator.remove();
        phi[position.getX()][position.getY()] = 3;
      }
    }
  }

  private boolean isListOut(final IntPoint position) {
    final int x = position.getX();
    final int y = position.getY();

    if (x - 1 >= 0 && phi[x - 1][y] < 0) {
      return true;
    } else if (x + 1 < image.getHeight() && phi[x + 1][y] < 0) {
      return true;
    } else if (y - 1 >= 0 && phi[x][y - 1] < 0) {
      return true;
    } else if (y + 1 < image.getWidth() && phi[x][y + 1] < 0) {
      return true;
    }

    return false;
  }

  private void switchOut() {
    final Iterator<IntPoint> inIterator = listIn.iterator();

    while (inIterator.hasNext()) {
      final IntPoint position = inIterator.next();

      if (!isListIn(position)) {
        inIterator.remove();
        phi[position.getX()][position.getY()] = -3;
      }
    }
  }

  private boolean isListIn(final IntPoint position) {
    final int x = position.getX();
    final int y = position.getY();

    if (x - 1 >= 0 && phi[x - 1][y] > 0) {
      return true;
    } else if (x + 1 < image.getHeight() && phi[x + 1][y] > 0) {
      return true;
    } else if (y - 1 >= 0 && phi[x][y - 1] > 0) {
      return true;
    } else if (y + 1 < image.getWidth() && phi[x][y + 1] > 0) {
      return true;
    }

    return false;
  }
}
