package ar.edu.itba.ati.back_2.random;

import static java.lang.Math.log;
import static java.lang.Math.sqrt;

import ar.edu.itba.ati.back_2.interfaces.RandomGenerator;
import java.util.Random;

public class RayleighRandomGenerator implements RandomGenerator {

  private final Random uniformRandom;
  private final double psi;

  public RayleighRandomGenerator(final double psi) {
    this.uniformRandom = new Random();
    this.psi = psi;
  }

  @Override
  public double getNext() {
    return this.psi * sqrt(-2 * log(1 - this.uniformRandom.nextDouble()));
  }
}
