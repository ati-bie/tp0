package ar.edu.itba.ati.back_2.operators.unary;

import java.util.function.DoubleUnaryOperator;

public final class Threshold implements DoubleUnaryOperator {

  private final double lowValue;
  private final double threshold;
  private final double highValue;

  public Threshold(final double lowValue, final double threshold, final double highValue) {
    if (Double.compare(lowValue, threshold) > 0 || Double.compare(highValue, threshold) < 0) {
      throw new IllegalArgumentException(
          "threshold must be between the lowValue and the highValue");
    }

    this.lowValue = lowValue;
    this.threshold = threshold;
    this.highValue = highValue;
  }

  @Override
  public double applyAsDouble(final double x) {
    if (Double.compare(x, lowValue) < 0 || Double.compare(x, highValue) > 0) {
      throw new IllegalArgumentException("x must be between the lowValue and the highValue");
    }

    return x <= threshold ? lowValue : highValue;
  }
}
