package ar.edu.itba.ati.back_2.facades;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.RandomGenerator;
import ar.edu.itba.ati.back_2.operators.ImageNoiseOperator;
import ar.edu.itba.ati.back_2.random.ExponentialRandomGenerator;
import ar.edu.itba.ati.back_2.random.GaussianRandomGenerator;
import ar.edu.itba.ati.back_2.random.RayleighRandomGenerator;
import ar.edu.itba.ati.back_2.random.UniformRandomGenerator;
import java.util.function.DoubleBinaryOperator;

public final class ImagesNoiseOperatorsFacade {

  public Image gauss(final Image image, final double noisePercentage, final double mean,
      final double standardDeviation) {

    return applyNoise(image, noisePercentage, new GaussianRandomGenerator(mean, standardDeviation),
        Double::sum);
  }

  public Image exponential(final Image image, final double noisePercentage, final double lambda) {
    return applyNoise(image, noisePercentage, new ExponentialRandomGenerator(lambda),
        (left, right) -> left * right);
  }

  public Image rayleigh(final Image image, final double noisePercentage, final double psi) {
    return applyNoise(image, noisePercentage, new RayleighRandomGenerator(psi),
        (left, right) -> left * right);
  }

  public Image saltAndPepper(final Image image, final double noisePercentage, final double p0,
      final double p1) {
    return applyNoise(image, noisePercentage, new UniformRandomGenerator(),
        (pixel, random) -> { //TODO order of parameters should not matter here
          if (random <= p0) {
            return 0;
          }
          if (random >= p1) {
            return 255;
          }
          return pixel;
        });
  }

  private Image applyNoise(final Image image, final double noisePercentage,
      final RandomGenerator randomGenerator, final DoubleBinaryOperator operator) {

    return getImageNoiseOperator(randomGenerator, noisePercentage, operator).apply(image);
  }

  private ImageNoiseOperator getImageNoiseOperator(final RandomGenerator randomGenerator,
      final double noisePercentage, final DoubleBinaryOperator operator) {

    return new ImageNoiseOperator(randomGenerator, operator, noisePercentage);
  }
}
