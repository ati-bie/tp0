package ar.edu.itba.ati.back_2.operators.unary;

import java.util.function.DoubleUnaryOperator;

public class Power implements DoubleUnaryOperator {

  @Override
  public double applyAsDouble(double operand) {
    return Math.pow(operand, 2);
  }
}
