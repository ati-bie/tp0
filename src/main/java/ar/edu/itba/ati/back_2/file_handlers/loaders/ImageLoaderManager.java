package ar.edu.itba.ati.back_2.file_handlers.loaders;

import ar.edu.itba.ati.back_2.file_handlers.FileExtension;
import ar.edu.itba.ati.back_2.file_handlers.ImageFileUtils;
import ar.edu.itba.ati.back_2.interfaces.Image;
import java.util.Objects;

public class ImageLoaderManager {

  public static Image loadImageFromFile(final String path) {
    Objects.requireNonNull(path, "path must not be null");

    final String extension = ImageFileUtils.getExtension(path);

    try {
      return loadImageWithExtension(extension, path);
    } catch (final IllegalArgumentException e) {
      throw new IllegalArgumentException("extension is not supported ,"+e.getMessage());
    }
  }

  private static Image loadImageWithExtension(final String extension, final String path) {
    final FileExtension fileExtension = FileExtension.valueOf(extension.toUpperCase());
    final ImageLoader imageLoader = ImageFileUtils.getImageLoader(fileExtension);

    return imageLoader.loadImageFromFile(path);
  }


}
