package ar.edu.itba.ati.back_2.file_handlers.writers;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.file_handlers.ImageFileUtils;
import ar.edu.itba.ati.back_2.models.ChannelType;
import java.io.FileOutputStream;
import java.io.IOException;

public class PPMWriter implements ImageWriter {

  @Override
  public void writeImageToFile(final ImageByte image, final String path) {
    try (final FileOutputStream out = new FileOutputStream(path)) {
      final byte[] headerBytes = ("P6\n" + image.getWidth() + " " + image.getHeight() + "\n255\n")
          .getBytes();
      final byte dataToWrite[] = new byte[image.getWidth() * image.getHeight() * 3];
      final byte[][] bandsData = new byte[][]{
          ImageFileUtils.convertChannelToByteArray(image, ChannelType.RED),
          ImageFileUtils.convertChannelToByteArray(image, ChannelType.GREEN),
          ImageFileUtils.convertChannelToByteArray(image, ChannelType.BLUE)
      };

      for (int i = 0; i < image.getDimensions() * bandsData.length; i++) {
        dataToWrite[i] = bandsData[i % 3][i / 3];
      }

      out.write(headerBytes);
      out.write(dataToWrite);
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }
}
