package ar.edu.itba.ati.back_2.file_handlers.writers;

import static ar.edu.itba.ati.back_2.models.ChannelType.GREY;

import ar.edu.itba.ati.back_2.adapter.ImageByte;
import ar.edu.itba.ati.back_2.file_handlers.ImageFileUtils;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PGMWriter implements ImageWriter {

  @Override
  public void writeImageToFile(final ImageByte image, final String path) {
    try (final FileOutputStream imageOutputStream = new FileOutputStream(path)) {
      final byte[] greyByteArray = ImageFileUtils.convertChannelToByteArray(image, GREY);
      byte[] headerBytes = ("P5\n" + image.getWidth() + " " + image.getHeight() + "\n255\n")
          .getBytes();

      imageOutputStream.write(headerBytes);
      imageOutputStream.write(greyByteArray);
      imageOutputStream.close();
    } catch (final FileNotFoundException e) {
      throw new IllegalArgumentException("file does not exist: " + path);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
