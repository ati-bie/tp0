package ar.edu.itba.ati.back_2.models;

import ar.edu.itba.ati.back_2.interfaces.Channel;

public final class ChannelImp implements Channel {

  private final double[][] matrix;

  ChannelImp(final int height, final int width) {
    if (height <= 0 || width <= 0) {
      throw new IllegalArgumentException("Height and width must be greater than zero");
    }

    matrix = new double[height][width];
  }

  public void setPixel(final int x, final int y, final double value) {
    matrix[x][y] = value;
  }

  public double getPixel(final int x, final int y) {
    return matrix[x][y];
  }
}
