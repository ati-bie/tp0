package ar.edu.itba.ati.back_2.models;

public class Pair {

  private final double min;
  private final double max;

  public Pair(final double min, final double max) {
    this.min = min;
    this.max = max;
  }

  public double getMin() {
    return min;
  }

  public double getMax() {
    return max;
  }

  public double getDifference() {
    return max - min;
  }

  @Override
  public String toString() {
    return "Pair{" +
        "min=" + min +
        ", max=" + max +
        '}';
  }
}
