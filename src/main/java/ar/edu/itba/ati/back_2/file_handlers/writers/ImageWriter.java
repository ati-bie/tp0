package ar.edu.itba.ati.back_2.file_handlers.writers;

import ar.edu.itba.ati.back_2.adapter.ImageByte;

public interface ImageWriter {

  void writeImageToFile(final ImageByte image, final String path);
}
