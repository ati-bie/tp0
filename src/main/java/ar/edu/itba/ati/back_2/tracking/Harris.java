package ar.edu.itba.ati.back_2.tracking;

import static ar.edu.itba.ati.back_2.models.ChannelType.GREY;
import static java.lang.Math.pow;

import ar.edu.itba.ati.back_2.filters.GaussWindowFilter;
import ar.edu.itba.ati.back_2.filters.borderDetectors.PrewittHorizontalFilter;
import ar.edu.itba.ati.back_2.filters.borderDetectors.PrewittVerticalFilter;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.IntPoint;
import ar.edu.itba.ati.back_2.models.Pair;
import ar.edu.itba.ati.back_2.operators.ImageBinaryOperator;
import ar.edu.itba.ati.back_2.operators.ImageFilterOperator;
import ar.edu.itba.ati.back_2.operators.ImageUnaryOperator;
import ar.edu.itba.ati.back_2.operators.unary.Power;
import ar.edu.itba.ati.back_2.utils.ChannelTypeImagesFactory;
import ar.edu.itba.ati.back_2.utils.ImageComparatorByChannelTypes;
import ar.edu.itba.ati.back_2.utils.ImagesStatistics;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Harris {

  /**
   * Empirical constant
   */
  private final static double K = 0.04;

  private final Image image;
  private final double threshold;

  private final ImageUnaryOperator powerUnaryOperator;
  private final ImageBinaryOperator productBinaryOperator;

  public Harris(final Image image, final double threshold) {
    this.image = ImageComparatorByChannelTypes.requireGreysacale(Objects.requireNonNull(image));
    this.threshold = /*requireBetweenBoundaries(threshold, 0, 1); */ threshold;
    powerUnaryOperator = new ImageUnaryOperator(new Power());
    productBinaryOperator = new ImageBinaryOperator((left, right) -> left * right);
  }

  private double requireBetweenBoundaries(final double value, final double min, final double max) {
    if (value < min || value > max) {
      throw new IllegalArgumentException(
          "Argument must be between bounds [" + min + ", " + max + "] - Value = " + value);
    }
    return value;
  }

  public List<IntPoint> run() {
    final List<IntPoint> points = new LinkedList<>();

    // Step 1
    final Image iXImage = new ImageFilterOperator(new PrewittVerticalFilter(3)).apply(image);
    final Image iYImage = new ImageFilterOperator(new PrewittHorizontalFilter(3)).apply(image);

    // Step 2
    final Image iXXImage = powerUnaryOperator.apply(iXImage);
    final Image gaussianIXXImage = new ImageFilterOperator(new GaussWindowFilter(2, 7))
        .apply(iXXImage);

    final Image iYYImage = powerUnaryOperator.apply(iYImage);
    final Image gaussianIYYImage = new ImageFilterOperator(new GaussWindowFilter(2, 7))
        .apply(iYYImage);

    // Step 3
    final Image iXYImage = productBinaryOperator.apply(iXImage, iYImage);
    final Image gaussianIXYImage = new ImageFilterOperator(new GaussWindowFilter(2, 7))
        .apply(iXYImage);

    // Step 4
    final Image resultImage = ChannelTypeImagesFactory
        .getGreyscaleImage(image.getHeight(), image.getWidth());

    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        final double iXX = gaussianIXXImage.getPixel(GREY, x, y);
        final double iYY = gaussianIYYImage.getPixel(GREY, x, y);
        final double iXYSquared = pow(gaussianIXYImage.getPixel(GREY, x, y), 2);

        final double result = (iXX * iYY - iXYSquared) - K * pow(iXX + iYY, 2);
        resultImage.setPixel(GREY, x, y, result);
      }
    }

    final Pair minMaxPair = ImagesStatistics
        .getChannelGlobalMinAndMaxPixel(resultImage);
    final double minMaxDiff = minMaxPair.getDifference();

    // Step 5
    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        final double r = resultImage.getPixel(GREY, x, y);

        if (r > threshold /* * minMaxDiff + minMaxPair.getMin()*/) {
          points.add(new IntPoint(x, y));
        }
      }
    }

    return points;
  }

}
