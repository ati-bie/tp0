package ar.edu.itba.ati.back_2.random;

import ar.edu.itba.ati.back_2.interfaces.RandomGenerator;
import java.util.Random;

public class UniformRandomGenerator implements RandomGenerator {

  private final Random uniformRandom;

  public UniformRandomGenerator() {
    this.uniformRandom = new Random();
  }

  @Override
  public double getNext() {
    return uniformRandom.nextDouble();
  }
}
