package ar.edu.itba.ati.back_2.utils;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

public final class ImageComparatorByChannelTypes {

  private static final Collection<ChannelType> RGB_CHANNELS = Arrays
      .asList(ChannelType.RED, ChannelType.GREEN, ChannelType.BLUE);

  private static final Collection<ChannelType> GREYSCALE_CHANNEL = Collections
      .singleton(ChannelType.GREY);

  public static boolean areOfTheSameType(final Image firstImage, final Image secondImage) {
    Objects.requireNonNull(firstImage, "firstImage must not be null");
    Objects.requireNonNull(secondImage, "secondImage must not be null");

    final Collection<ChannelType> firstImageChannelTypes = firstImage.getChannelTypes();
    final Collection<ChannelType> secondImageChannelTypes = secondImage.getChannelTypes();

    return areTheSameSize(firstImageChannelTypes, secondImageChannelTypes) &&
        areMutuallyIncluded(firstImageChannelTypes, secondImageChannelTypes);
  }

  private static boolean areTheSameSize(final Collection<ChannelType> firstChannelTypes,
      final Collection<ChannelType> secondChannelTypes) {

    return firstChannelTypes.size() == secondChannelTypes.size();
  }

  private static boolean areMutuallyIncluded(final Collection<ChannelType> firstChannelTypes,
      final Collection<ChannelType> secondChannelTypes) {

    return firstChannelTypes.containsAll(secondChannelTypes) &&
        secondChannelTypes.containsAll(firstChannelTypes);
  }

  public static Image requireRGB(final Image image) {
    final Collection<ChannelType> imageChannelTypes = image.getChannelTypes();
    if (areTheSameSize(imageChannelTypes, RGB_CHANNELS) && areMutuallyIncluded(imageChannelTypes,
        RGB_CHANNELS)) {
      return image;
    }
    throw new IllegalArgumentException("image has not RGB channel types");
  }

  public static Image requireGreysacale(final Image image) {
    final Collection<ChannelType> imageChannelTypes = image.getChannelTypes();
    if (areTheSameSize(imageChannelTypes, GREYSCALE_CHANNEL) && areMutuallyIncluded(
        imageChannelTypes,
        GREYSCALE_CHANNEL)) {
      return image;
    }
    throw new IllegalArgumentException("image has not greyscale channel type");
  }
}
