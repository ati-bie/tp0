package ar.edu.itba.ati.back_2.file_handlers.loaders;

import ar.edu.itba.ati.back_2.interfaces.Image;

public interface ImageLoader {

  Image loadImageFromFile(final String path);
}
