package ar.edu.itba.ati.back_2.operators.unary;

import static java.lang.Math.sin;
import static java.lang.Math.tan;

import ar.edu.itba.ati.back_2.filters.HoughTransform.NormalConfiguration;
import ar.edu.itba.ati.back_2.models.Point;
import java.util.Objects;
import java.util.function.DoubleUnaryOperator;

public final class LinearFunction implements DoubleUnaryOperator {

  private final double m;
  private final double b;

  public LinearFunction(final Point firstPoint, final Point secondPoint) {
    Objects.requireNonNull(firstPoint, "firstPoint must not be null");
    Objects.requireNonNull(secondPoint, "secondPoint must not be null");

    if (Double.compare(firstPoint.getX(), secondPoint.getX()) == 0) {
      throw new IllegalArgumentException("X coordinate of both points are numerically equal");
    }

    final Point slopeRatioPoint = firstPoint.subtract(secondPoint);
    this.m = slopeRatioPoint.getY() / slopeRatioPoint.getX();

    this.b = firstPoint.getY() - m * firstPoint.getX();
  }

  public LinearFunction(final NormalConfiguration normalConfiguration) {
    this.m = -1 / tan(normalConfiguration.getTheta());
    this.b = normalConfiguration.getRho() / sin(normalConfiguration.getTheta());
  }

  @Override
  public double applyAsDouble(final double x) {
    return m * x + b;
  }

  public double applyInverseAsDouble(final double y) {
    return (y - b) / m;
  }

  @Override
  public String toString() {
    return "LinearFunction{" +
        "m=" + m +
        ", b=" + b +
        '}';
  }
}
