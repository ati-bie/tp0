package ar.edu.itba.ati.back_2.thresholds;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.ThresholdOperator;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.Pair;
import ar.edu.itba.ati.back_2.operators.ImageUnaryOperator;
import ar.edu.itba.ati.back_2.operators.unary.LinearTransformation;
import ar.edu.itba.ati.back_2.operators.unary.Threshold;
import ar.edu.itba.ati.back_2.utils.ImagesStatistics;

import java.util.ArrayList;
import java.util.List;

public class OtsuThreshold implements ThresholdOperator {
    private final int colorsAmount = 256;

    private double minValue, maxValue;
    private Image image;

    @Override
    public Image apply(Image originalImage) {
        double threshold = getThreshold(originalImage);
        return new ImageUnaryOperator(new Threshold(this.minValue, threshold, this.maxValue)).apply(this.image);
    }


    public double getThreshold(Image originalImage) {
        Pair pair = ImagesStatistics.getChannelGlobalMinAndMaxPixel(originalImage);

        this.image = new ImageUnaryOperator(new LinearTransformation(pair.getMin(), pair.getMax()))
                .apply(originalImage);


        double[] p = new double[colorsAmount];
        int pixel;
        this.maxValue = Double.MIN_VALUE;
        this.minValue = Double.MAX_VALUE;
        final double ratioToSum = 1.0 / image.getDimensions();

        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                pixel = (int)Math.round(image.getPixel(ChannelType.GREY, i, j));
                p[pixel] += ratioToSum;

                minValue = Math.min(minValue, image.getPixel(ChannelType.GREY, i, j));
                maxValue = Math.max(maxValue, image.getPixel(ChannelType.GREY, i, j));
            }
        }

        // calculate cumulative sums and means
        int maxExistentGray = 0;
        double[] cumulativeSums = new double[colorsAmount];
        cumulativeSums[0] = p[0];
        double[] cumulativeMeans = new double[colorsAmount];

        for (int color = 1; color < colorsAmount; color++) {
            cumulativeSums[color] = cumulativeSums[color-1] + p[color];
            cumulativeMeans[color] = cumulativeMeans[color-1] + color * p[color];

            if (p[color] != 0) {
                maxExistentGray = color;
            }
        }
        double globalMean = cumulativeMeans[maxExistentGray];

        // calculate variances
        List<Integer> maxVariances = new ArrayList<>();
        double maxVariance = -1, currentVariance, currentCumSum;
        for (int color = 0; color < colorsAmount; color++) {
            currentCumSum = cumulativeSums[color];
            currentVariance = Math.pow(globalMean * currentCumSum - cumulativeMeans[color], 2)
                    / (currentCumSum * (1 - currentCumSum));

            if (currentVariance == maxVariance) {
                maxVariances.add(color);
            } else {
                if (currentVariance > maxVariance) {
                    maxVariances.clear();
                    maxVariances.add(color);
                    maxVariance = currentVariance;
                }
            }
        }

        // calculate threshold and apply it
        double threshold = maxVariances.size() == 1 ?
                maxVariances.get(0) : maxVariances.stream().mapToInt(x -> x).average().getAsDouble();
        System.out.println(threshold);

        return threshold;
    }
}
