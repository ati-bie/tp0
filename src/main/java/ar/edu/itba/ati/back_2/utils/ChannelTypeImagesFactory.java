package ar.edu.itba.ati.back_2.utils;

import static ar.edu.itba.ati.back_2.models.ChannelType.BLUE;
import static ar.edu.itba.ati.back_2.models.ChannelType.GREEN;
import static ar.edu.itba.ati.back_2.models.ChannelType.GREY;
import static ar.edu.itba.ati.back_2.models.ChannelType.RED;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.ImageImp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public final class ChannelTypeImagesFactory {

  public static Image getImageDoubleWithChannelTypes(final Collection<ChannelType> channelTypes,
      final int height, final int width) {
    return new ImageImp(channelTypes, height, width);
  }

  public static Image getRGBImage(final int height, final int width) {
    return getImageDoubleWithChannelTypes(Arrays.asList(RED, GREEN, BLUE), height, width);
  }

  public static Image getGreyscaleImage(final int height, final int width) {
    return getImageDoubleWithChannelTypes(Collections.singletonList(GREY), height, width);
  }

  public static Image getGreyscaleImageWithDefaultValue(final int height, final int width,
      final double defaultValue) {

    final Image newImage = getGreyscaleImage(height, width);
    fillImageWithDefaultValue(newImage, defaultValue);
    return newImage;
  }


  public static void fillImageWithDefaultValue(final Image image, final double defaultValue) {
    for (int x = 0; x < image.getHeight(); x++) {
      for (int y = 0; y < image.getWidth(); y++) {
        for (final ChannelType channelType : image.getChannelTypes()) {
          image.setPixel(channelType, x, y, defaultValue);
        }
      }
    }
  }
}
