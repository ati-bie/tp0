package ar.edu.itba.ati.back_2.facades;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import java.awt.image.BufferedImage;

/**
 * Created by Marco on 10/12/2017.
 */
public class PlaygroundOperatorsFacade {

    public Image byteStreamToImage(Image image, BufferedImage bufferedImage) {

        for (int x = 0; x < image.getHeight(); x++) {
            for (int y = 0; y < image.getWidth(); y++) {
                int clr=  bufferedImage.getRGB(y, x);

                if(image.getChannelTypes().contains(ChannelType.GREY)){
                    int  blue  =  clr & 0x000000ff;
                    image.setPixel(ChannelType.GREY, x, y, blue);

                }else{
                    int  red   = (clr & 0x00ff0000) >> 16;
                    int  green = (clr & 0x0000ff00) >> 8;
                    int  blue  =  clr & 0x000000ff;

                    image.setPixel(ChannelType.RED, x, y, red);
                    image.setPixel(ChannelType.GREEN, x, y, green);
                    image.setPixel(ChannelType.BLUE, x, y, blue);
                }


            }
        }

        return image;
    }
}
