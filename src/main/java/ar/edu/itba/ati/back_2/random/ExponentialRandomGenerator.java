package ar.edu.itba.ati.back_2.random;

import ar.edu.itba.ati.back_2.interfaces.RandomGenerator;
import java.util.Random;

public class ExponentialRandomGenerator implements RandomGenerator {

  private final Random uniformRandom;
  private final double lambda;

  public ExponentialRandomGenerator(final double lambda) {
    if (lambda <= 0) {
      throw new IllegalArgumentException("lambda must be greater than zero");
    }

    uniformRandom = new Random();
    this.lambda = lambda;
  }

  @Override
  public double getNext() {
    return -Math.log(this.uniformRandom.nextDouble()) / this.lambda;
  }
}
