package ar.edu.itba.ati.back_2.adapter;

import ar.edu.itba.ati.back_2.file_handlers.loaders.ImageLoaderManager;
import ar.edu.itba.ati.back_2.file_handlers.writers.ImageWriterManager;
import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.utils.ChannelTypeImagesFactory;
import java.util.LinkedList;
import java.util.List;

/**
 * Facade adapter to load/write an image.
 *
 * Note: this is intended to be used only by the front-end
 */
public class ImagesFileHandlersAdapter {

  private ImageByte emptyImage;

  public ImageByte loadImage(final String path) {
    return new ImageByte(ImageLoaderManager.loadImageFromFile(path));
  }

  public void writeImage(final String path, final ImageByte imageByte) {
    ImageWriterManager.writeImageToFile(path, imageByte);
  }

  public ImageByte getEmptyColorImage(int height, int width) {
    List<ChannelType> channels = new LinkedList<>();

    channels.add(ChannelType.RED);
    channels.add(ChannelType.GREEN);
    channels.add(ChannelType.BLUE);

    //channels.add(ChannelType.GREY);

    Image newImage = ChannelTypeImagesFactory.getImageDoubleWithChannelTypes(channels, height, width);
    return new ImageByte(newImage);
  }

  public ImageByte getEmptyGrayscaleImage(int height, int width) {
    List<ChannelType> channels = new LinkedList<>();
    channels.add(ChannelType.GREY);
    Image newImage = ChannelTypeImagesFactory.getImageDoubleWithChannelTypes(channels, height, width);
    return new ImageByte(newImage);
  }
}
