package ar.edu.itba.ati.back_2.operators;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.utils.ImageComparatorByChannelTypes;
import ar.edu.itba.ati.back_2.utils.ImageDuplicator;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.DoubleBinaryOperator;

public final class ImageBinaryOperator implements BiFunction<Image, Image, Image> {

  private final DoubleBinaryOperator operator;

  public ImageBinaryOperator(final DoubleBinaryOperator operator) {
    Objects.requireNonNull(operator, "operator must not be null");

    this.operator = operator;
  }

  @Override
  public Image apply(final Image firstImage, final Image secondImage) {
    Objects.requireNonNull(firstImage, "firstImage must not be null");
    Objects.requireNonNull(secondImage, "secondImage must not be null");

    if (!ImageComparatorByChannelTypes.areOfTheSameType(firstImage, secondImage)) {
      throw new IllegalArgumentException("firstImage and secondImage are not of the same type");
    }

    final double valueToFill = 0;
    final Image newImage =
        ImageDuplicator.createImageWithDimensionCombination(firstImage, secondImage, valueToFill);

    for (final ChannelType channelType : newImage.getChannelTypes()) {
      applyToNewChannel(newImage, channelType, firstImage, secondImage);
    }

    return newImage;
  }

  private void applyToNewChannel(final Image newImage, final ChannelType channelType,
      final Image firstImage, final Image secondImage) {

    for (int x = 0; x < newImage.getHeight(); x++) {
      for (int y = 0; y < newImage.getWidth(); y++) {
        applyToNewPixel(newImage, channelType, x, y, firstImage, secondImage);
      }
    }
  }

  private void applyToNewPixel(final Image newImage, final ChannelType channelType,
      final int x, final int y, final Image firstImage, final Image secondImage) {

    final double firstImagePixel = getPixelOrDefaultValue(firstImage, channelType, x, y);
    final double secondImagePixel = getPixelOrDefaultValue(secondImage, channelType, x, y);
    final double result = operator.applyAsDouble(firstImagePixel, secondImagePixel);
    newImage.setPixel(channelType, x, y, result);
  }

  private double getPixelOrDefaultValue(final Image image, final ChannelType channelType,
      final int x, final int y) {

    if (x < image.getHeight() && y < image.getWidth()) {
      return image.getPixel(channelType, x, y);
    }
    return 0;
  }
}
