package ar.edu.itba.ati.back_2.operators;

import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.RandomGenerator;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.models.IntPoint;
import ar.edu.itba.ati.back_2.utils.ImageDuplicator;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.function.DoubleBinaryOperator;
import java.util.function.Function;

public final class ImageNoiseOperator implements Function<Image, Image> {

  private final RandomGenerator randomGenerator;
  private final double noisePercentage;
  private final DoubleBinaryOperator operator;

  public ImageNoiseOperator(final RandomGenerator randomGenerator,
      final DoubleBinaryOperator operator, final double noisePercentage) {
    Objects.requireNonNull(randomGenerator);
    Objects.requireNonNull(operator);

    if (!isNoisePercentageInRange(noisePercentage)) {
      throw new IllegalArgumentException("noisePercentage must be between 0 and 1");
    }

    this.randomGenerator = randomGenerator;
    this.operator = operator;
    this.noisePercentage = noisePercentage;
  }

  private boolean isNoisePercentageInRange(final double noisePercentage) {
    return Double.compare(noisePercentage, 0) >= 0 && Double.compare(noisePercentage, 1) <= 0;
  }

  @Override
  public Image apply(final Image image) {
    Objects.requireNonNull(image, "image cannot be null");

    final Set<IntPoint> randomPoints = getRandomPoints(image.getHeight(), image.getWidth());
    final Image newImage = ImageDuplicator.createImageWithSamePixelValues(image);
    final ChannelType channelType = getRandomChannelTypeToApplyNoise(newImage.getChannelTypes());

    for (final IntPoint randomPoint : randomPoints) {
      final int x = randomPoint.getX();
      final int y = randomPoint.getY();
      final double originalPixel = newImage.getPixel(channelType, x, y);
      final double nextRandom = randomGenerator.getNext();
      final double result = operator.applyAsDouble(originalPixel, nextRandom);

      newImage.setPixel(channelType, x, y, result);
    }

    return newImage;
  }

  private Set<IntPoint> getRandomPoints(final int height, final int width) {
    final Set<IntPoint> randomPoints = new HashSet<>();
    final Random uniformRandom = new Random();
    final int randomPointsToBeChosen = toIntExact(round(height * width * noisePercentage));
    int randomPointsChosen = 0;

    while (randomPointsChosen < randomPointsToBeChosen) {
      final IntPoint randomPoint = new IntPoint(uniformRandom.nextInt(height),
          uniformRandom.nextInt(width));

      if (randomPoints.add(randomPoint)) {
        randomPointsChosen++;
      }
    }

    return randomPoints;
  }

  private ChannelType getRandomChannelTypeToApplyNoise(Collection<ChannelType> channelTypes) {
    final Random uniformRandom = new Random();
    final int randomIndex = uniformRandom.nextInt(channelTypes.size());

    return (ChannelType) channelTypes.toArray()[randomIndex]; //TODO do not convert to array
  }
}
