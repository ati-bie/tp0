package ar.edu.itba.ati.back_2.filters.borderDetectors;

import static java.lang.Math.exp;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.pow;
import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.models.ChannelType;

public class LaplacianGaussianFilter implements WindowFilter {

  private double[][] window;

  public LaplacianGaussianFilter(final double sigma) {

    int side = toIntExact(round((6 * sigma) + 1));
//    side += (side + 1) % 2;

    int midPoint = max(3, ((side - 1) / 2));

    this.window = new double[side][side];
    for (int i = -midPoint; i < midPoint + 1; i++) {
      for (int j = -midPoint; j < midPoint + 1; j++) {
//        int midCenterX = abs(i - midPoint) - 1;
//        int midCenterY = abs(j - midPoint) - 1;

        double rSquared = pow(i, 2) + pow(j, 2);
        double sigmaSquared = pow(sigma, 2);

//        window[midPoint + i][midPoint + j] =
//            (-1 / (Math.sqrt(2 * Math.PI)*sigma * sigma * sigma)) *
//                (2-(rSquared)/(sigma * sigma))*
//                Math.exp( -(rSquared) / (2 * sigma * sigma));
//      }
        window[midPoint + i][midPoint + j] =
            ((rSquared - 2 * sigmaSquared) / pow(sigmaSquared, 2)) * exp(
                -rSquared / (2 * sigmaSquared));

      }
    }
  }

  @Override
  public double applyFilter(final Image image, final ChannelType channelType, final int x,
      final int y) {

    final int minOffset = (window.length - 1) / 2;
    double sum = 0;

    for (int i = -1 * minOffset; i <= minOffset; i++) {
      for (int j = -1 * minOffset; j <= minOffset; j++) {
        // Avoids getting elements outside window by repetition method
        int useI = max(min(x + i, image.getHeight() - 1), 0);
        int useJ = max(min(y + j, image.getWidth() - 1), 0);
        sum += window[i + minOffset][j + minOffset] * image.getPixel(channelType, useI, useJ);
      }
    }
    return sum;
  }
}
