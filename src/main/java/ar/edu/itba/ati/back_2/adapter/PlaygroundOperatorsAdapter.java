package ar.edu.itba.ati.back_2.adapter;

import ar.edu.itba.ati.back_2.facades.PlaygroundOperatorsFacade;
import java.awt.image.BufferedImage;

/**
 * Created by Marco on 10/12/2017.
 */
public class PlaygroundOperatorsAdapter {

    private final PlaygroundOperatorsFacade facade;

    public PlaygroundOperatorsAdapter() { facade = new PlaygroundOperatorsFacade(); }

    public ImageByte byteStreamToImage(final ImageByte imageByte, final BufferedImage bufferedImage) {
        return new ImageByte(facade.byteStreamToImage(imageByte.getImage(), bufferedImage)); }

}
