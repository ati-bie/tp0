package ar.edu.itba.ati.back_2.file_handlers.loaders;

import static ar.edu.itba.ati.back_2.models.ChannelType.BLUE;
import static ar.edu.itba.ati.back_2.models.ChannelType.GREEN;
import static ar.edu.itba.ati.back_2.models.ChannelType.RED;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.utils.ChannelTypeImagesFactory;
import ij.ImagePlus;
import ij.io.Opener;
import java.util.Arrays;
import java.util.List;

public class PPMLoader implements ImageLoader {

  @Override
  public Image loadImageFromFile(final String path) {
    final ImagePlus imagePlus = new Opener().openImage(path);

    return mapImagePlusToImage(imagePlus);
  }

  private Image mapImagePlusToImage(final ImagePlus imagePlus) {
    final Image image = ChannelTypeImagesFactory.getRGBImage(imagePlus.getHeight(),
        imagePlus.getWidth());

    mapPixelsFromImagePlusToImage(imagePlus, image);

    return image;
  }

  private void mapPixelsFromImagePlusToImage(final ImagePlus imagePlus, final Image image) {
    final List<ChannelType> channelTypes = Arrays.asList(RED, GREEN, BLUE);

    for (int channelTypeIndex = 0; channelTypeIndex < channelTypes.size(); channelTypeIndex++) {
      for (int x = 0; x < image.getHeight(); x++) {
        for (int y = 0; y < image.getWidth(); y++) {
          final int[] colorPixel = imagePlus.getPixel(y, x);

          image.setPixel(RED, x, y, colorPixel[0]);
          image.setPixel(GREEN, x, y, colorPixel[1]);
          image.setPixel(BLUE, x, y, colorPixel[2]);
        }
      }
    }
  }
}
