package ar.edu.itba.ati.back_2.operators.unary;

import ar.edu.itba.ati.back_2.models.Point;
import java.util.function.DoubleUnaryOperator;

public class LinearTransformation implements DoubleUnaryOperator {

  private final LinearFunction linearFunction;

  public LinearTransformation(final double x1, final double x2) {
    linearFunction = new LinearFunction(new Point(x1, 0), new Point(x2, 255));
  }

  @Override
  public double applyAsDouble(final double x) {
    return linearFunction.applyAsDouble(x);
  }
}
