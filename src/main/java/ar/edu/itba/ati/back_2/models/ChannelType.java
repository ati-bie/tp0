package ar.edu.itba.ati.back_2.models;

public enum ChannelType {
  GREY, RED, GREEN, BLUE
}
