package ar.edu.itba.ati.back_2.operators;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.utils.ImageDuplicator;

import java.util.Objects;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;

public final class ImageUnaryOperator implements Function<Image, Image> {

  private final DoubleUnaryOperator operator;

  public ImageUnaryOperator(final DoubleUnaryOperator operator) {
    Objects.requireNonNull(operator, "operator must not be null");

    this.operator = operator;
  }

  @Override
  public Image apply(final Image originalImage) {
    Objects.requireNonNull(originalImage, "image must not be null");

    final Image newImage =
        ImageDuplicator.createImageWithSameChannelTypes(originalImage);

    for (final ChannelType channelType : newImage.getChannelTypes()) {
      applyToNewChannel(newImage, channelType, originalImage);
    }

    return newImage;
  }

  public void applyToNewChannel(final Image newImage, final ChannelType channelType,
      final Image originalImage) {

    for (int x = 0; x < newImage.getHeight(); x++) {
      for (int y = 0; y < newImage.getWidth(); y++) {
        applyToNewPixel(newImage, channelType, x, y, originalImage);
      }
    }
  }

  private void applyToNewPixel(final Image newImage, final ChannelType channelType,
      final int x, final int y, final Image originalImage) {
    final double newValue = operator.applyAsDouble(originalImage.getPixel(channelType, x, y));

    newImage.setPixel(channelType, x, y, newValue);
  }
}
