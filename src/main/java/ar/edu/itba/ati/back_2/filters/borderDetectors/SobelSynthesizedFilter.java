package ar.edu.itba.ati.back_2.filters.borderDetectors;

import ar.edu.itba.ati.back_2.interfaces.Image;
import ar.edu.itba.ati.back_2.interfaces.WindowFilter;
import ar.edu.itba.ati.back_2.models.ChannelType;
import ar.edu.itba.ati.back_2.utils.ArrayUtils;

public class SobelSynthesizedFilter implements WindowFilter {

  private final static double[][] VERTICAL_WINDOW;
  private final static double[][] HORIZONTAL_WINDOW;
  private final static int SIDE;

  static {
    SIDE = 3;

    VERTICAL_WINDOW = new double[SIDE][SIDE];
    HORIZONTAL_WINDOW = new double[SIDE][SIDE];

    for (int i = 0; i < SIDE; i++) {
      for (int j = 0; j < SIDE; j++) {
        if (i == 0) {
          if (i == (SIDE - 1) / 2) {
            HORIZONTAL_WINDOW[i][j] = -2;
          } else {
            HORIZONTAL_WINDOW[i][j] = -1;
          }
        } else if (i == SIDE - 1) {
          if (i == (SIDE - 1) / 2) {
            HORIZONTAL_WINDOW[i][j] = 2;
          } else {
            HORIZONTAL_WINDOW[i][j] = 1;
          }
        }
        if (j == 0) {
          if (i == (SIDE - 1) / 2) {
            VERTICAL_WINDOW[i][j] = -2;
          } else {
            VERTICAL_WINDOW[i][j] = -1;
          }
        } else if (j == SIDE - 1) {
          if (i == (SIDE - 1) / 2) {
            VERTICAL_WINDOW[i][j] = 2;
          } else {
            VERTICAL_WINDOW[i][j] = 1;
          }
        }

      }
    }
  }

  public static double[][] getVerticalWindow() {
    return ArrayUtils.deepCopy(VERTICAL_WINDOW);
  }

  public static double[][] getHorizontalWindow() {
    return ArrayUtils.deepCopy(HORIZONTAL_WINDOW);
  }

  @Override
  public double applyFilter(final Image image, final ChannelType channelType, final int x,
      final int y) {

    final int minOffset = (VERTICAL_WINDOW.length - 1) / 2;
    double sumVer = 0, sumHor = 0;

    for (int i = -1 * minOffset; i <= minOffset; i++) {
      for (int j = -1 * minOffset; j <= minOffset; j++) {
        // Avoids getting elements outside window by repetition method
        int useI = Math.max(Math.min(x + i, image.getHeight() - 1), 0);
        int useJ = Math.max(Math.min(y + j, image.getWidth() - 1), 0);

        sumVer +=
            VERTICAL_WINDOW[i + minOffset][j + minOffset] * image.getPixel(channelType, useI, useJ);
        sumHor += HORIZONTAL_WINDOW[i + minOffset][j + minOffset] * image
            .getPixel(channelType, useI, useJ);
      }
    }
    return Math.sqrt((sumHor * sumHor) + (sumVer * sumVer));
  }
}
